// -*- C++ -*-

/* 
 * columnimportplugin.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_COLUMNIMPORTPLUGIN_H
#define GUPPI_COLUMNIMPORTPLUGIN_H

#include "columnimporter.h"
#include "plugin.h"

#include <gtk--.h>

#define COLUMN_IMPORT_PLUGIN_TYPE "importer_column"

class ColumnImportPlugin : public Plugin, public Gtk_Signal_Base {
public:
  ColumnImportPlugin() {}
  virtual ~ColumnImportPlugin() {}
  
  // Key used internally to classify the plugin (e.g. "plot")
  virtual const char* get_type() const;

  // Name to display in menus (e.g. "2D")
  virtual const char* get_menu_name() const = 0;

  // Human-readable name of the plugin (e.g. "Two Dimensional Plotting")
  virtual const char* get_name() const = 0;

  // Longer one-liner describing the plugin (e.g. "Draws 2D scatter
  // and line plots.")
  virtual const char* get_description() const = 0;
  
  // OK to unload?
  virtual bool unloadable() const = 0;

  virtual ColumnImporter* 
  make_importer(ColumnImporterData cid) const = 0;

protected:
  // Each plugin returns a value here which should match the 
  //  Guppi one.
  virtual unsigned int plugin_version() const = 0;

};

#endif
