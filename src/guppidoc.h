// -*- C++ -*-

/* 
 * guppidoc.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_GUPPIDOC_H
#define GUPPI_GUPPIDOC_H

// A Guppi "Document," which consists of a dataset and some plots
//  showing data from the set. 

// The GuppiDoc should not have any GUI elements. It is the "model",
//  while the GuppiApp is the "view" and "controller" for the entire
//  document. There are specialized views for the individual columns
//  and plots.

// My plan is to use Gtk-- signals to keep the GUI in sync with the 
//  data. 

#include "saveable.h"
#include "plotdoc.h"
#include "plotcontext.h"

class GuppiDoc : public Saveable,
		 public Named,
		 public Iconed
{
public:

  GuppiDoc();
  virtual ~GuppiDoc();

  void delete_self();

  ColumnDocList & get_columndoclist() { return columns_; }
  PlotContext &   get_plotcontext() { return pc_; }

  // GuppiDoc "owns" all the sub-documents and has to delete them.

  void add_plot(PlotDoc & p);
  void delete_plot(PlotDoc* p);

  typedef list<PlotDoc*>::iterator   iterator;

  typedef list<PlotDoc*>::const_iterator   const_iterator;
  
  iterator begin() {
    iterator i(plots_.begin());
    return i;
  }
  
  iterator end() {
    iterator i(plots_.end());
    return i;
  }
  
  const_iterator begin() const {
    const_iterator i(plots_.begin());
    return i;
  }
  
  const_iterator end() const {
    const_iterator i(plots_.end());
    return i;
  }

  class View : public Named::View, 
               public Iconed::View {
  public:
    virtual void add_plot(const PlotDoc& pd) = 0;
    virtual void delete_plot(const PlotDoc& pd) = 0;
    
  };

  class Model : public Model<GuppiDoc::View*> {
  public:
    void plot_added(const PlotDoc & pd) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->add_plot(pd);
	++i;
      }
      unlock_views();
    }

    void plot_deleted(const PlotDoc & pd) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->delete_plot(pd);
	++i;
      }
      unlock_views();
    }

  };
  
  Model guppidoc_model;

  void set_filename(const string & s) { filename_ = s; }
  const string & get_filename() const { return filename_; }

private:

  ColumnDocList  columns_;
  PlotContext    pc_;

  list<PlotDoc*> plots_;
  
  // Number of documents so far, used in the 
  //  default title.
  static guint num_docs_;

  string filename_;
};


#endif


