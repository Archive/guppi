// -*- C++ -*-

/* 
 * arrayplot.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "arrayplot.h"

#include "canvaslayer.h"
#include "canvasaxis.h"

#include "widgets.h"

class ArrayPlotImpl : public ArrayDoc::View
{ 
public:
  ArrayPlotImpl(ArrayDoc & ad, list<GtkMenuItem*> & menu);
  ~ArrayPlotImpl();
  
  Gtk_Widget* widget();
  ArrayLayout& array(); 
  const ArrayLayout& array() const;

  // handled by ArrayPlot proper
  void change_name    (const string & name) {}
  void change_icon    (const string & icon) {}

  // handled by us.
  void change_size(size_t rows, size_t cols);
  void change_layer(size_t row, size_t col);
  void change_axis(size_t row, size_t col, Axis_Base::alignment_t a);
  void destroy_model() { 
    // so any more references cause segfault.
    ad_ = 0; 
  }

  void canvas_allocated(GtkAllocation* a) {
    canvas_.set_scroll_region(0,0,a->width,a->height);
    // we need to do it *now*, not queue, because we need 
    //  it done before the canvas redraw idle
    do_layout();
  }

  gint do_layout();
  void queue_do_layout();

  void add_layer();

private:
  ArrayDoc* ad_;
  GtkWidget* canvas_;
  ArrayLayout  array_;

  GnomeCanvasGroup* arraygroup_;
  GnomeCanvasItem* noplots_;

  bool layout_queued_;

  list<GtkMenuItem*>& menu_;
};

ArrayPlot::ArrayPlot(ArrayDoc &ad) 
  : ad_(&ad), impl_(new ArrayPlotImpl(ad, menu_))
{
  ad_->named_model.add_view(*this);
  ad_->iconed_model.add_view(*this);
  ad_->plotdoc_model.add_view(*this);
}

ArrayPlot::~ArrayPlot()
{
  if (ad_) {
    ad_->named_model.remove_view(*this);
    ad_->iconed_model.remove_view(*this);
    ad_->plotdoc_model.remove_view(*this);
  }
  if (impl_) delete impl_;
}

GtkWidget*
ArrayPlot::widget()
{
  return impl_->widget();
}
 
ArrayPlotImpl::ArrayPlotImpl(ArrayDoc& ad, list<Gtk_MenuItem*>& menu)
  : ad_(&ad), canvas_(0), array_(),
    arraygroup_(canvas_.root(), NULL),
    noplots_(0),
    menu_(menu)
{
  canvas_ = gnome_canvas_new();
  gtk_widget_set_events(gtk_widget_get_events(canvas_) 
                        | GDK_BUTTON_PRESS_MASK);

  Widgets::zero_on_destroy (GTK_OBJECT(canvas_));

  arraygroup_ = gnome_canvas_item_new(gnome_canvas_group_get_type(),
                                      canvas_->root,
                                      NULL);

  Widgets::zero_on_destroy(GTK_OBJECT(arraygroup_));

  noplots_ = gnome_canvas_item_new(gnome_canvas_text_get_type(),
                                   arraygroup_, 
                                   "text", _("No Plots"), 
                                   NULL);

  Widgets::zero_on_destroy(GTK_OBJECT(noplots_));

  size_t r;
  size_t c;

  r = ad_->rows();
  c = ad_->columns();

  if (r == 0 || c == 0) {
    // leave noplots visible and create no items.
  }
  else {
    gnome_canvas_item_hide(noplots_);
    
    // we are at least 1x1, so create the plots
    size_t i = 0;
    while ( i < r ) {
      size_t j = 0;
      while ( j < c ) {
	change_layer(i,j);
	change_axis(i,j,Axis_Base::NORTH);
	change_axis(i,j,Axis_Base::SOUTH);
	change_axis(i,j,Axis_Base::EAST);
	change_axis(i,j,Axis_Base::WEST);

	++j;
      }
      ++i;
    }
  }

#if 0
  connect_to_method(canvas_.size_allocate,
		    this,
		    &ArrayPlotImpl::canvas_allocated);
#else 
  g_warning(__FUNCTION__);
#endif

  GtkWidget* mi = gtk_menu_item_new_with_label(_("Add plot to array"));
  menu_.push_back(GTK_MENU_ITEM(mi));

#if 0
  connect_to_method(mi->activate,
		    this,
		    &ArrayPlotImpl::add_layer);
#else 
  g_warning(__FUNCTION__);
#endif

#if 0 // we don't care about these, but technically we implement them
  ad_->named_model.add_view(*this);
  ad_->iconed_model.add_view(*this);
#endif
  ad_->plotdoc_model.add_view(*this);
  ad_->arraydoc_model.add_view(*this);
}

ArrayPlotImpl::~ArrayPlotImpl()
{
#if 0 // we don't care about these, but technically we implement them
  ad_->named_model.remove_view(*this);
  ad_->iconed_model.remove_view(*this);
#endif
  ad_->plotdoc_model.remove_view(*this);
  ad_->arraydoc_model.remove_view(*this);

  size_t r = array_.rows();
  size_t c = array_.columns();

  size_t i = 0;
  while ( i < r ) {
    size_t j = 0;
    while ( j < c ) {
      PlotLayoutHandle* plh;
      AxisLayoutHandle* alh;

      plh = array_.get_plot(i,j);
      if (plh) delete plh;

      alh = array_.get_axis(i,j,Axis_Base::NORTH);
      if (alh) delete alh;

      alh = array_.get_axis(i,j,Axis_Base::SOUTH);
      if (alh) delete alh;

      alh = array_.get_axis(i,j,Axis_Base::EAST);
      if (alh) delete alh;

      alh = array_.get_axis(i,j,Axis_Base::WEST);
      if (alh) delete alh;
      
      ++j;
    }
    ++i;
  }
}

void 
ArrayPlotImpl::add_layer()
{
  LayerStack* ls = 0;

  // purely a temporary hack, allows us to add one layer
  if (ad_->rows() > 0 && ad_->columns() > 0) {
    ls = ad_->get_layer(0,0);
    if (ls) delete ls;
    ad_->set_layer(0,0,0);
  }
  else {
    ad_->resize(1,1);
  }

  ls = ad_->make_layerstack();
  
  

  ad_->set_layer(0,0,ls);
}

void
ArrayPlotImpl::change_size (size_t rows, size_t cols)
{  
  size_t r = ad_->rows();
  size_t c = ad_->columns();

  size_t ar = array_.rows();
  size_t ac = array_.columns();

  // If the arraydoc shrank, then theoretically the removed cells were empty,
  //  and we were not displaying them anyway.
  // If it grew, they should be empty because the arraydoc has to be resized 
  //  before anything can be added.
  // So we know we don't need to do anything other than resize our ArrayLayout

#ifdef GNOME_ENABLE_DEBUG
  // An elaborate assertion to verify the above...
  size_t i = 0;
  while ( i < r ) {
    size_t j = 0;
    while ( j < c ) {
      PlotLayoutHandle* plh;
      AxisLayoutHandle* alh;

      plh = array_.get_plot(i,j);
      if (plh) g_warning("Plot %u %u remains in ArrayPlot!", i, j);

      alh = array_.get_axis(i,j,Axis_Base::NORTH);
      if (alh) g_warning("Axis %u %u remains in ArrayPlot!", i, j);

      alh = array_.get_axis(i,j,Axis_Base::SOUTH);
      if (alh)  g_warning("Axis %u %u remains in ArrayPlot!", i, j);

      alh = array_.get_axis(i,j,Axis_Base::EAST);
      if (alh)  g_warning("Axis %u %u remains in ArrayPlot!", i, j);

      alh = array_.get_axis(i,j,Axis_Base::WEST);
      if (alh)  g_warning("Axis %u %u remains in ArrayPlot!", i, j);
      
      ++j;
    }
    ++i;
  }
#endif

  if ( (r >= ar) && (c >= ac) ) {
    // WARNING: possible bug source, array_ out of sync
    ; // ArrayLayout will grow automatically when we need it to.
  }
  else if ( (r < ar) && (c < ac) ) {
    array_.shrink(r,c);
  }
  else if (r < ar) {
    // c >= ac, but shrink shouldn't take an arg larger than 
    //  current size, so use ac.
    array_.shrink(r,ac);
  }
  else if (c < ac) {
    array_.shrink(c,ar);
  }
  else {
    g_warning("Resize case not handled. ad_: %u x %u  array_: %u x %u",
	      r,c,ar,ac);
  }

  queue_do_layout();
}

void
ArrayPlotImpl::change_layer(size_t row, size_t col)
{
  PlotLayoutHandle* plh;
  
  if (array_.rows() > row && array_.columns() > col) {
    plh = array_.get_plot(row,col);
    if (plh) delete plh;
    array_.erase_plot(row,col);
  }
  // if it isn't small enough, array_ resizes automatically

  Layer* layer = ad_->get_layer(row,col);
  if (layer) {
    CanvasLayer* cl = new CanvasLayer(arraygroup_);
    cl->set_layer(ad_->get_layer(row,col));
    array_.place_plot(cl,row,col);
  }
  queue_do_layout();
}

void
ArrayPlotImpl::change_axis(size_t row, size_t col, Axis_Base::alignment_t a)
{
  AxisLayoutHandle* alh;
  
  alh = array_.get_axis(row,col,a);
  
  if (alh) delete alh;
  array_.erase_axis(row,col,a);

  Axis* axis = ad_->get_axis(row,col,a);
  if (axis) {
    CanvasAxis* ca = 0;
    ca = axis->make_canvas_axis(arraygroup_,a);
    array_.place_axis(ca,row,col,a);
  }
  queue_do_layout();
}

GtkWidget*
ArrayPlotImpl::widget()
{
  return canvas_;
}

gint 
ArrayPlotImpl::do_layout()
{
  double x1, y1, x2, y2;
  
  gnome_canvas_get_scroll_region(GNOME_CANVAS(canvas_),
                                 &x1, &y1, &x2, &y2);

  // This makes item coordinates == world coordinates; otherwise
  //  all of our stuff would be hosed up. FIXME need to convert 
  //  from world coords to item coords for this array group,
  //  I am just too lazy to add necessary methods to Gnome_CanvasGroup
  gnome_canvas_item_set(GNOME_CANVAS_ITEM(arraygroup_), 
                        "x", x1, "y", y1, 0);

  array_.set_size(x1, y1, x2, y2);

  gnome_canvas_item_set(noplots_, "x", x2/2, "y", y2/2, NULL);  

  if (array_.rows() == 0 || array_.columns() == 0)
    gnome_canvas_item_show(noplots_);
  else 
    gnome_canvas_item_hide(noplots_);

  array_.do_layout();
  layout_queued_ = false;
  return FALSE;
}

void 
ArrayPlotImpl::queue_do_layout()
{
  if (layout_queued_) return;
  else {
    layout_queued_ = true;
    // add at a slightly high priority, so it happens before the 
    //  canvas draw routine.
#if 0
    connect_to_method(Gtk_Main::idle(-1), this, &do_layout);
#else
    g_warning(__FUNCTION__);
#endif
  }
}

// These should be passed out via our ArrayPlot::Model,
//  so the window we're contained in can respond properly.
void
ArrayPlot::change_name    (const string & name)
{
  g_warning(__FUNCTION__);
}

void
ArrayPlot::change_icon    (const string & icon)
{
  g_warning(__FUNCTION__);
}

void
ArrayPlot::destroy_model()
{
  ad_ = 0;
}
