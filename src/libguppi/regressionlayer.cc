// -*- C++ -*-

/* 
 * regressionlayer.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "regressionlayer.h"
#include "markermenu.h"
#include "columndocmenu.h"

class RegressionEdit : public Gtk_Frame {
public:
  RegressionEdit(RegressionLayer& rl);
  ~RegressionEdit();

private:
  RegressionLayer& rl_;

  Gtk_VBox box_;
  Gtk_HBox  linebox_;
  Gtk_Label linelabel_;
  Gnome_ColorPicker linecolor_;

  Gtk_HBox       xbox_;
  Gtk_Label      xlabel_;
  Gtk_OptionMenu xoption_;
  ColumnDocMenu  xmenu_;
  Gtk_HBox       ybox_;
  Gtk_Label      ylabel_;
  Gtk_OptionMenu yoption_;
  ColumnDocMenu  ymenu_;

  void change_name(const Axis& a);
  void change_color(const RegressionLayer & layer);
  void set_color(guint r, guint g, guint b, guint a);
  void rl_destroyed(Layer & l);
  void x_chosen(const ColumnDoc* cd);
  void y_chosen(const ColumnDoc* cd);
  void x_changed(const ColumnDoc* cd);
  void y_changed(const ColumnDoc* cd);

  bool block_columnmenu_update_;

  RegressionEdit();
};

RegressionEdit::RegressionEdit(RegressionLayer& rl) 
  : rl_(rl), box_(false,GNOME_PAD_SMALL),
    linebox_(false,GNOME_PAD_SMALL),
    linelabel_(_("Line color: ")),
    linecolor_(),
    xbox_(false,0), xlabel_(_("X (Independent)")), 
    xoption_(), xmenu_(rl.cdl_),
    ybox_(false,0), ylabel_(_("Y (Dependent)")), 
    yoption_(), ymenu_(rl.cdl_),
    block_columnmenu_update_ (false)
{
  set_shadow_type(GTK_SHADOW_IN);
  add(&box_);

  change_color(rl);

  connect_to_method(rl_.color_changed,
		    this,
		    &RegressionEdit::change_color);

  connect_to_method(linecolor_.color_set,
		    this,
		    &RegressionEdit::set_color);

  connect_to_method(rl_.destroy,
		    this,
		    &RegressionEdit::rl_destroyed);

  // callbacks for future selections.
  connect_to_method(xmenu_.columndoc_activated,
		    this,
		    &RegressionEdit::x_chosen);

  connect_to_method(ymenu_.columndoc_activated,
		    this,
		    &RegressionEdit::y_chosen);

  connect_to_method(rl_.x_changed,
		    this,
		    &RegressionEdit::x_changed);

  connect_to_method(rl_.y_changed,
		    this,
		    &RegressionEdit::y_changed);

  linebox_.pack_start(linelabel_,true,true);
  linebox_.pack_end(linecolor_,false,false);
  box_.pack_start(linebox_,false,false);
  linebox_.show_all();

  xbox_.pack_start(xlabel_,false,false);
  xbox_.pack_end(xoption_,true,true);
  xbox_.set_border_width(GNOME_PAD_SMALL/2);
  xoption_.set_menu(xmenu_);
  ybox_.pack_start(ylabel_,false,false);
  ybox_.pack_end(yoption_,true,true);
  ybox_.set_border_width(GNOME_PAD_SMALL/2);
  yoption_.set_menu(ymenu_);
  box_.pack_start(xbox_,false,false);
  box_.pack_start(ybox_,false,false);

  // set menu to correct state. Must happen after set_menu on the 
  //  option menu.
  x_changed(rl_.x());
  y_changed(rl_.y());

  box_.show_all();
}

RegressionEdit::~RegressionEdit()
{

}

void 
RegressionEdit::change_color(const RegressionLayer & colored)
{
  const Gdk_Color & c(colored.get_color());
  linecolor_.set(c.red(),c.green(),c.blue(),0);
}

void 
RegressionEdit::set_color(guint r, guint g, guint b, guint a)
{
  rl_.set_color_rgb(r,g,b);
}

void
RegressionEdit::rl_destroyed(Layer & l)
{
  delete_self();
}

void 
RegressionEdit::x_chosen(const ColumnDoc* cd)
{
  block_columnmenu_update_ = true;
  rl_.set_x(cd);
  block_columnmenu_update_ = false;
}

void 
RegressionEdit::y_chosen(const ColumnDoc* cd)
{
  block_columnmenu_update_ = true;
  rl_.set_y(cd);
  block_columnmenu_update_ = false;
}

void 
RegressionEdit::x_changed(const ColumnDoc* cd)
{
  if (block_columnmenu_update_) return; // already active due to user intervention.
  xoption_.set_history(xmenu_.index(cd));
}

void 
RegressionEdit::y_changed(const ColumnDoc* cd)
{
  if (block_columnmenu_update_) return;
  yoption_.set_history(ymenu_.index(cd));
}

////////////////////////////////////////////////

RegressionLayer::RegressionLayer(const NumericAxis* xa,
				 const NumericAxis* ya,
				 const ColumnDocList & cdl)
  : XYLayer(xa,ya), cdl_(cdl)
{
  set_name(get_vs_string(x(),y()) + _(" (regression)"));
}


RegressionLayer::~RegressionLayer()
{
  
}

// FIXME there shouldn't be GUI stuff in here really. But it's 
//  hard to fix without exceptions.

void        
RegressionLayer::set_x(const ColumnDoc* cd)
{
  if (cd) {
    if (cd == y()) {
      Gnome::error_dialog(_("It makes no sense to regress data vs. itself."));
      cd = 0;
    }
    else if (y()) {
      if (cd->ds().size() !=  y()->ds().size()) {
	Gnome::error_dialog(_("Must choose an axis with the same number of data points."));
	cd = 0;
      }
    }
  }    
  set_name(get_vs_string(cd,y()) + _(" (regression)"));

  if (cd && y()) { r_.model(cd->ds(),y()->ds()); }
  XYLayer::set_x(cd);
}

void        
RegressionLayer::set_y(const ColumnDoc* cd)
{
  if (cd) {
    if (cd == x()) {
      Gnome::error_dialog(_("It makes no sense to regress data vs. itself."));
      cd = 0;
    }
    else if (x()) {
      if (cd->ds().size() !=  x()->ds().size()) {
	Gnome::error_dialog(_("Must choose an axis with the same number of data points."));
	cd = 0;
      }
    }
  }

  set_name(get_vs_string(x(),cd) + _(" (regression)"));

  if (cd && x()) { r_.model(x()->ds(),cd->ds()); }
  XYLayer::set_y(cd);
}

Gtk_Widget* 
RegressionLayer::make_editor()
{
  return new RegressionEdit(*this);
}

void        
RegressionLayer::draw(Plot_Area& target) const
{
  Gdk_Draw* draw = target.drawing_target();
  g_return_if_fail(draw != 0);

  draw->gc_save();

  draw->gc()->set_foreground(get_color());

  gchar* error_string = 0;
  if (x() == 0 && 
      y() == 0) { 
    error_string = _("No Data"); 
  }
  else if (x() == 0) { 
    error_string = _("No X Data");
  }
  else if (y() == 0) { 
    error_string = _("No Y Data"); 
  }

  const NumericAxis* xa, *ya;

  xa = x_axis();
  ya = y_axis();

  if (xa == 0 && ya == 0) {
    error_string = _("No axes");
  }
  else if (xa == 0) {
    error_string = _("No X axis");
  }
  else if (ya == 0) {
    error_string = _("No Y axis");
  }

  if (error_string) {
    draw->draw_string(30,30,error_string);
    draw->gc_restore();
    return;
  } 
 
  g_assert(xa);
  g_assert(ya);
  g_assert(x());
  g_assert(y());

  target.set_x_bounds(xa->get_start(), xa->get_stop());
  target.set_y_bounds(ya->get_start(), ya->get_stop());
   
  double x1, y1, x2, y2;
  x1 = x()->ds().min();
  x2 = x()->ds().max();
  y1 = r_.predict(x1);
  y2 = r_.predict(x2);
  
  target.draw_line_through_points(x1,y1,x2,y2);
  
  draw->gc_restore();
  return;
}

