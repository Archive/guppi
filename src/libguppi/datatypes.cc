// -*- C++ -*-

/* 
 * datatypes.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "datatypes.h"

// Add a global for each type
static GuppiRealType global_guppi_real_type;
// ...

void 
register_all_builtin_datatypes(Store<DataType> & store)
{
#ifdef GNOME_ENABLE_DEBUG
  g_print("Registering datatypes... ");
#endif
  // Add a line for each type
  store.register_proxy(global_guppi_real_type);
  // ...
#ifdef GNOME_ENABLE_DEBUG
  g_print("Done.\n");
#endif
}


