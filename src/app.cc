// -*- C++ -*-

/* 
 * app.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <algo.h>
#include <list>

#include "app.h"
#include "guppi.h"
#include "columnlist.h"
#include "importer.h"
#include "datatypestore.h"

#include "plugins.h"
#include "plotplugin.h"
#include "columnimportplugin.h"
#include "pluginwindow.h"

static void delete_columnimporter(Importer<ColumnDoc>* importer) 
{ 
  importer->delete_self(); 
  global_plugin_registry.consider_unloading();
}

class GuppiAppImpl : public GuppiDoc::View
{
public:
  GuppiAppImpl(GuppiDoc* doc,
	       const gchar* geometry,
	       GuppiApp & app);
  ~GuppiAppImpl();

  GtkWidget* widget() { return app_; }

  // These have to be public for the callbacks,
  //  but don't use them. 
  // If I ever get un-lazy the callbacks could be declared
  //  friend
  // Menu actions.
  void about();
  void new_doc();
  void open();
  void import_column(size_t i);
  void save_as();
  void preferences();
  void import_columns(Importer<ColumnDoc>* ci);
  void new_plot(size_t i);

  // abuse potential
  GuppiDoc* doc() { return doc_; }

protected:
  // setup/reset
  void init();
  void view_doc();
  void make_menus();
  void set_sensitivity();
  void set_doc(GuppiDoc * doc);
  
  // Interface for GuppiDoc::View

  void change_icon(const string & icon);
  void change_name(const string & name);

  void add_plot(const PlotDoc & pd);
  void delete_plot(const PlotDoc & pd);
  
  void destroy_model();

private:
  GuppiDoc * doc_;
  GuppiApp & guppiapp_;

  ColumnList columns_page_;

  GtkWidget* app_;
};


GuppiApp::GuppiApp(GuppiDoc* doc, const gchar* geometry) :
  Gnome_App(Guppi::APPNAME, ""), 
  impl_(new GuppiAppImpl(doc,geometry,*this))
{
  
}

GuppiApp::~GuppiApp()
{
  delete impl_;
#ifdef GNOME_ENABLE_DEBUG
  g_print("~GuppiApp\n");
#endif
}

GtkWidget* 
GuppiApp::widget()
{
  return impl_->widget();
}

GuppiAppImpl::GuppiAppImpl(GuppiDoc* doc,
			   const gchar * geometry,
			   GuppiApp & app) : 
  doc_(doc), guppiapp_(app), 
  columns_page_(doc->get_columndoclist()), 
  app_(0)
{
  init();
}

GuppiAppImpl::~GuppiAppImpl()
{  
  if (doc_) {
    doc_->guppidoc_model.remove_view(*this);
    doc_->named_model.remove_view(*this);
    doc_->iconed_model.remove_view(*this);
  }
}

void
GuppiAppImpl::init()
{
  make_menus();

  gnome_app_set_contents(GNOME_APP(app_),
                         columns_page_.widget());

  gtk_widget_show_all(columns_page_.widget());

  view_doc();
}


// For initial setup, we have to slurp everything;
//  then up-to-dateness is preserved by signals.
void
GuppiAppImpl::view_doc()
{
  g_return_if_fail(doc_ != 0);

  change_name(doc_->get_name());
  
  set_sensitivity();

  doc_->guppidoc_model.add_view(*this);
  doc_->named_model.add_view(*this);
  doc_->iconed_model.add_view(*this);
}

void
GuppiAppImpl::set_doc(GuppiDoc* doc)
{
  g_warning(__FUNCTION__);
}

void
GuppiAppImpl::change_icon(const string & icon)
{
  g_warning(__FUNCTION__);
}

void
GuppiAppImpl::change_name(const string & name)
{
  gtk_window_set_title(GTK_WINDOW(app_), name.c_str());
}

void
GuppiAppImpl::add_plot(const PlotDoc & pd)
{

}

void
GuppiAppImpl::delete_plot(const PlotDoc & pd)
{

}

void
GuppiAppImpl::destroy_model()
{
  if (doc_) {
    doc_ = 0;
    gtk_widget_destroy(app_);
    app_ = 0;
    g_warning(__FUNCTION__);
    guppiapp_.delete_self(); // FIXME
  }
}


//////////////// callbacks begin

void
GuppiAppImpl::new_doc()
{
  big_fish.new_doc();
}

void 
GuppiAppImpl::open()
{
  gnome_ok_dialog(_("Implement loading and saving! You know you want to!"));
}

void
GuppiAppImpl::import_column(size_t i)
{
  PluginHandle* ph = 
    global_plugin_registry.get_handle(COLUMN_IMPORT_PLUGIN_TYPE,i);
  if (ph == 0) Gnome::error_dialog(_("Plugin failure"));
  ColumnImportPlugin* cip = (ColumnImportPlugin*) ph->get_plugin();

  Importer<ColumnDoc>* ci = 
    cip->make_importer(ColumnImporterData(global_datatype_store));

  cip->unref(); // the importer should keep a ref too
#if 0
  connect_to_method(ci->finished, this,
		    &GuppiAppImpl::import_columns);
  connect_to_function(ci->cancelled,
		      delete_columnimporter);
#else
  g_warning(__FUNCTION__);
#endif
}

void
GuppiAppImpl::save_as()
{
  gnome_ok_dialog(_("Implement loading and saving! You know you want to!"));
}

void 
GuppiAppImpl::preferences()
{
  gnome_ok_dialog(_("Haven't thought of any yet."));
}

void 
GuppiAppImpl::import_columns(Importer<ColumnDoc>* ci)
{
  g_print("Importing columns...\n");
  list<ColumnDoc*>* columns = ci->get_docs();
  if (columns != 0) {
    list<ColumnDoc*>::iterator i = columns->begin();
    while (i != columns->end()) {
      g_print("Adding imported column: %s\n", (*i)->get_name().c_str());
      doc_->get_columndoclist().add_column(**i);
      ++i;
    }
    delete columns;
    // note that doc_ now owns the columns and will delete them
    //  when it dies.
  }

  delete_columnimporter(ci);
}

void
GuppiAppImpl::new_plot(size_t i)
{
  PluginHandle* ph = global_plugin_registry.get_handle(PLOT_PLUGIN_TYPE,i);
  if (ph == 0) Gnome::error_dialog(_("Plugin failure"));
  PlotPlugin* pp = (PlotPlugin*) ph->get_plugin();

  PlotDoc* pd = pp->make_plotdoc(doc_->get_plotcontext());

  pp->unref(); // the PlotDoc keeps a ref in effect (not yet in practice, but soon)
  
  doc_->add_plot(*pd);  
}

///////////////////////////////// Menu cruft

// Bunch of callbacks - they just call the relevant methods on the 
//  object sent in as data.

static void 
about_cb(GtkWidget * w, gpointer data)
{
  g_return_if_fail(data != 0);
  big_fish.about();
}

static void
new_doc_cb(GtkWidget * w, gpointer data)
{
  g_return_if_fail(data != 0);
  g_print("new_doc_cb(GtkWidget * w, gpointer data)\n");
  ((GuppiAppImpl *)data)->new_doc();
}

static void
open_cb(GtkWidget * w, gpointer data)
{
  g_return_if_fail(data != 0);
  ((GuppiAppImpl *)data)->open();
}

static void
save_cb(GtkWidget * w, gpointer data)
{
  g_return_if_fail(data != 0);
  big_fish.save_doc( ((GuppiAppImpl*)data)->doc() );
}

static void
save_as_cb(GtkWidget * w, gpointer data)
{
  g_return_if_fail(data != 0);
  ((GuppiAppImpl *)data)->save_as();
}


static void
plugins_cb(GtkWidget * w, gpointer data)
{
  g_return_if_fail(data != 0);
  Gnome_Dialog* gd = new PluginWindow(global_plugin_registry);
  gd->show();
}

static void
close_cb(GtkWidget * w, gpointer data)
{
  g_return_if_fail(data != 0);
  big_fish.close_doc(((GuppiAppImpl *)data)->doc());
}

static void 
exit_cb(GtkWidget * w, gpointer data)
{
  big_fish.exit();
}

static void
preferences_cb(GtkWidget * w, gpointer data)
{
  g_return_if_fail(data != 0);
  ((GuppiAppImpl *)data)->preferences();
}


static GnomeUIInfo help_menu[] = {
  GNOMEUIINFO_HELP(const_cast<gchar*>(Guppi::APPNAME)),
  {
    GNOME_APP_UI_ITEM, N_("_About..."), 
    N_("Tell about this application"), 
    about_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT, 
    0, (GdkModifierType)0, NULL 
  },
  GNOMEUIINFO_END
};

static GnomeUIInfo file_menu[] = {
  {
    GNOME_APP_UI_ITEM, 
    N_("_New Data Set"), N_("Open a new data set document"), 
    new_doc_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW, 'n', 
    GDK_CONTROL_MASK, NULL 
  },
  {
    GNOME_APP_UI_ITEM, 
    N_("_Open..."), N_("Open a new file in this window"), 
    open_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_OPEN, 'o', 
    GDK_CONTROL_MASK, NULL 
  },
#define MENU_SAVE_POS 2
  {
    GNOME_APP_UI_ITEM, 
    N_("_Save"), N_("Save file to disk"), 
    save_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE, 's', 
    GDK_CONTROL_MASK, NULL 
  },
#define MENU_SAVEAS_POS 3
  {
    GNOME_APP_UI_ITEM, 
    N_("S_ave As..."), N_("Save file to disk"), 
    save_as_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE, 'f', 
    GDK_CONTROL_MASK, NULL 
  },
  {
    GNOME_APP_UI_ITEM, 
    N_("P_lugins..."), N_("View available plugins"), 
    plugins_cb, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 'p', 
    GDK_CONTROL_MASK, NULL 
  },  
  GNOMEUIINFO_SEPARATOR,
  {
    GNOME_APP_UI_ITEM, N_("Close"), 
    N_("_Close this window"),
    close_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXIT, 'c', 
    GDK_CONTROL_MASK, NULL 
  },
  {
    GNOME_APP_UI_ITEM, N_("Exit"), 
    N_("_Quit the application"),
    exit_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXIT, 'q', 
    GDK_CONTROL_MASK, NULL 
  },
  GNOMEUIINFO_END
};

static GnomeUIInfo new_plot_menu[] = {

  GNOMEUIINFO_END
};

static GnomeUIInfo import_column_menu[] = {
  GNOMEUIINFO_END
};

static GnomeUIInfo document_menu[] = {
#define NEW_PLOT_POS 0
  GNOMEUIINFO_SUBTREE( N_("New _Plot"), new_plot_menu ),
#define MENU_IMPORT_POS 1
  GNOMEUIINFO_SUBTREE( N_("_Import Column"), import_column_menu ),
  GNOMEUIINFO_END
};

static GnomeUIInfo prefs_menu[] = {
  {
    GNOME_APP_UI_ITEM, N_("_Preferences..."), 
    N_("Change application preferences"),
    preferences_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PREF, 'p', 
    GDK_CONTROL_MASK, NULL 
  },
  GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[] = {
  GNOMEUIINFO_SUBTREE (N_("_File"), file_menu),
  GNOMEUIINFO_SUBTREE (N_("_Data Set"), document_menu),
  GNOMEUIINFO_SUBTREE (N_("_Preferences"), prefs_menu),
  GNOMEUIINFO_SUBTREE (N_("_Help"), help_menu),
  GNOMEUIINFO_END
};

void 
GuppiAppImpl::make_menus()
{
  app_.create_menus_with_data(main_menu, this);

#ifdef GNOME_ENABLE_DEBUG
  g_print("Constructing import menu from registered importers:\n");
#endif
  PluginRegistry::const_iterator i = 
    global_plugin_registry.begin(COLUMN_IMPORT_PLUGIN_TYPE);
  PluginRegistry::const_iterator end = 
    global_plugin_registry.end(COLUMN_IMPORT_PLUGIN_TYPE);
  size_t n = 0;

  while ( i != end ) {
#ifdef GNOME_ENABLE_DEBUG
    g_print("%d: %s\n", n, (*i)->get_name().c_str());
#endif
    GtkWidget* mi = gtk_menu_item_new_with_label((*i)->get_menu_name().c_str());
    gtk_menu_append(GTK_MENU(import_column_menu[0].widget),
		    mi);

#if 0
    connect_to_method(mi->activate,
		      this,
		      &GuppiAppImpl::import_column,
		      n);
#else 
    g_warning(__FUNCTION__);
#endif
    gtk_widget_show(mi);
    ++n;
    ++i;
  }


#ifdef GNOME_ENABLE_DEBUG
  g_print("Constructing plot menu from registered plot types:\n");
#endif
  PluginRegistry::const_iterator j = 
    global_plugin_registry.begin(PLOT_PLUGIN_TYPE);
  end =
    global_plugin_registry.end(PLOT_PLUGIN_TYPE);
  n = 0;

  while ( j != end ) {
    PluginHandle* ph = *j;
    g_assert(ph != 0);

#ifdef GNOME_ENABLE_DEBUG
    g_print("%d: %s\n", n, ph->get_name().c_str());
#endif
    GtkWidget* mi = gtk_menu_item_new_with_label(ph->get_menu_name().c_str());

    gtk_menu_append(GTK_MENU(new_plot_menu[0].widget),
		    mi);

#if 0
    connect_to_method(mi->activate,
		      this,
		      &GuppiAppImpl::new_plot,
		      n);
#else
    g_warning(__FUNCTION__);
#endif

    gtk_widget_show(mi);

    ++n;
    ++j;
  }
}

void
GuppiAppImpl::set_sensitivity()
{
  gtk_widget_set_sensitive(file_menu[MENU_SAVE_POS].widget,
			   doc_->need_save());  
}

