// -*- C++ -*-

/* 
 * categoricalaxis.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "categoricalaxis.h"

CategoricalAxis::CategoricalAxis()
  : categories_(0)
{
  
}

CategoricalAxis::~CategoricalAxis()
{
  destroy();

  if (categories_)  delete_data();
}

void 
CategoricalAxis::set_size(size_t s)
{
  if (categories_ == 0) {
    categories_ = new vector<char*>(s,0);
    return;
  }
  else {
    size_t N = categories_->size();
    if (s == N) return;
    if (s > N) {
      categories_->resize(s,0);
      return;
    }
    else { // N > s
      size_t i = s;
      while (i < N) {
	char* c = (*categories_)[i];
	if (c) delete [] c;
	++i;
      }
      categories_->resize(s);
    }
  }
}

size_t 
CategoricalAxis::size() const
{
  if (categories_ == 0) return 0;
  else return categories_->size();
}

const char* 
CategoricalAxis::get_category(size_t i)
{
  g_return_val_if_fail(categories_ != 0, 0);
  g_return_val_if_fail(categories_->size() >= i, 0);

  return (*categories_)[i];
}

void        
CategoricalAxis::set_category(const char* s, size_t i)
{
  g_return_if_fail(categories_ != 0);
  g_return_if_fail(categories_->size() >= i);

  if ((*categories_)[i] != 0) delete [] (*categories_)[i];
  if (s == 0) { 
    (*categories_)[i] = 0;
    return;
  }
  else {
    size_t len = strlen(s);
    char* c = new char[len+1];
    strncpy(c,s,len+1);
    (*categories_)[i] = c;
    return;
  }
}

void 
CategoricalAxis::delete_data()
{
  if (categories_ == 0) return;

  vector<char*>::iterator i = categories_->begin();
  while (i != categories_->end()) {
    delete [] *i;
    ++i;
  }
  delete categories_;
  categories_ = 0;
}
