// -*- C++ -*-

/* 
 * canvasdrawable.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "canvasdrawable.h"

#ifdef GNOME_ENABLE_DEBUG
static gint debug_func(GdkEvent* e) {
  g_print("event static func\n");
  return FALSE;
}
#endif

CanvasDrawable::CanvasDrawable(Gnome_CanvasGroup & parent, 
                               const gchar* first_arg_name, ...)
  : Gnome_CanvasItem(GNOME_CANVAS_ITEM(gtk_type_new(c_class.get_type()))), 
    x1_(0.0), y1_(0.0), x2_(0.0), y2_(0.0),
    bw_(0), bh_(0),
    buffer_(0), 
    pixmap_gc_(0),
    pixmap_needs_update_(true)
{
  va_list ap;
   
  va_start(ap,first_arg_name);
  item_construct(&parent, first_arg_name, ap);
  va_end(ap);

#ifdef GNOME_ENABLE_DEBUG
  connect_to_function(event,
                      debug_func);
#endif
}

CanvasDrawable::~CanvasDrawable()
{
  if (buffer_) {
    gdk_pixmap_unref(buffer_);
    pixmap_gc_ = 0;
  }

  if (pixmap_gc_) {
    gdk_gc_unref(pixmap_gc_);
    pixmap_gc_ = 0;
  }

  buffer_ = 0; 
  pixmap_gc_ = 0;
}


void 
CanvasDrawable::make_buffer()
{
  // Determine how big the buffer is supposed to be.
  double wx1 = x1_, wy1 = y1_, wx2 = x2_, wy2 = y2_;
  int cx1, cy1, cx2, cy2;

  i2w(&wx1, &wy1);
  i2w(&wx2, &wy2);
  
  canvas()->w2c(wx1, wy1, &cx1, &cy1);
  canvas()->w2c(wx2, wy2, &cx2, &cy2);

  if ( buffer_ && 
       (bw_ == cx2 - cx1) && 
       (bh_ == cy2 - cy1) ) {
    ; /* nothing */
  }
  else {
    if (buffer_) gdk_pixmap_unref(buffer_);
   
    bw_ = cx2 - cx1;
    bh_ = cy2 - cx1;

    if (bw_ == 0 || bh_ == 0) {
      buffer_ = 0;
      return;
    }

    g_assert(bw_ > 0);
    g_assert(bh_ > 0);

    GdkWindow* window = GTK_LAYOUT(canvas()->gtkobj())->bin_window;
    if (window) {
      buffer_ = gdk_pixmap_new(window, bw_, bh_, canvas()->get_visual()->depth);
    }
    else buffer_ = 0;
  }

  if (buffer_) {
    queue_buffer_draw();
  }
}

void 
CanvasDrawable::queue_buffer_draw()
{
  pixmap_needs_update_ = true;

  /* We need to require a redraw for the whole area */
  
  int x1, y1, x2, y2;
  
  double dx = 0.0;
  double dy = 0.0;
  
  i2w (&dx, &dy);
  
  canvas()->w2c (dx + x1_, dy + y1_, &x1, &y1);
  canvas()->w2c (dx + x2_, dy + y2_, &x2, &y2);

  canvas()->request_redraw(x1, x2, y1, y2);
}

void 
CanvasDrawable::bounds_impl(double* x1, double* y1, double* x2, double* y2)
{
  g_return_if_fail(x1);
  g_return_if_fail(y1);
  g_return_if_fail(x2);
  g_return_if_fail(y2);

  // I think we want to return item coordinates. I hope so.

  *x1 = x1_;
  *y1 = y1_;
  *x2 = x2_;
  *y2 = y2_;
}

void 
CanvasDrawable::reconfigure_impl()
{
  double x1, y1, x2, y2;
  int cx1, cx2, cy1, cy2;
  
  make_buffer();
  
  x1 = x1_;
  x2 = x2_;
  y1 = y1_;
  y2 = y2_;

  i2w(&x1, &y1);
  i2w(&x2, &y2);
  
  canvas()->w2c(x1, y1, &cx1, &cy1);
  canvas()->w2c(x2, y2, &cx2, &cy2);


  GNOME_CANVAS_ITEM(gtkobj())->x1 = cx1 - 1;
  GNOME_CANVAS_ITEM(gtkobj())->x2 = cx2 + 1;
  GNOME_CANVAS_ITEM(gtkobj())->y1 = cy1 - 1;
  GNOME_CANVAS_ITEM(gtkobj())->y2 = cy2 + 1;

  parent()->child_bounds(this); 

  // I don't feel this should be needed but evidently it is
  queue_buffer_draw();
}


void 
CanvasDrawable::realize_impl()
{
  pixmap_gc_ = gdk_gc_new(GTK_LAYOUT(canvas()->gtkobj())->bin_window);
  reconfigure_impl();
}

void 
CanvasDrawable::unrealize_impl()
{
  if (buffer_) {
    gdk_pixmap_unref(buffer_);
    buffer_ = 0;
  }
  if (pixmap_gc_) {
    gdk_gc_unref(pixmap_gc_);
    pixmap_gc_ = 0;
  }
}

void 
CanvasDrawable::draw_impl(GdkDrawable* drawable, 
			  int x, int y, int w, int h)
{
  if (buffer_ == 0) {
    // probably means we have a dimension measuring 0 
    return;
  }
  else if (pixmap_needs_update_) {
    Plot_Area pa;
    Gdk_Draw draw;

    draw.set_drawable(buffer_);

    g_assert(draw.gc());

    // draw background
    draw.gc()->set_foreground(get_color());
    draw.draw_rectangle(true, 0,0, draw.width(), draw.height());
#ifdef GNOME_ENABLE_DEBUG
    Gdk_Color c;
    c.set("black");
    draw.gc()->set_foreground(c);
    draw.draw_string(10,10,"FOO");
    draw.gc()->set_line_style(GDK_LINE_ON_OFF_DASH);
    draw.draw_rectangle(false, 0,0,draw.width()-1,draw.height()-1);
#endif

    pa.set_drawing_target(&draw);
    draw_on_buffer(pa);

    pixmap_needs_update_ = false;
  }

  double dx, dy;
  int x1, y1, x2, y2;
  
  /* Get canvas pixel coordinates */
  
  dx = 0.0;
  dy = 0.0;
  
  i2w (&dx, &dy);
  
  canvas()->w2c (dx + x1_, dy + y1_, &x1, &y1);
  canvas()->w2c (dx + x2_, dy + y2_, &x2, &y2);

  g_print("our redraw is x: %d y: %d w: %d h: %d\n",
	  x, y, w, h);
  g_print("our size is x1: %f x2: %f y1: %f y2: %f\n",
	  x1_, x2_, y1_, y2_);
  g_print("our pixel size is x1: %d x2: %d y1: %d y2: %d\n",
	  x1, x2, y1, y2);
  g_print("our buffer size is %d x %d\n", bw_, bh_);

  // 0,0 on our buffer_ is x1, y1 on the canvas.

  int bx = 0;
  int by = 0;

  // width/height to copy
  int rw = bw_;
  int rh = bh_;

  // target drawable points
  int tx = x1 - x;
  int ty = y1 - y;

  if (tx < 0)
    {
      bx =  -tx;
      rw -= bx;
      tx =  0;
    }
  else
    {
      w -= tx;
      if (rw > w) rw = w;
    }

  if (ty < 0)
    {
      by =  -ty;
      rh -= by;
      ty =  0;
    }
  else
    {
      h -= ty;
      if (rh > h) rh = h;
    }
  

#ifdef GNOME_ENABLE_DEBUG

  g_print(" Copying a %d x %d area from pixmap %d,%d to target %d,%d\n",
          rw, rh, bx, by, tx, ty);
         
#endif

  gdk_draw_pixmap(drawable,
		  pixmap_gc_,
		  buffer_,
		  // buffer points
		  bx,
		  by,
		  // points on the target drawable
		  tx,
		  ty,
		  // don't draw more buffer than we have
		  rw, 
		  rh);

#ifdef GNOME_ENABLE_DEBUG
  // draw red box around redraw region
  Gdk_Color c;
  c.set("red");
  gdk_gc_set_foreground(pixmap_gc_, c.color());
  gdk_draw_rectangle(drawable, pixmap_gc_, FALSE, 
		     tx, ty, rw-1, rh-1);

#endif

}

double 
CanvasDrawable::point_impl(double x, double y, 
			  int cx, int cy, 
			  GnomeCanvasItem** actual_item)
{
  *actual_item = gtkobj();

  double dx, dy;
  if ((x >= x1_) && (y >= y1_) && (x <= x2_) && (y <= y2_)) {
    return 0.0;
  }

  /* Point is outside */

  if (x < x1_)
    dx = x1_ - x;
  else if (x > x2_)
    dx = x - x2_;
  else
    dx = 0.0;

  if (y < y1_)
    dy = y1_ - y;
  else if (y > y2_)
    dy = y - y2_;
  else
    dy = 0.0;

  return sqrt (dx * dx + dy * dy);
}

gint 
CanvasDrawable::event_impl(GdkEvent* event)
{
  g_print("Drawable event\n");
  return FALSE; // for now
}

void 
CanvasDrawable::color_changed_callback(const Colored& c)
{
  queue_buffer_draw();
}

