// -*- C++ -*-

/* 
 * messages.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "messages.h"

struct cb_data {
  Receiver* rec;
  const char* id;
};

static void 
activate_cb(struct cb_data cd) 
{
  cd.rec->send(cd.id);
}
  
Gtk_MenuItem* 
Receiver::make_menuitem(const Message & m)
{
  Gtk_MenuItem* mi = new Gtk_MenuItem(m.menu_name);
 
  struct cb_data cd = { this, m.id };
 
  connect_to_function(mi->activate,
                      &activate_cb,
                      cd);
  
  return mi;
}

class FreeItemsMenu : public Gtk_Menu {
public:
  // funny name so it doesn't get called by accident
  void free_append(Gtk_MenuItem* child) {
    deleteme.push_back(child);
    Gtk_Menu::append(*child);
  }

  virtual ~FreeItemsMenu() {
    vector<Gtk_MenuItem*>::iterator i = deleteme.begin();
    while (i != deleteme.end()) {
      (*i)->hide();
      delete *i;
      ++i;
    }
  }

private:
  vector<Gtk_MenuItem*> deleteme;

};

Gtk_Menu*     
Receiver::make_menu    (const_iterator b, const_iterator e)
{
  FreeItemsMenu* fim = new FreeItemsMenu;

  while (b != e) {
    Gtk_MenuItem* mi = make_menuitem(*b);

    fim->free_append(mi);
    
    ++b;
  }

  return fim;
}
