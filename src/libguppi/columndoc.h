// -*- C++ -*-

/* 
 * columndoc.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_COLUMNDOC_H
#define GUPPI_COLUMNDOC_H

// ColumnDoc is just a wrapper around DataSet that 
//  generates signals. Its name is stupid and a relic
//  of an earlier era of Guppi; should be renamed ScalarData soon.

// Eventually we will have to do better; we need an abstract base,
//  and at least two subclasses - one a wrapper around DataSet,
//  one a wrapper around a CORBA object.

#include <goose/DataSet.h>

#include "named.h"

class ColumnDoc : public Named
{
public:
  virtual ~ColumnDoc() {}

  // Casting away the const qualifies you to be 
  //  tortured slowly. It may not be the main 
  //  representation kept by the ColumnDoc.
  // These are not guaranteed to remain valid;
  //  they have to be used immediately and not saved 
  //  anywhere unless you copy them.
  virtual const DataSet& ds() const = 0;
  virtual const double*  data() const = 0;
  virtual const double*  data(size_t start, size_t count) const = 0;

  // This one is a hack. If the DataSet can be modified directly,
  //  we return it. That allows optimizations for native DataSet 
  //  operations on a DataSetColumn (see below). If modifying a 
  //  DataSet would do nothing, this function returns 0.
  // You must call data_changed() manually after you change the DataSet.
  // Purposely has inconsistent return value with the const version; keeps
  //  them separate.
  virtual DataSet* ds() = 0;

  virtual void set(size_t i, double) = 0;
  virtual void set(size_t i, const double*, size_t count) = 0;
  virtual void set(const DataSet& ds) = 0;

  virtual double element(size_t i) const = 0;

  virtual void clear() = 0;

  // Some common operations are provided directly,
  //  in case creating a dataset is inefficient.
  // These are all simple to compute so any subclass
  // can do it.
  virtual size_t   size() const = 0;
  virtual double   min() const = 0;
  virtual double   max() const = 0;
  virtual double   range() const = 0;

  // Instead of a copy constructor, so it can 
  //  be virtual and work with weirdo columndoc types.
  virtual ColumnDoc* dup() const = 0;

  class View : public Named::View {
  public:
    virtual void change_data(const ColumnDoc& cd) = 0;
    
  };

  class Model : public Model<ColumnDoc::View*> {
  public:
    void data_changed(const ColumnDoc& cd) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_data(cd);
	++i;
      }
      unlock_views();
    }

  };
  
  Model columndoc_model;

protected:
  ColumnDoc(const gchar* default_name) : Named(default_name) {}

  ColumnDoc(const ColumnDoc&);
  ColumnDoc& operator=(const ColumnDoc&);
};

class DataSetColumn : public ColumnDoc
{
public:
  DataSetColumn();
  // DataSetColumn will delete this DataSet.
  DataSetColumn(DataSet & ds);
  virtual ~DataSetColumn();

  virtual const DataSet& ds() const;

  virtual const double*  data() const;
  virtual const double*  data(size_t start, size_t count) const;

  virtual DataSet* ds();

  virtual void set(size_t i, double);
  virtual void set(size_t i, const double*, size_t count);
  // Note: since dataset does the whole refcounting thing, 
  //  this could cause unexpected behavior with loadable modules.
  virtual void set(const DataSet& ds);

  virtual double element(size_t i) const;

  virtual void clear();

  virtual size_t   size() const;
  virtual double   min() const;
  virtual double   max() const;
  virtual double   range() const;

  virtual ColumnDoc* dup() const; 

private:

  // "owned" by the DataSetColumn for memory management purposes.
  DataSet* data_;
};


#endif

