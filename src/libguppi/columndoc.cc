// -*- C++ -*-

/* 
 * columndoc.cc 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "columndoc.h"
#include <libgnome/libgnome.h>

DataSetColumn::DataSetColumn() : ColumnDoc(_("Untitled Column")), 
  data_(0)
{
  g_warning("Creating DataSetColumn with no DataColumn, not sure this works.");

}

DataSetColumn::DataSetColumn(DataSet & ds) : ColumnDoc(_("Untitled Column")),
  data_(&ds)
{
  
}

DataSetColumn::~DataSetColumn()
{
  if (data_ != 0) delete data_;
}

const DataSet& 
DataSetColumn::ds() const
{ 
#ifdef GNOME_ENABLE_DEBUG
  if (data_ == 0) { g_warning("Null DataSet"); };
#endif
  return *data_; 
}

const  double*  DataSetColumn::data() const { return data_->data(); }
const  double*  DataSetColumn::data(size_t start, size_t count) const { 
  // Returns more than requested, but user never knows the difference.
  return &((data_->data())[start]); 
}

DataSet* 
DataSetColumn::ds()
{
  return data_;
}

void DataSetColumn::set(size_t i, double x) 
{
  data_->set(i,x);
  columndoc_model.data_changed(*this);
}

void 
DataSetColumn::set(size_t i, const double* xs, size_t count)
{
  data_->set(i,xs,count);
  columndoc_model.data_changed(*this);
}

void 
DataSetColumn::set(const DataSet& ds)
{
  *data_ = ds;
}

double 
DataSetColumn::element(size_t i) const
{
  return (data_->data())[i];
}

void 
DataSetColumn::clear()
{
  data_->clear();
}

size_t   DataSetColumn::size() const { return data_->size(); }
double   DataSetColumn::min() const { return data_->min(); }
double   DataSetColumn::max() const { return data_->max(); }
double   DataSetColumn::range() const { return data_->range(); }

ColumnDoc*
DataSetColumn::dup() const {
  g_return_val_if_fail(data_ != 0, 0);
  DataSet* ds = new DataSet(*data_);
  ColumnDoc* cd = new DataSetColumn(*ds);
  return cd;
}






