// -*- C++ -*-

/* 
 * arraydoc.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_ARRAYDOC_H
#define GUPPI_ARRAYDOC_H

#include <vector>

#include <gtk--plot.h>

#include "plotcontext.h"
#include "plotdoc.h"
#include "axis.h"
#include "layer.h"

class ArrayDoc : public PlotDoc {
public:
  ArrayDoc(PlotContext& pc);
  virtual ~ArrayDoc();

  PlotContext& pc() { return pc_; }

  // rows/columns of plots in the array of plots
  size_t rows() const { return cells_.rows(); }
  size_t columns() const { return cells_.columns(); }

  // Sure wish one could overload operator[][]
  LayerStack* get_layer(size_t row, size_t col);

  const LayerStack* get_layer(size_t row, size_t col) const;

  Axis* get_axis(size_t row, size_t col, Axis_Base::alignment_t a);

  const Axis* get_axis(size_t row, size_t col, Axis_Base::alignment_t a) const;

  // This will not delete stuff if it shrinks the array; you need to do that 
  //  first.
  void resize(size_t newrows, size_t newcols);

  // ArrayDoc will delete anything it contains at destruct time; 
  //  it will not delete things that are replaced.

  void set_axis(size_t row, size_t col, Axis* axis, Axis_Base::alignment_t a);

  void set_layer(size_t row, size_t col, LayerStack* layerstack);

  virtual LayerStack* make_layerstack() = 0;

  virtual Plot*       make_plot  () = 0;

  class View : public PlotDoc::View
  {
  public:
    // passes new size
    virtual void change_size (size_t r, size_t c) = 0;
    // passes location of added/removed plot
    virtual void change_layer(size_t r, size_t c) = 0;
    // passes location of added/removed axis
    virtual void change_axis (size_t r, size_t c, 
			      Axis_Base::alignment_t a) = 0;
    
  };

  class Model : public Model<ArrayDoc::View*> {
  public:
    void size_changed(size_t r, size_t c) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_size(r,c);
	++i;
      }
      unlock_views();
    }
    void layer_changed(size_t r, size_t c) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_layer(r,c);
	++i;
      }
      unlock_views();
    }
    void axis_changed(size_t r, size_t c, 
		      Axis_Base::alignment_t a) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_axis(r,c,a);
	++i;
      }
      unlock_views();
    }
  };
  
  Model arraydoc_model;

protected:  
  class Cell {
  public:
    Cell();
    ~Cell();

    void clear();

    LayerStack* layerstack;
    Axis* axes[4];
  };

private:
  PlotContext & pc_;

  Grid<Cell*> cells_;
};


#endif


