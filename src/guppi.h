// -*- C++ -*-

/* 
 * guppi.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_GUPPI_H
#define GUPPI_GUPPI_H


/// This is a toplevel object that can be thought of as a 
/// "GuppiDoc manager." It keeps track of the GuppiDocs
/// and is reponsible for deleting them, and GuppiDocs are in 
/// turn responsible for managing the sub-document types.

#include <list>

#include <gnome.h>

#include "../config.h"

#include "guppidoc.h"

class Guppi {
public:
  Guppi();
  virtual ~Guppi();

  void new_doc();
  void close_doc(GuppiDoc* doc);
  void save_doc(GuppiDoc* doc);

  // exit the program.
  void exit(); 

  void about();

#if 0  
  void session_die();
  void session_save_state(Gnome_Client* client, gint phase,
			  GnomeRestartStyle save_style, gint shutdown,
			  GnomeInteractStyle interact_style,
			  gint fast, gpointer client_data);
#endif
  static const gchar* APPNAME;

  void new_doc(GuppiDoc* doc, const gchar * geometry = 0);

private:
  // Keep up with our documents
  list<GuppiDoc*> docs_;
  void add_doc(GuppiDoc* gd) { 
    docs_.push_back(gd); 
  }
  void remove_doc(GuppiDoc* gd);
};


// Global Guppi object oversees everything.
extern Guppi big_fish;

#endif
