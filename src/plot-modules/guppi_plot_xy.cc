// -*- C++ -*-

/* 
 * guppi_plot_xy.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "xydoc.h"
#include "plotplugin.h"

class XYPlugin : public PlotPlugin, public PlotDoc::View {
public:
  XYPlugin() {}
  virtual ~XYPlugin() {}

  virtual const char* get_menu_name() const { return _("2D"); }

  virtual const char* get_name() const { return _("Two Dimensional Plotting"); }

  virtual const char* get_description() const { return _("Scatter and line plots in two dimensions"); }

  // Make the PlotDoc type the plugin provides.
  virtual PlotDoc* make_plotdoc(PlotContext & pc) const { 
    ref(); // the PlotDoc effectively holds a ref
    PlotDoc* pd = new XYDoc(pc); 

    // not interested in icon/name models
    //  (destroy_model() should be called only once -
    //   if we eventually add other models, need to fix that)
    pd->plotdoc_model.add_view(const_cast<XYPlugin&>(*this));

    // we are never going to remove_view(), because we must
    //  last longer than the plotdoc.

    return pd;
  }

  virtual bool unloadable() const { 
    return refcount_ == 0;
  }


  // View interface

  void destroy_model() {
#ifdef GNOME_ENABLE_DEBUG
    g_print("PlotDoc model destroyed in XYPlugin\n");
#endif
    unref();
  }

  // Useless to us, but we have to implement
  //  the whole view.
  void change_name(const string & name) {}
  void change_icon(const string & icon) {}


protected:
  virtual unsigned int plugin_version() const { return GUPPI_PLUGIN_INTERFACE_VERSION; }

};

static XYPlugin* global_plot_plugin = 0;

#include <gmodule.h>

// This prevents symbol mangling.
extern "C" 
{
  // Symbol the Guppi module loader will look for.
G_MODULE_EXPORT const Plugin* init_plugin() 
{
  // Can't just use a global; it segfaults on unload, 
  //  due to some _fini routine the compiler or linker 
  //  presumably adds.
  if (global_plot_plugin == 0) global_plot_plugin = new XYPlugin;
  global_plot_plugin->ref();
  return global_plot_plugin;
}

G_MODULE_EXPORT void
g_module_unload (GModule *module)
{
#ifdef GNOME_ENABLE_DEBUG
  g_print ("Unloading XY plot plugin.\n");
#endif
  if (global_plot_plugin) {
    g_assert(global_plot_plugin->unloadable());
    delete global_plot_plugin;
  }
}

}

