// -*- C++ -*-

/* 
 * axisedit.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <gnome--.h>

#include <list>
#include <float.h> // Someday, <limits> can replace this

#include "axisedit.h"

class NumericAxisEditImpl : public Gtk_VBox {
public:
  NumericAxisEditImpl(NumericAxisEdit & nae,
		      NumericAxis & ca,
		      bool removable);
  ~NumericAxisEditImpl() { delete startadj_; delete stopadj_; }

  Signal0 remove;

private:
  void change_name(const Axis & a);
  void change_size(const NumericAxis & a);

  void name_entry_activate();
  void start_changed();  
  void stop_changed();

  void show_size();
  
  NumericAxis & na_;

  Gtk_Entry      name_entry_;

  Gtk_HBox       rangebox_;

  Gtk_Label      startlabel_, stoplabel_;

  // Have to be on the heap so we can change them
  Gtk_Adjustment* startadj_, * stopadj_;
  
  Gtk_SpinButton start_, stop_;

  // since we want to set the value and also respond to the 
  //  value being set by others, we need to switch off 
  //  the set when we change the spinbutton adjustment or 
  //  change the Axis
  bool changingstart_, changingstop_;

  Gtk_Button     remove_;
};

NumericAxisEdit::NumericAxisEdit(NumericAxis & na,
				 bool removable) 
  : Gtk_Wrapper(new NumericAxisEditImpl(*this,na,removable))
{

}

NumericAxisEdit::~NumericAxisEdit()
{

}

NumericAxisEditImpl::NumericAxisEditImpl(NumericAxisEdit & nae,
					 NumericAxis & na,
					 bool removable)
  : Gtk_VBox(TRUE,2), 
    
    na_(na),

    name_entry_(),

    rangebox_(FALSE,2),
    
    startlabel_(_("Start")),
    stoplabel_(_("Stop")),
    
    startadj_(new Gtk_Adjustment(0.0,0.0,0.0)), 
    stopadj_ (new Gtk_Adjustment(0.0,0.0,0.0)),  
    
    start_(*startadj_),
    stop_(*stopadj_),
    
    changingstart_(false),
    changingstop_(false),

    remove_(_("Remove"))
{
  set_border_width(1);

  name_entry_.set_text(na_.get_name());
  pack_start(name_entry_,TRUE,TRUE);

  pack_end(rangebox_);

  rangebox_.pack_start(startlabel_,FALSE,FALSE);
  rangebox_.pack_start(start_,TRUE,TRUE);
  rangebox_.pack_end(remove_,FALSE,FALSE);
  rangebox_.pack_end(stop_,TRUE,TRUE);
  rangebox_.pack_end(stoplabel_,FALSE,FALSE);

  // should be more intelligent.
  start_.set_digits(3);
  stop_.set_digits(3);

  show_size();

  remove_.set_sensitive(removable);

  connect_to_method(remove,
		    &nae,
		    &NumericAxisEdit::remove_cb);
  
  connect_to_signal(remove_.clicked,
		    remove);

  connect_to_method(na_.name_changed,
		    this,
		    &NumericAxisEditImpl::change_name);
		   
  connect_to_method(na_.size_changed,
		    this,
		    &NumericAxisEditImpl::change_size);

  connect_to_method(name_entry_.activate,
		    this,
		    &NumericAxisEditImpl::name_entry_activate);

  connect_to_method(start_.changed,
		    this,
		    &NumericAxisEditImpl::start_changed);

  connect_to_method(stop_.changed,
		    this,
		    &NumericAxisEditImpl::stop_changed);

  // this behavior is really wrong for a standalone widget,
  // but should be OK
  show_all();
}

void 
NumericAxisEditImpl::change_name(const Axis & a)
{
  name_entry_.set_text(a.get_name().c_str());
}

void 
NumericAxisEditImpl::change_size(const NumericAxis & a)
{
  show_size();
}

void
NumericAxisEditImpl::name_entry_activate()
{
  na_.set_name(name_entry_.get_text());
}

void 
NumericAxisEditImpl::start_changed()
{
  if (changingstart_) return; // prevent infinite loop
  changingstart_ = true;
  gdouble v = start_.get_value_as_float();
  na_.set_start(v); // hidden recursive call
  changingstart_ = false;
}

void 
NumericAxisEditImpl::stop_changed()
{
  if (changingstop_) return;  // prevent infinite loop
  changingstop_ = true;
  gdouble v = stop_.get_value_as_float();
  na_.set_stop(v); // hidden recursive call
  changingstop_ = false;
}

void
NumericAxisEditImpl::show_size()
{
  if (changingstart_ || changingstop_) return;  // prevent infinite loop

  // hmm, perhaps we only need one of these.
  changingstart_ = true;
  changingstop_ = true;

  // I am very unhappy with this; need a Gtk_Adjustment that takes
  //  a double, and need a way to set better defaults. 
  double axis_min = na_.has_min() ? na_.get_min() : -(FLT_MAX);
  double axis_max = na_.has_max() ? na_.get_max() : FLT_MAX;

  Gtk_Adjustment* tmp1 = new Gtk_Adjustment(na_.get_start(), axis_min,
					    axis_max, 1.0, 5.0, 1.0);
  
  Gtk_Adjustment* tmp2 = new Gtk_Adjustment(na_.get_start(), axis_min,
					    axis_max, 1.0, 5.0, 1.0);
  
  // random numbers.
  tmp1->set_value(0);
  tmp2->set_value(10);

  start_.set_adjustment(*tmp1);
  stop_.set_adjustment(*tmp2);

  delete startadj_;
  delete stopadj_;

  startadj_ = tmp1;
  stopadj_  = tmp2;

  start_.set_value(na_.get_start());
  stop_.set_value(na_.get_stop());

  changingstart_ = false;
  changingstop_ = false;
}
