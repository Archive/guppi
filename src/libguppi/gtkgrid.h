// -*- C++ -*-

/* 
 * gtkgrid.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_GTKGRID_H
#define GUPPI_GTKGRID_H

#include <gnome--.h>
#include <gtk--plot.h>

// Dynamic table of sorts; badly implemented.
// Needs re-writing for real. Maybe just use a bunch of boxes?
//  Maybe use Gtk_Fixed?


// The grid owns any widget you put in it, and will delete it. This
//  is because the grid is meant to be a dynamic widget; you probably
//  won't want to put locally allocated objects into it, only 
//  dynamically generated ones.


// Actually calling Gtk_Table methods will surely bomb; 
//  plus eventually I don't want to use it. So ignore the
//  Gtk_Table part of the hierarchy.
class Gtk_Grid : public Gtk_Table {
public:
  Gtk_Grid();
  ~Gtk_Grid();

  void reserve(size_t r, size_t c);

  void add(Gtk_Widget* w, size_t row, size_t column);

  void delete_row(size_t row);
  void delete_column(size_t column);
  
  // eventually these should support inserts.
  void add_row();
  void add_column();

  size_t rows() { return children_.rows(); }
  size_t columns() { return children_.columns(); }
  
private:
  Grid<Gtk_Widget*> children_;

  void nuke  (size_t row, size_t column);
  void shift_row(size_t r);
  void shift_column(size_t c);
  void reattach_below (size_t start_below);
  void shift_table_up (size_t start_below);
  void shift_table_left(size_t start_rightof);
  void reattach(size_t r, size_t c);
};


#endif
