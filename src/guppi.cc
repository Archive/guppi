// -*- C++ -*-

/* 
 * guppi.cc 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <algo.h>
#include <fstream>
#include <signal.h> // portability issues here need fixing.

#include "guppi.h"
#include "app.h"
#include "datatypes.h"
#include "datatypestore.h"
#include "plugins.h"

void
signal_handler (int sig_num);

Guppi::Guppi()
{

}

Guppi::~Guppi()
{

}

const gchar* Guppi::APPNAME = "guppi";

//#if 0

void
Guppi::new_doc(GuppiDoc* doc, const gchar* geometry)
{
  g_return_if_fail(doc != 0);

  add_doc(doc);
  GuppiApp* a = new GuppiApp(doc,
			     geometry);
  a->show();
}

// creates an empty document. (for now with test data)
void 
Guppi::new_doc()
{
  GuppiDoc* doc = new GuppiDoc;
  
  new_doc(doc,0);
}

void
Guppi::close_doc(GuppiDoc* doc)
{
  g_return_if_fail(doc != 0);

  if (doc->need_save()) {
    g_warning("Doc not saved, oops!"); // FIXME
  }

  if (docs_.size() == 1) {
    // this is the last window
    exit();
  }

  remove_doc(doc);
  doc->delete_self();
}

void
Guppi::save_doc(GuppiDoc* doc)
{
  Gnome::ok_dialog(_("Load and save not implemented."));

  doc->saved();
}

void
Guppi::exit()
{
  // fixme - check for unsaved documents, do modal dialogs 
  //  for each.
  g_warning("Exit doesn't check for unsaved documents yet.");

  list<GuppiDoc*>::iterator i = docs_.begin();
  while (i != docs_.end()) {
    delete *i;
    ++i;
  }

  Gtk_Main::instance()->quit();
}

void
Guppi::about()
{
  const gchar * authors [] = { "Jon Trowbridge <trow@emccta.com>",
			       "Havoc Pennington <hp@pobox.com>",
			       NULL };

  // could be NULL, but it's OK.
  gchar * logo = gnome_pixmap_file("guppi.xpm");

  Gnome_About * about = 
    new Gnome_About("Guppi: The GNU/GNOME Useful Plot Production Interface",
		    VERSION,
		    "Copyright (C) 1998 EMC Capital Management Inc.,"
		    " GNU General Public License.",
		    authors,
		    _("Guppi allows you to create several different kinds of plots "
		      "for data visualization."),
		    logo );

  g_free(logo);

  about->show();

  g_warning("Delete the about dialog. FIXME");
}

#if 0
void
Guppi::session_die()
{

}

void 
Guppi::session_save_state(Gnome_Client* client, gint phase,
			  GnomeRestartStyle save_style, gint shutdown,
			  GnomeInteractStyle interact_style,
			  gint fast, gpointer client_data)
{


}
#endif


void
Guppi::remove_doc(GuppiDoc* doc)
{
  list<GuppiDoc*>::iterator i;
  i = find(docs_.begin(), docs_.end(), doc);
  if (i != docs_.end()) {
    docs_.erase(i);
  }
#ifdef G_ENABLE_CHECKS
  else {
    g_warning("Didn't find doc in remove_doc\n");
  }
#endif
}

/*************************** 
 Globals
 **********/
			   
static list<gchar*> start_geometries;
static list<gchar*> start_files;

//#endif

Guppi big_fish;

int
main(int argc, char * argv[])
{
  // Initialize the i18n stuff
  bindtextdomain (PACKAGE, GNOMELOCALEDIR);
  textdomain (PACKAGE);

  Gnome_Main gtkmain(Guppi::APPNAME, VERSION, argc, argv);

  // this isn't really useful now, but someday maybe.
  //  for now it just catches segfaults.
  signal (SIGHUP, signal_handler);
  signal (SIGINT, signal_handler);
  signal (SIGQUIT, signal_handler);
  signal (SIGABRT, signal_handler);
  signal (SIGBUS, signal_handler);
  signal (SIGSEGV, signal_handler);
  signal (SIGPIPE, signal_handler);
  signal (SIGTERM, signal_handler);
  signal (SIGFPE, signal_handler);

  if (g_module_supported()) {

#if 0  
    // filename args ignored for now.
    list<gchar*>::const_iterator igeometries;
    igeometries = start_geometries.begin();
    while (igeometries != start_geometries.end()) {
      big_fish.new_app(*igeometries);
      ++igeometries;
    }
#endif

    register_all_builtin_datatypes(global_datatype_store);

    vector<const gchar*> module_filenames;

    // Load in the plugins.

    char* err = global_plugin_registry.register_dir(GUPPI_PLUGIN_DIR);
    if (err) {
      gnome_error_dialog(err);
#ifdef GNOME_ENABLE_DEBUG
      g_warning(err);
#endif
      g_free(err);
      err = 0;
    }

    // eventually GNOME_ENABLE_DEBUG this
    if (g_file_exists("./guppi.cc")) {

      // Load in plugins in the src directory also
      err = global_plugin_registry.register_dir("./plot-modules/.libs");
      if (err) {
	gnome_error_dialog(err);
#ifdef GNOME_ENABLE_DEBUG
	g_warning(err);
#endif
	g_free(err);
        err = 0;
      }
      
      err = global_plugin_registry.register_dir("./import-modules/.libs");
      if (err) {
	gnome_error_dialog(err);
#ifdef GNOME_ENABLE_DEBUG
	g_warning(err);
#endif
	g_free(err);
        err = 0;
      }

      err = global_plugin_registry.register_dir("./transform-modules/.libs");
      if (err) {
	gnome_error_dialog(err);
#ifdef GNOME_ENABLE_DEBUG
	g_warning(err);
#endif
	g_free(err);
        err = 0;
      }
    }

    big_fish.new_doc();
  } // g_module_supported()
  else {
    gnome_error_dialog(_("Platform doesn't support dynamic module loading;\n"
                         " can't use Guppi."));
    // FIXME connect a function to the "close" signal which quits the program.
  } // !g_module_supported()

  try {
    gtkmain.run();
  }
  catch (Exception & e) {
    cerr << "Unhandled exception: " << e.what() << endl;
    exit(1);
  }

  exit(0);
}

void
signal_handler (int sig_num)
{     
  // paranoia
  static bool recursed = false;
  if (recursed) {
    _exit(1);
  }
  recursed = true;

  fprintf(stderr,"Got %s", g_strsignal(sig_num));

  switch (sig_num)
    {
    case SIGHUP:
      fprintf(stderr,"  *** Guppi fatal signal, exiting...\n");
      break;
    case SIGINT:
      fprintf(stderr,"  *** Guppi fatal signal, exiting...\n");
      break;
    case SIGQUIT:
      fprintf(stderr,"  *** Guppi fatal signal, exiting...\n");
      break;
    case SIGABRT:
      fprintf(stderr,"  *** Guppi fatal signal, exiting...\n");
      break;
    case SIGBUS:
      fprintf(stderr,"  *** Guppi fatal signal, exiting...\n");
      break;
    case SIGSEGV:
      g_on_error_query("guppi");
      break;
    case SIGPIPE:
      fprintf(stderr,"  *** Guppi fatal signal, exiting...\n");
      break;
    case SIGTERM:
      fprintf(stderr,"  *** Guppi fatal signal, exiting...\n");
      break;
    case SIGFPE:
      fprintf(stderr,"  *** Guppi fatal signal, exiting...\n");
      break;
    default:
      fprintf(stderr,"  *** Guppi fatal signal, exiting...\n");
      break;
    }
  exit(1);
}

