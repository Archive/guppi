// -*- C++ -*-

/* 
 * canvasdrawable.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_CANVASDRAWABLE_H
#define GUPPI_CANVASDRAWABLE_H

/* Canvas item base class which provides a backing pixmap 
   and copies it to the canvas as needed. */

#include <gnome--.h>
#include <gtk--plot.h>

#include "colored.h"

class CanvasDrawable : public Gnome_CanvasItem,
		       public Colored
{
public:
  CanvasDrawable(Gnome_CanvasGroup & parent, const gchar* first_arg_name, ...);
  virtual ~CanvasDrawable();

protected:
  
  virtual void bounds_impl(double* x1, double* y1, double* x2, double* y2);

  virtual void reconfigure_impl();

  virtual void realize_impl();

  virtual void unrealize_impl();

  virtual void draw_impl(GdkDrawable* drawable, 
			 int x, int y, int w, int h);

  virtual double point_impl(double x, double y, 
			    int cx, int cy, 
			    GnomeCanvasItem** actual_item);

  virtual gint event_impl(GdkEvent* event);

  virtual void translate_impl(double x, double y) {
    x1_ += x;
    y1_ += y;
    x2_ += x;
    y2_ += y;
    reconfigure_impl();
  }

  // children need to call this if they change the size of the 
  //  item.
  void make_buffer();

  // Mark the pixmap for updating
  void queue_buffer_draw();

  virtual void draw_on_buffer(Plot_Area& pa) = 0;

  // Keep our size and position, in canvas item coordinates
  double x1_, y1_, x2_, y2_;

  // buffer dimensions
  int bw_, bh_;

private:
  // We keep one so we can render to the canvas's without
  //  expensive drawing calls.
  GdkPixmap* buffer_;
  GdkGC* pixmap_gc_;

  bool pixmap_needs_update_;

  virtual void color_changed_callback(const Colored& c);

};

#endif
