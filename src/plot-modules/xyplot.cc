// -*- C++ -*-

/* 
 * xyplot.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <gtk--plot.h>

#include "xyplot.h"

#ifdef GNOME_ENABLE_DEBUG
static const gchar* alignment_string(Axis_Base::alignment_t a)
{
  static const gchar* north = "North";
  static const gchar* south = "South";
  static const gchar* east =  "East";
  static const gchar* west =  "West";
  static const gchar* invalid = "**INVALID**";

  switch (a) {
  case Axis_Base::NORTH:
    return north;
  case Axis_Base::SOUTH:
    return south;
  case Axis_Base::EAST:
    return east;
  case Axis_Base::WEST:
    return west;
  default:
    return invalid;
  }
}
#endif



list<Gtk_MenuItem*>&
XYPlot::menu()
{
  return menu_;
}

XYPlot::XYPlot(XYDoc& xyd)
  : ArrayPlot(xyd)
{

}

XYPlot::~XYPlot()
{

}


