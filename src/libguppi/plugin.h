// -*- C++ -*-

/* 
 * plugin.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PLUGIN_H
#define GUPPI_PLUGIN_H

// A magic number.
#define GUPPI_PLUGIN_INTERFACE_VERSION 0

// A plugin should have an extern "C" symbol 
//  const Plugin* init_plugin() which inits the plugin and 
//  returns its implementation of the Plugin interface.
// It should be OK to call init_plugin more than once.

// The interface plugins must implement. Each type of 
//  plugin also has its own specific additions.
class Plugin {
public:
  Plugin() : refcount_(0) {}
  virtual ~Plugin() {}
  
  // Return false if this isn't a compatible plugin version.
  bool check_version() const;

  // Key used internally to classify the plugin (e.g. "plot")
  virtual const char* get_type() const = 0;

  // Name to display in menus (e.g. "2D")
  virtual const char* get_menu_name() const = 0;

  // Human-readable name of the plugin (e.g. "Two Dimensional Plotting")
  virtual const char* get_name() const = 0;

  // Longer one-liner describing the plugin (e.g. "Draws 2D scatter
  // and line plots.")
  virtual const char* get_description() const = 0;
  
  // OK to unload? - items can choose whether to factor 
  //  reference counting in to this.
  virtual bool unloadable() const = 0;

  // These determine whether the Plugin can be loaded or unloaded.
  // You should unref() when you finish with the plugin.
  void unref() const;

  // Done once automatically each time you request the plugin.
  //  (otherwise you'd have a race condition to do it fast enough
  ///  after the request.)
  void ref() const;

protected:
  // Each plugin returns a value here which should match the 
  //  Guppi one.
  virtual unsigned int plugin_version() const = 0;

  mutable int refcount_;

private:
  static unsigned int guppi_plugin_version;
};

#include <gmodule.h>

// Load a plugin. Return non-0 if error. must free all three return vals 
// (error, plugin, module)
char*
load_plugin(const char* filename, const Plugin** plugin, GModule** handle);

#endif
