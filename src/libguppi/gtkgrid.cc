// -*- C++ -*-

/* 
 * gtkgrid.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "gtkgrid.h"

Gtk_Grid::Gtk_Grid()
  : Gtk_Table(0,0,true)
{
  
}

Gtk_Grid::~Gtk_Grid()
{
  size_t i = 0;
  size_t nr = children_.rows();
  while ( i < nr ) {
    size_t j = 0;
    size_t nc = children_.columns();
    while ( j < nc ) {
      Gtk_Widget* w = children_[i][j];
      if (w != 0) {
	w->hide();
	delete w;
      }
      ++j;
    }
    ++i;
  }
}

void
Gtk_Grid::reserve(size_t r, size_t c)
{
  resize(r,c);
  children_.reserve(r,c);
}

void 
Gtk_Grid::add(Gtk_Widget* w, size_t r, size_t c)
{
  g_return_if_fail ( children_[r][c] == 0 );
  g_return_if_fail ( w != 0 );
  g_return_if_fail(r < children_.rows());
  g_return_if_fail(c < children_.columns());

  size_t rows = children_.rows();

  children_[r][c] = w;

  // This makes our grid start on the bottom
  g_print("Widget %d,%d in a %dx%d grid, attaching at left: %d right: %d top: %d bottom: %d\n",
	  r,c,rows,children_.columns(),c,c+1,rows-r-1,rows-r);
  attach(*w, c, c+1, rows-r-1, rows-r);
}

void 
Gtk_Grid::reattach(size_t r, size_t c)
{
  size_t rows = children_.rows();

  Gtk_Widget* w = children_[r][c];
  if (w == 0) return;
  
  // This results in a search through the list of table
  //  children; which makes the add_row procedure
  //  *incredibly* inefficient from an algorithmic standpoint.
  //  Since the numbers are small hopefully we can get away with it.
  remove(w);

  // This makes our grid start on the bottom
  g_print("Widget %d,%d in a %dx%d grid, REattaching at left: %d right: %d top: %d bottom: %d\n",
	  r,c,rows,children_.columns(),c,c+1,rows-r-1,rows-r);
  // This magical calculation is assumed in too many places. Yuck.
  attach(*w, c, c+1, rows-r-1, rows-r);
}

void 
Gtk_Grid::nuke  (size_t row, size_t column)
{
  Gtk_Widget* w = children_[row][column];
  if (w != 0) {
    remove(w);
    w->hide();
    delete w;
    children_[row][column] = 0;
  }
}

void 
Gtk_Grid::shift_row(size_t r)
{
  g_warning("not implemented");  
}

void 
Gtk_Grid::shift_column(size_t c)
{
  g_warning("not implemented");  
}


void
Gtk_Grid::delete_row(size_t row)
{
  size_t i = 0;
  size_t cols = children_.columns();
  while ( i < cols ) {
    nuke(row,i);
    ++i;
  }
  shift_table_up(row);
  children_.delete_row(row);
}

void
Gtk_Grid::delete_column(size_t column)
{
  size_t i = 0;
  size_t rows = children_.rows();
  while ( i < rows ) {
    nuke(i,column);
    ++i;
  }
  shift_table_left(column);
  children_.delete_column(column);
}
  
void
Gtk_Grid::add_row()
{
  // only appends for now.
  size_t rows = children_.rows();
  resize(rows+1,children_.columns());
  children_.add_row();
  
  reattach_below(rows);
}

void 
Gtk_Grid::reattach_below (size_t start_below)
{
  size_t cols = children_.columns();
  size_t i = 0;
  while ( i < start_below ) {
    size_t j = 0;
    while ( j < cols ) {
      reattach(i,j);
      ++j;
    }
    ++i;
  }
}

void 
Gtk_Grid::shift_table_up (size_t start_below)
{
  size_t cols = children_.columns();
  size_t rows = children_.rows();
  // careful not to underflow the size_t -
  //  if start_below == 0 the loop shouldn't be entered.
  size_t i = start_below;
  while ( i > 0 ) {
    --i;

    size_t j = 0;
    while ( j < cols ) {
      Gtk_Widget* w = children_[i][j];
      if ( w != 0 ) {
	remove(w);
	attach(*w,j,j+1,rows-i-2,rows-i-1);
      }
      ++j;
    }
  }
  resize(rows-1,cols);
}

void 
Gtk_Grid::shift_table_left(size_t start_rightof)
{
  size_t cols = children_.columns();
  size_t rows = children_.rows();
  size_t i = start_rightof + 1;
  while ( i < cols ) {
    size_t j = 0;
    while ( j < rows ) {
      Gtk_Widget* w = children_[j][i];
      if ( w != 0 ) {
	remove(w);
	attach(*w,i-1,i,rows-j-1,rows-j);
      }
      ++j;
    }
    ++i;
  }
  resize(rows,cols-1);
}

void
Gtk_Grid::add_column()
{
  // Adds to the right so no shifting needed.
  resize(children_.rows(),children_.columns()+1);
  children_.add_column();
}

