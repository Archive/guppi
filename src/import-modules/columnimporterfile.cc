// -*- C++ -*-

/* 
 * columnimporterfile.cc 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <fstream>

#include <gnome--.h> // i18n

#include "columnimporterfile.h"
#include "textimport.h"

class ColumnImporterFileImpl {
public:
  ColumnImporterFileImpl(ColumnImporterFile & cif, ColumnImporterData cid);
  ~ColumnImporterFileImpl();

  list<ColumnDoc*> * get_docs(); 

private:
  ColumnImporterFile & cif_;

  ColumnImporterData cid_;

  void file_selected_cb();
  void fs_cancel_cb();
  gint fs_delete_cb(GdkEventAny* e);

  void tid_clicked_cb(gint button);
  gint tid_delete_cb(GdkEventAny* e);

  list<ColumnDoc*> * docs_;

  ImportEngine ie_;

  string text_; // being imported.

  Gtk_FileSelection* fs_;

  TextImportDialog* tid_;
};

ColumnImporterFile::ColumnImporterFile(ColumnImporterData cid)
  : impl_(new ColumnImporterFileImpl(*this, cid))
{

}

ColumnImporterFile::~ColumnImporterFile()
{
  delete impl_;
}

list<ColumnDoc*> *
ColumnImporterFile::get_docs()
{
  return impl_->get_docs();
}

ColumnImporterFileImpl::ColumnImporterFileImpl(ColumnImporterFile & cif,ColumnImporterData cid) :
  cif_(cif), cid_(cid), docs_(0), ie_(), 
  text_(""), fs_(0), tid_(0)
{
  fs_ = new Gtk_FileSelection(_("Import Column"));

  connect_to_method(fs_->get_ok_button()->clicked,
		    this,
		    &ColumnImporterFileImpl::file_selected_cb);

  connect_to_method(fs_->get_cancel_button()->clicked,
		    this,
		    &ColumnImporterFileImpl::fs_cancel_cb);
  
  connect_to_method(fs_->delete_event,
		    this,
		    &ColumnImporterFileImpl::fs_delete_cb);

  fs_->show();
}

ColumnImporterFileImpl::~ColumnImporterFileImpl()
{
  if (fs_ != 0) {
    fs_->hide();
    delete fs_;
  }
  if (tid_ != 0) {
    tid_->hide();
    delete tid_;
  }
  if (docs_ != 0) delete docs_;
} 
  
list<ColumnDoc*> * 
ColumnImporterFileImpl::get_docs()
{
  // could be 0!
  list<ColumnDoc*> * tmp = docs_;
  // We are no longer responsible for this pointer.
  docs_ = 0;
  return tmp;
}

void
ColumnImporterFileImpl::file_selected_cb()
{
  string filename = fs_->get_filename();
  
  fs_->hide();
  delete fs_;
  fs_ = 0;

  if (g_file_exists(filename.c_str())) {
    
    ifstream infile(filename.c_str());
    if (!infile) {
      g_warning("couldn't open file, arrgh");
    }
    else {
      char buf[4097];
      
      while (infile) {
	infile.read(buf,4096);
	buf[infile.gcount()] = '\0'; 
	text_ += buf;
      }
      
#ifdef GNOME_ENABLE_DEBUG
      //      cout << "Importing text: '" << text_ << "'" << endl;
#endif
      ie_.guess_columns(text_);
      tid_ = new TextImportDialog(text_,ie_,cid_.datatypes);
      connect_to_method(tid_->clicked,
			this,
			&ColumnImporterFileImpl::tid_clicked_cb);
      connect_to_method(tid_->delete_event,
			this,
			&ColumnImporterFileImpl::tid_delete_cb);
      tid_->show();
      //      Gnome::error_dialog("Temporarily de-implemented.");
    }      
  }
  else {
    g_warning(_("File not found: %s"), filename.c_str());
  }
}

void 
ColumnImporterFileImpl::fs_cancel_cb()
{
  fs_->hide();
  delete fs_;
  fs_ = 0;

  cif_.cancelled(&cif_);
}

gint 
ColumnImporterFileImpl::fs_delete_cb(GdkEventAny* e)
{
  fs_cancel_cb();
  return 1; // cancel should handle it.
}

void 
ColumnImporterFileImpl::tid_clicked_cb(gint button)
{
  switch(button) {
  case GNOME_OK:
    do {
      ie_.load(text_);
      g_return_if_fail(docs_ == 0);
      docs_ = new list<ColumnDoc*>;
      
      vector<DataSet*> results = ie_.import();
      
      vector<DataSet*>::iterator i = results.begin();
      while ( i != results.end() ) {
	docs_->push_back(new DataSetColumn(**i));
	++i;
      }
      cif_.finished(&cif_);
    } while(false);
    break;
  case GNOME_CANCEL:
    cif_.cancelled(&cif_);
    break;
  default:
    g_assert_not_reached();
  }
  

}

gint
ColumnImporterFileImpl::tid_delete_cb(GdkEventAny* e)
{
  cif_.cancelled(&cif_);
  return 0;
}

