// -*- C++ -*-

/* 
 * columnlist.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "columnlist.h"

#include "columnwindow.h"

#include "transformplugin.h"
#include "plugins.h"

#include <goose/Exception.h>

#include <algorithm>

///////////////// The column list itself

class ColumnListImpl : public Gtk_CList,
                       public ColumnDocList::View {
public:
  ColumnListImpl(ColumnDocList& cdl);
  ~ColumnListImpl();

  void add_column(const ColumnDoc& cd);
  void delete_column(const ColumnDoc& cd);
  // we're statically allocated in app.cc
  void destroy_model() { 
    if (cdl_) { 
      cdl_ = 0; 
    } 
  }

  void display_column_icon(const ColumnDoc & c);
  void display_column_name(const ColumnDoc & c);
  void display_column(const ColumnDoc & c);
  void display_all_columns();

  gint button_press_event_impl(GdkEventButton* be);

  void column_info();
  void column_delete();

  void column_transform_in_place(PluginHandle* ph);
  void column_transform_copy(PluginHandle* ph);

#ifdef GNOME_ENABLE_DEBUG
  void column_debug();
#endif

  class Row : public ColumnDoc::View {
  public:
    Row(ColumnListImpl & clist) 
      : clist_(clist), cd_(0)
      {

      }
    
    virtual ~Row() 
    {
      if (cd_) {
        cd_->columndoc_model.remove_view(*this);
        cd_->named_model.remove_view(*this);
      }
      
    }

    void set_column(const ColumnDoc& cd)
      {
        g_return_if_fail(cd_ == 0);
        cd_ = &cd;
        cd_->columndoc_model.add_view(*this);
        cd_->named_model.add_view(*this);
        
        redisplay();        
      }

    void change_name(const string & name) { redisplay(); }
    void change_data(const ColumnDoc & cd) { redisplay(); }
    void destroy_model() { 
      if (cd_) { 
        int pos = clist_.find_row_from_data((gpointer)cd_);
        if (pos >= 0) clist_.remove_row(pos);
        cd_ = 0; 
        clist_.add_to_freelist(this); 
      } 
    }
    
  private:
    ColumnListImpl& clist_;
    const ColumnDoc* cd_;

    void redisplay();
    
  };

  void add_to_freelist(Row* r) {
    vector<Row*>::iterator i = find(inuselist_.begin(), inuselist_.end(), r);
    if (i != inuselist_.end()) inuselist_.erase(i);
    freelist_.push_back(r);
  }

private:
  ColumnDocList* cdl_;

  Gtk_Menu popup_;
  Gtk_MenuItem mi_info_;
  Gtk_MenuItem mi_delete_;
  Gtk_MenuItem mi_transform_in_place_;
  Gtk_MenuItem mi_transform_copy_;    
  Gtk_Menu transform_in_place_;
  Gtk_Menu transform_copy_;
#ifdef GNOME_ENABLE_DEBUG
  Gtk_MenuItem mi_debug_;
#endif

  list<Gtk_MenuItem*> need_deleting_;

  // last one clicked.
  ColumnDoc* clicked_cd_;

  // To avoid having Rows delete themselves on column 
  //  destruction, we do this whole rigmarole.
  vector<Row*> freelist_;
  vector<Row*> inuselist_;
};

ColumnList::ColumnList(ColumnDocList& cdl)
  : impl_(0)
{
  set_policy(GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);
  impl_ = new ColumnListImpl(cdl);
  add(*impl_);
  impl_->show();
}

ColumnList::~ColumnList()
{
  impl_->hide();
  delete impl_;
}

ColumnListImpl::ColumnListImpl(ColumnDocList& cdl) 
  : Gtk_CList(6), cdl_(&cdl),  popup_(), mi_info_(_("Info...")),
    mi_delete_(_("Delete")), 
    mi_transform_in_place_(_("Transform")),
    mi_transform_copy_(_("Copy & Transform")),
#ifdef GNOME_ENABLE_DEBUG
    mi_debug_(_("Debug")),
#endif
    clicked_cd_(0)
{
  popup_.append(mi_info_);
  mi_info_.show();
  popup_.append(mi_delete_);
  mi_delete_.show();
#ifdef GNOME_ENABLE_DEBUG
  popup_.append(mi_debug_);
  mi_debug_.show();

  connect_to_method(mi_debug_.activate,
		    this,
		    &ColumnListImpl::column_debug);

  column_debug();
  column_debug();
  column_debug();
  column_debug();
  column_debug();
  column_debug();
#endif


  PluginRegistry::iterator i = global_plugin_registry.begin(TRANSFORM_PLUGIN_TYPE);
  PluginRegistry::iterator end = global_plugin_registry.end(TRANSFORM_PLUGIN_TYPE);

  while ( i != end ) {

    Gtk_MenuItem* mi;
    PluginHandle* ph = *i;
    g_assert(ph != 0);

    mi = new Gtk_MenuItem(ph->get_menu_name().c_str());
    transform_in_place_.append(*mi);
    need_deleting_.push_back(mi);
    connect_to_method(mi->activate,
		      this,
		      &ColumnListImpl::column_transform_in_place,
		      ph);
    mi->show();

    mi = new Gtk_MenuItem(ph->get_menu_name().c_str());
    transform_copy_.append(*mi);
    need_deleting_.push_back(mi);
    connect_to_method(mi->activate,
		      this,
		      &ColumnListImpl::column_transform_copy,
		      ph);
    mi->show();
    ++i;
  }

  popup_.append(mi_transform_in_place_);
  mi_transform_in_place_.show();

  mi_transform_in_place_.set_submenu(&transform_in_place_);

  popup_.append(mi_transform_copy_);
  mi_transform_copy_.show();
  mi_transform_copy_.set_submenu(&transform_copy_);

  connect_to_method(mi_info_.activate,
		    this,
		    &ColumnListImpl::column_info);

  connect_to_method(mi_delete_.activate,
		    this,
		    &ColumnListImpl::column_delete);

  column_titles_show();
  column_titles_passive();

  set_column_title(0,_("Column"));
  set_column_width(0,100);
  set_column_title(1,_("N"));
  set_column_title(2,_("Mean"));
  set_column_title(3,_("Std. Dev."));
  set_column_title(4,_("Maximum"));
  set_column_title(5,_("Minimum"));

  gint j = 1;
  while ( j < 5 ) {
    set_column_width(j,65);
    ++j;
  }

  display_all_columns();

  cdl_->columndoclist_model.add_view(*this);
}

ColumnListImpl::~ColumnListImpl()
{
  if (cdl_) {
    cdl_->columndoclist_model.remove_view(*this);
  }

  list<Gtk_MenuItem*>::iterator i = need_deleting_.begin();
  while (i != need_deleting_.end()) {
    delete *i;
    ++i;
  }

  vector<Row*>::iterator j = freelist_.begin();
  while (j != freelist_.end()) {
    delete *j;
    ++j;
  }

  j = inuselist_.begin();
  while (j != inuselist_.end()) {
    delete *j;
    ++j;
  }

}

void 
ColumnListImpl::add_column(const ColumnDoc& cd)
{
  Row* r = 0;
  if (!freelist_.empty()) {
    r = freelist_.back();
    freelist_.pop_back();
  }
  else {
    r = new Row(*this);
  }
  inuselist_.push_back(r);

  r->set_column(cd);
}

void 
ColumnListImpl::delete_column(const ColumnDoc& cd)
{
  // handled by the destroy_model method in Row
}

gint
ColumnListImpl::button_press_event_impl(GdkEventButton* be)
{
  gint row;
  gint col;
  if (get_selection_info(be->x,be->y,&row,&col) <= 0) 
    return Gtk_CList::button_press_event_impl(be);
      
  clicked_cd_ = (ColumnDoc *)get_row_data(row);

  if (clicked_cd_ != 0) {
    if (be->button == 3) {
      popup_.popup(0,0,be->button,be->time);
      return TRUE;
    }
    else if (be->type == GDK_2BUTTON_PRESS) {
      column_info();
      return TRUE;
    }
    else {
      return Gtk_CList::button_press_event_impl(be); 
    }
  }
  else {
    g_warning("Null data on row %d", row);
    return Gtk_CList::button_press_event_impl(be);
  }
}

void 
ColumnListImpl::display_all_columns()
{
  freeze();
  ColumnDocList::iterator i = cdl_->begin();
  while ( i != cdl_->end() ) {
    add_column(**i);
    ++i;
  }
  thaw();
}

// unfortunately you can't set data and name separately
void 
ColumnListImpl::Row::redisplay()
{
  g_return_if_fail(cd_ != 0);

  int pos;

  pos = clist_.find_row_from_data((gpointer)cd_);

  // If the number of columns in the CList increases this 
  //  has to increase as well.
  // I passionately hate string array interfaces. They make a mess
  //  like this one.
  const gchar* text[6];
  text[0] = cd_->get_name().c_str();

  // Each one needs its own buffer, since we 
  //  need all the buffers at once to stuff into the
  //  CList.
  gchar sizebuf[100];
  g_snprintf(sizebuf,100,"%u",cd_->ds().size());
  text[1] = &sizebuf[0];

  gchar meanbuf[100];
  g_snprintf(meanbuf,100,"%f",cd_->ds().mean());
  text[2] = &meanbuf[0];

  gchar sdevbuf[100];
  g_snprintf(sdevbuf,100,"%f",cd_->ds().sdev());
  text[3] = &sdevbuf[0];

  gchar maxbuf[100];
  g_snprintf(maxbuf,100,"%f",cd_->ds().max());
  text[4] = &maxbuf[0];
 
  gchar minbuf[100];
  g_snprintf(minbuf,100,"%f",cd_->ds().min());
  text[5] = &minbuf[0];  

  if ( pos != -1 ) {
    // icon already exists for this ColumnDoc - reinsert with new 
    //  characteristics.
    clist_.freeze();
    clist_.remove_row(pos);
    clist_.insert_row(pos,text);
    clist_.thaw();
  }
  else {
    // no icon yet, so add one.
    pos = clist_.append(text);
  }

  clist_.set_row_data(pos,(gpointer)cd_);
}


void
ColumnListImpl::column_info()
{
  g_return_if_fail(clicked_cd_ != 0);

  Gnome_Dialog* d = 0;

  try {
    d = new ColumnWindow(*clicked_cd_);
  }
  catch (Exception & e) {
    cerr << e.what() << endl;
    d = 0;
  }
  if (d) d->show();  
  clicked_cd_ = 0;
}

void 
ColumnListImpl::column_delete()
{
  g_return_if_fail(clicked_cd_ != 0);
  
  cdl_->delete_column(clicked_cd_);
  clicked_cd_ = 0;
}

void 
ColumnListImpl::column_transform_in_place(PluginHandle* ph)
{
  TransformPlugin* tp = (TransformPlugin*)ph->get_plugin();

  tp->transform(*clicked_cd_);
  clicked_cd_ = 0;
  
  tp->unref();
}

void 
ColumnListImpl::column_transform_copy(PluginHandle* ph)
{
  ColumnDoc* new_cd = clicked_cd_->dup();
  clicked_cd_ = 0;

  TransformPlugin* tp = (TransformPlugin*)ph->get_plugin();

  tp->transform(*new_cd);

  cdl_->add_column(*new_cd);
  
  tp->unref();
}


#ifdef GNOME_ENABLE_DEBUG
void
ColumnListImpl::column_debug()
{
  static int i = 0;

  DataSet* ds = new DataSet;
  DataSet* ds_close = new DataSet;
  static gdouble d = 0.02;
  gdouble end = d+800.0;
  while ( d < end ) {
    int r = rand();
    double v;
    if (r % 2 == 0) v = d + (double)r/(RAND_MAX/12.0);
    else v = d - (double)r/(RAND_MAX/15.0);
    ds->add(v);
    ds_close->add(d + rand()/(RAND_MAX/30.0));
    d += (20.73/(i+1));
  }
  ColumnDoc* cd = new DataSetColumn(*ds);
  ColumnDoc* cd_close = new DataSetColumn(*ds_close);

  switch (i) {
  case 0:
    cd->set_name("One");
    cd_close->set_name("One-ish");
    break;
  case 1: 
    cd->set_name("Two");
    cd_close->set_name("Two-ish");
    break;
  case 2: 
    cd->set_name("Three");
    cd_close->set_name("Three-ish");
    break;
  case 3: 
    cd->set_name("Four");
    cd_close->set_name("Four-ish");
    break;
  case 4: 
    cd->set_name("Five");
    cd_close->set_name("Five-ish");
    break;
  default:
    cd->set_name("Six");
    cd_close->set_name("Six-ish");
    i = -1;
    d -= 3053.0;
    break;
  }
  ++i;

  cdl_->add_column(*cd);
  cdl_->add_column(*cd_close);
}
#endif
