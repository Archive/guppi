// -*- C++ -*-

/* 
 * guppi_importer_column_file.cc 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "columnimporterfile.h"
#include "columnimportplugin.h"

#include <gtk--.h>

class FileImportPlugin : public ColumnImportPlugin {
public:
  // Name to display in menus (e.g. "2D")
  virtual const char* get_menu_name() const { return _("From file"); }

  // Human-readable name of the plugin (e.g. "Two Dimensional Plotting")
  virtual const char* get_name() const {
    return _("Text file importer");
  }

  // Longer one-liner describing the plugin (e.g. "Draws 2D scatter
  // and line plots.")
  virtual const char* get_description() const { 
    return _("Import data columns from text files."); 
  }
  
  // OK to unload?
  virtual bool unloadable() const {
    g_return_val_if_fail(outstanding_importers >= 0, false); 
    return outstanding_importers == 0 && refcount_ == 0;
  };

  virtual ColumnImporter* 
  make_importer(ColumnImporterData cid) const {
    ColumnImporterFile* cif = new ColumnImporterFile(cid);
    ++outstanding_importers;
    connect_to_method(cif->destroy,
		      (FileImportPlugin*)this,
		      &FileImportPlugin::importer_destroyed);
    return cif;
  }

protected:
  virtual unsigned int plugin_version() const { 
    return GUPPI_PLUGIN_INTERFACE_VERSION;
  }

private:
  void importer_destroyed() {
    // Need to be sure the destroy is complete before 
    //  we authorize unloading.
    connect_to_method(Gtk_Main::idle(),
		      (FileImportPlugin*)this,
		      &FileImportPlugin::decrement_outstanding);
  }

  gint decrement_outstanding() {
    --outstanding_importers;
    return 0;
  }


  static int outstanding_importers;
};

int FileImportPlugin::outstanding_importers = 0;

static FileImportPlugin* global_import_plugin = 0;

extern "C" {

    // Symbol the Guppi module loader will look for.
  const Plugin* init_plugin() 
{
  // Can't just use a global; it segfaults on unload, 
  //  due to some _fini routine the compiler or linker 
  //  presumably adds.
  if (global_import_plugin == 0) global_import_plugin = new FileImportPlugin;
  global_import_plugin->ref();
  return global_import_plugin;
}

  G_MODULE_EXPORT void
  g_module_unload (GModule *module)
{
#ifdef GNOME_ENABLE_DEBUG
  g_print ("Unloading text import plugin.\n");
#endif
  if (global_import_plugin) delete global_import_plugin;
}

	   }
