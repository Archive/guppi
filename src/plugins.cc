// -*- C++ -*-

/* 
 * plugins.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "plugins.h"
#include <libgnome/libgnome.h> // i18n and glib

// It is significant that we don't keep the Plugin& around;
//  we want to unload it.
PluginHandle::PluginHandle(GModule* module, const char* filename, const Plugin & plugin)
  : filename_(filename), 
    type_(plugin.get_type()),
    menu_name_(plugin.get_menu_name()), 
    name_(plugin.get_name()),
    description_(plugin.get_description()),
    handle_(module),
    plugin_(&plugin)
{
  g_assert(module != 0);
}

PluginHandle::~PluginHandle()
{
  if (handle_) {
    g_module_close(handle_);
  }
}

const string & 
PluginHandle::get_type() const
{
  return type_;
}


const string & 
PluginHandle::get_menu_name() const
{
  return menu_name_;
}

const string & 
PluginHandle::get_name() const
{
  return name_;
}

const string & 
PluginHandle::get_description() const
{
  return description_;
}

const Plugin* 
PluginHandle::get_plugin()
{
  if (handle_ == 0) {
    char* error = load();
    if (error) {
      g_warning(error); 
      g_free(error);
      return 0;
    }
  }
  
  return plugin_;
}

char* 
PluginHandle::load()
{
  g_return_val_if_fail(handle_ == 0, 0);
  g_return_val_if_fail(plugin_ == 0, 0);
  
  return load_plugin(filename_.c_str(), &plugin_, &handle_);
}

char* 
PluginHandle::unload()
{
  g_assert(handle_ != 0);
  g_assert(plugin_ != 0);
  g_assert(plugin_->unloadable());

  g_module_close(handle_);

  handle_ = 0;
  plugin_ = 0;
  return 0;
}

bool 
PluginHandle::loaded() const
{
  return handle_ != 0;
}

void 
PluginHandle::consider_unloading()
{
  if (handle_ == 0) return;
  
  if (plugin_->unloadable()) unload();
}

#include "plotplugin.h"
#include "columnimportplugin.h"
#include "transformplugin.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

PluginRegistry::PluginRegistry()
{

}

PluginRegistry::~PluginRegistry()
{
  iterator i = plot_plugins_.begin();
  while ( i != plot_plugins_.end() ) {
    delete *i;
    ++i;
  }
  i = column_importer_plugins_.begin();
  while ( i != column_importer_plugins_.end() ) {
    delete *i;
    ++i;
  }
  i = transform_plugins_.begin();
  while ( i != transform_plugins_.end() ) {
    delete *i;
    ++i;
  }
}

char*
PluginRegistry::register_dir(const char* dirname)
{
  g_return_val_if_fail(dirname != 0,0);

  struct stat sbuf;
  if (stat(dirname,&sbuf) == -1) {
    return g_copy_strings(_("Couldn't scan for plugins in "),
			  dirname,
			  "\n (", g_strerror(errno), ")", NULL);
  }
  
  if (!S_ISDIR(sbuf.st_mode)) {
    return g_copy_strings(_("Couldn't scan for plugins in "),
			  dirname,
			  "\n (not a directory)", NULL);
  }

  DIR* dir = opendir(dirname);

  if (dir == 0) {
    return g_copy_strings(_("Failed to open directory "),
			  dirname,
			  "\n (", g_strerror(errno), ")", NULL);
  }
  
  char* retval = 0;

  errno = 0;

  struct dirent* entry;
  while ((entry = readdir(dir))) {
    const char* filename = entry->d_name;
    
    static const char* first_part = "libguppi_";
    static const char* last_part  = ".so";

    if (strncmp(first_part, filename, strlen(first_part)) == 0) {
      size_t len = strlen(filename);
      g_assert(len > 3);
      if (strcmp(last_part, &filename[len-strlen(last_part)]) == 0) {
	// Candidate!
	char* fullname = g_concat_dir_and_file(dirname,filename);
	char* error = register_plugin(fullname);
	if (error) {
	  if (retval) {
	    char* tmp = g_copy_strings(retval,"\n",error,NULL);
	    g_free(retval);
	    g_free(error);
	    retval = tmp;
	  }
	  else {
	    retval = error;
	  }
	}
	g_free(fullname);
      }
    }
  }
  if (errno != 0) {
    g_warning(g_strerror(errno));
  }
  if (closedir(dir) == -1) {
    g_warning(g_strerror(errno));
  }
  return retval; 
}
  
char*
PluginRegistry::register_plugin(const char* filename)
{
  g_return_val_if_fail(filename != 0,0);
  
  GModule* module;
  const Plugin* plugin;

  char* error = load_plugin(filename, &plugin, &module);
  
  if (error) return error;
  else {
    g_assert(module != 0);
    g_assert(plugin != 0);

    const char* type = plugin->get_type();    

    vector<PluginHandle*>* v = type_to_vector(type);
    if (v != 0) {
      // Check for duplicates - not so efficient, but 
      //  efficient enough unless you have thousands of plugins.
      const string name = plugin->get_name();

      vector<PluginHandle*>::iterator i = v->begin();
      while ( i != v->end() ) {
	PluginHandle* old_ph = *i;
	g_assert(old_ph != 0); // may as well error check while we're at it

	if (old_ph->get_name() == name) {
	  gchar buf[256];
	  g_snprintf(buf,255,
		     _("Duplicate plugin `%s', ignoring all but the first instance.\n"),
		     name.c_str());
	  g_module_close(module);
	  return g_strdup(buf);
	}

	++i;
      }

      PluginHandle* ph = new PluginHandle(module, filename, *plugin);
      v->push_back(ph);
      plugin->unref();
      ph->consider_unloading(); // Unload if possible
    }
    else {
      gchar buf[256];
      g_snprintf(buf, 255, _("Unknown type %s for plugin %s"),
		 type, filename);
      g_module_close(module);
      return g_strdup(buf);
    }

    return 0;
  }
}


PluginRegistry::iterator 
PluginRegistry::begin(const char* type)
{
  vector<PluginHandle*>* v = type_to_vector(type);
  g_assert(v != 0);
  return v->begin();
}

PluginRegistry::iterator 
PluginRegistry::end(const char* type)
{
  vector<PluginHandle*>* v = type_to_vector(type);
  g_assert(v != 0);
  return v->end();
}

PluginRegistry::const_iterator 
PluginRegistry::begin(const char* type) const
{
  const vector<PluginHandle*>* v = type_to_vector(type);
  g_assert(v != 0);
  return v->begin();
}

PluginRegistry::const_iterator 
PluginRegistry::end(const char* type) const
{
  const vector<PluginHandle*>* v = type_to_vector(type);
  g_assert(v != 0);
  return v->end();

}

PluginHandle* 
PluginRegistry::get_handle(const char* type, size_t i)
{
  vector<PluginHandle*>* v = type_to_vector(type);
  g_assert(v != 0);
  return (*v)[i];
}

const PluginHandle* 
PluginRegistry::get_handle(const char* type, size_t i) const
{
  const vector<PluginHandle*>* v = type_to_vector(type);
  g_assert(v != 0);
  return (*v)[i];
}

vector<PluginHandle*>*
PluginRegistry::type_to_vector(const char* type)
{
  g_assert(type != 0);

  // If this is ever too slow we can do a map or something
  if (strcmp(type,PLOT_PLUGIN_TYPE) == 0) {
    return &plot_plugins_;
  }
  else if (strcmp(type,COLUMN_IMPORT_PLUGIN_TYPE) == 0) {
    return &column_importer_plugins_;
  }
  else if (strcmp(type,TRANSFORM_PLUGIN_TYPE) == 0) {
    return &transform_plugins_;
  }
  else {
    return 0;
  }
}

const vector<PluginHandle*>* 
PluginRegistry::type_to_vector(const char* type) const
{
  // discard const.
  return ((PluginRegistry*)this)->type_to_vector(type);
}

void 
PluginRegistry::consider_unloading()
{
  connect_to_method(Gtk_Main::idle(),
		    this,
		    &PluginRegistry::consider_unloading_cb);
}


gint 
PluginRegistry::consider_unloading_cb()
{
  iterator i = plot_plugins_.begin();
  while ( i != plot_plugins_.end() ) {
    (*i)->consider_unloading();
    ++i;
  }
  i = column_importer_plugins_.begin();
  while ( i != column_importer_plugins_.end() ) {
    (*i)->consider_unloading();
    ++i;
  }
  i = transform_plugins_.begin();
  while ( i != transform_plugins_.end() ) {
    (*i)->consider_unloading();
    ++i;
  }
  return 0;
}


PluginRegistry global_plugin_registry;
