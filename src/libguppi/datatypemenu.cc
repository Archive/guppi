// -*- C++ -*-

/* 
 * datatypemenu.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "datatypemenu.h"

DataTypeMenu::DataTypeMenu(const Store<DataType> & datatypes)
  : Gtk_Menu()
{
  make_menuitems(datatypes);
}

DataTypeMenu::~DataTypeMenu()
{
  list<Gtk_MenuItem*>::iterator i = menuitems_.begin();

  while (i != menuitems_.end()) {
    remove(*i);
    (*i)->hide();
    delete *i;
    ++i;
  }
}

void 
DataTypeMenu::make_menuitems(const Store<DataType> & datatypes)
{
#ifdef GNOME_ENABLE_DEBUG
  g_print("There are %u datatypes.\n", datatypes.size());
#endif
  Store<DataType>::const_iterator i = datatypes.begin();

  while ( i != datatypes.end() ) {
#ifdef GNOME_ENABLE_DEBUG
    g_print("Adding menuitem for %s\n", (*i)->name().c_str());
#endif
    Gtk_MenuItem* mi = new Gtk_MenuItem((*i)->name());
    append(*mi);
    mi->show();
    
    connect_to_method( mi->activate,
		       this,
		       &DataTypeMenu::activate_callback,
		       *i );
    
    menuitems_.push_back(mi);
    ++i;
  }
}

