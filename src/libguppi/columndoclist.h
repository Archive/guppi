// -*- C++ -*-

/* 
 * columndoclist.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_COLUMNDOCLIST_H
#define GUPPI_COLUMNDOCLIST_H

#include <set>

#include "columndoc.h"

class ColumnDocList
{
public:
  ColumnDocList() {}
  ~ColumnDocList() {}

  // We "own" all the sub-documents and have to delete them.
  void add_column(ColumnDoc & c);
  void delete_column(ColumnDoc* c);

  typedef set<ColumnDoc*>::iterator iterator;

  typedef set<ColumnDoc*>::const_iterator const_iterator;


  iterator begin() {
    iterator i(columns_.begin());
    return i;
  }

  iterator end() {
    iterator i(columns_.end());
    return i;
  }

  const_iterator begin() const {
    const_iterator i(columns_.begin());
    return i;
  }

  const_iterator end() const {
    const_iterator i(columns_.end());
    return i;
  }

  class View : public virtual ::View {
  public:
    virtual void add_column(const ColumnDoc& cd) = 0;
    virtual void delete_column(const ColumnDoc& cd) = 0;
    
  };

  class Model : public Model<ColumnDocList::View*> {
  public:
    void column_added(const ColumnDoc & cd) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->add_column(cd);
	++i;
      }
      unlock_views();
    }

    void column_deleted(const ColumnDoc & cd) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->delete_column(cd);
	++i;
      }
      unlock_views();
    }

  };
  
  Model columndoclist_model;

private:
  set<ColumnDoc*> columns_;
};


#endif

