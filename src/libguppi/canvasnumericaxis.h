// -*- C++ -*-

/* 
 * canvasnumericaxis.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_CANVASNUMERICAXIS_H
#define GUPPI_CANVASNUMERICAXIS_H

#include "numericaxis.h"
#include "canvasaxis.h"
#include <gtk--plot.h>

class CanvasNumericAxis : public CanvasAxis
{
public:
  CanvasNumericAxis(Axis_Base::alignment_t a, NumericAxis* axis, 
		    Gnome_CanvasGroup& parent);
  virtual ~CanvasNumericAxis();

  void set_axis(NumericAxis* axis);
  NumericAxis* get_axis();
  const NumericAxis* get_axis() const;

  // AxisLayoutHandle interface
  virtual void get_size_hints(double* x, double* y, double* w, double* h);
  virtual void set_size      (double x, double y, double w, double h);
  virtual void set_bounds(double start, double stop);

protected:

  virtual void draw_on_buffer(Plot_Area& pa);

  void change_size(const NumericAxis& a);
  void change_name(const Axis& a);

private:
  NumericAxis* axis_;

  Axis_Base             axis_base_;
  Numeric_Axis_Labeller labeller_;

  Connection c_[2];
  bool axis_connected_;
};

#endif

