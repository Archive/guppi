// -*- C++ -*-

/* 
 * plotwindow.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "plotwindow.h"
#include "guppi.h"
#include "plot.h"

class PlotWindowImpl : public Gtk_VBox,
                       public Plot::View,
                       public PlotDoc::View
{
public:
  PlotWindowImpl(PlotDoc & pd, PlotWindow & pw);
  ~PlotWindowImpl();

  PlotWindow* pw() { return pw_; }

  void destroy_model() { 
    if (pw_) {
      pd_ = 0;
      pw_->delete_self(); 
      pw_ = 0; 
    }
  }
  
  void change_status(const gchar* s) {
    g_return_if_fail(s);
    appbar_.set_status(s);
  }


  void change_name(const string & name);

  void change_icon(const string & icon);

private:
  void make_menus();

  void add_plot(Plot& plot);

  PlotDoc* pd_;
  PlotWindow* pw_;

  Gnome_AppBar appbar_;

  Plot* plot_;

  // This prevents resize events when the statusbar changes
  Gtk_Alignment align_;
};


PlotWindow::PlotWindow(PlotDoc& pd) : 
  Gnome_App(Guppi::APPNAME, ""), 
  impl_(new PlotWindowImpl(pd,*this))
{
  impl_->show();
}

PlotWindow::~PlotWindow()
{
  impl_->hide();
  delete impl_;
}

PlotWindowImpl::PlotWindowImpl(PlotDoc& pd, PlotWindow& pw)
  :  pd_(&pd), pw_(&pw),
     appbar_(true,true,GNOME_PREFERENCES_USER),
     plot_(0),
     align_(0.0,0.0,1.0,1.0)
{
  pw_->set_contents(&align_);
  pw_->set_statusbar(&appbar_);
  align_.add(this);

  pd_->plotdoc_model.add_view(*this);
  pd_->named_model.add_view(*this);
  pd_->iconed_model.add_view(*this);

  add_plot(*pd_->make_plot());  

  make_menus();
}

PlotWindowImpl::~PlotWindowImpl()
{
  if (pd_) {
    pd_->plotdoc_model.remove_view(*this);
    pd_->named_model.remove_view(*this);
    pd_->iconed_model.remove_view(*this);
  }

  if (plot_) {
    plot_->plot_model.remove_view(*this);
    plot_->widget()->hide();
    delete plot_;
  }
}

void
PlotWindowImpl::add_plot(Plot& plot)
{
  g_return_if_fail(plot_ == 0);

  plot_ = &plot;
  pack_start(*(plot_->widget()),TRUE,TRUE);

  plot_->plot_model.add_view(*this);

  plot_->widget()->show();
}

void
PlotWindowImpl::change_name(const string & name)
{
  pw_->set_title(name.c_str());
}

void
PlotWindowImpl::change_icon(const string & icon)
{
  g_warning(__FUNCTION__);
}

///////////////////////////////// Menu cruft

// Bunch of callbacks - they just call the relevant methods on the 
//  object sent in as data.

static void 
about_cb(GtkWidget * w, gpointer data)
{
  g_return_if_fail(data != 0);
  big_fish.about();
}

static void
close_cb(GtkWidget * w, gpointer data)
{
  g_return_if_fail(data != 0);
  // app can't close itself.
  ((PlotWindowImpl *)data)->pw()->delete_self();
}

static void 
exit_cb(GtkWidget * w, gpointer data)
{
  big_fish.exit();
}

static GnomeUIInfo help_menu[] = {
  GNOMEUIINFO_HELP(const_cast<gchar*>(Guppi::APPNAME)),
  {GNOME_APP_UI_ITEM, N_("About..."), 
   N_("Tell about this application"), 
   about_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT, 0, 
   (GdkModifierType)0, NULL },
  GNOMEUIINFO_END
};

static GnomeUIInfo file_menu[] = {
  {GNOME_APP_UI_ITEM, N_("Close"), 
   N_("Close this window"),
   close_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXIT, 'c', 
   GDK_CONTROL_MASK, NULL },
  GNOMEUIINFO_SEPARATOR,
  {GNOME_APP_UI_ITEM, N_("Exit"), 
   N_("Quit the application"),
   exit_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXIT, 'q', 
   GDK_CONTROL_MASK, NULL },
  GNOMEUIINFO_END
};

static GnomeUIInfo plot_menu[] = {
  // dynamically constructed
  GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[] = {
  GNOMEUIINFO_SUBTREE (N_("File"), file_menu),
  GNOMEUIINFO_SUBTREE (N_("Plot"), plot_menu),
  GNOMEUIINFO_SUBTREE (N_("Help"), help_menu),
  GNOMEUIINFO_END
};

void 
PlotWindowImpl::make_menus()
{
  g_assert(plot_);

  pw_->create_menus_with_data(main_menu, this);

  list<Gtk_MenuItem*>::iterator i = plot_->menu().begin();
  while ( i != plot_->menu().end() ) {
    Gtk_MenuItem* mi = *i;
    gtk_menu_append(GTK_MENU(plot_menu[0].widget),
		    GTK_WIDGET(mi->gtkobj()));

    mi->show();
    ++i;
  }
  
  
}



