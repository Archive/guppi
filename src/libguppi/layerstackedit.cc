// -*- C++ -*-

/* 
 * layerstackedit.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "layerstackedit.h"

#include <map.h>

class LayerStackEditImpl : public Gtk_VBox {
public:
  LayerStackEditImpl(LayerStackEdit& wrapper, LayerStack& ls);
  ~LayerStackEditImpl();
  
private:
  LayerStack& ls_;

  Gtk_Notebook notebook_;
  Gtk_HButtonBox buttons_;
  Gtk_Button remove_button_;

  Gtk_MenuBar add_bar_;
  Gtk_MenuItem add_item_;
  Gtk_Menu* add_menu_;

  void add_layer(Layer & layer);
  void delete_layer(Layer & layer, Gtk_Widget* page);

  void remove_button_clicked();

  void stack_destroyed(Layer& l, LayerStackEdit* lse);

  void change_name(Layer & l,Gtk_Label* label);

  map<Gtk_Widget*,Layer*> pages_;
  // for updating labels if necessary
  map<Layer*,Gtk_Label*> labels_;
};

LayerStackEdit::LayerStackEdit(LayerStack & ls) : 
  Gtk_Wrapper(new LayerStackEditImpl(*this,ls))
{
  
}

LayerStackEditImpl::LayerStackEditImpl(LayerStackEdit& wrapper, LayerStack& ls) :
  Gtk_VBox(FALSE,0), ls_(ls), notebook_(), buttons_(),
  // FIXME stock pixmaps on these.
  remove_button_(_("Remove this layer")), 
  add_bar_(),
  add_item_(_("New layer")),
  add_menu_(0)
{
  pack_start(notebook_,true,true);
  pack_end(buttons_,true,true);
  add_bar_.append(add_item_);
  buttons_.pack_start(add_bar_,true,true);
  buttons_.pack_end(remove_button_,true,true);

  add_menu_ = ls_.make_layer_menu();
  add_item_.set_submenu(add_menu_);

  // Add any layers we already have.
  LayerStack::iterator i = ls_.begin();
  while ( i != ls_.end() ) {
    add_layer(**i);
    ++i;
  }

  connect_to_method(ls_.layer_added,
		    this,
		    &LayerStackEditImpl::add_layer);

  connect_to_method(remove_button_.clicked,
		    this,
		    &LayerStackEditImpl::remove_button_clicked);
 

  show_all(); // wrong, but hey
}

// I don't expect anyone to believe me, but egcs 1.0 and 1.1 both
//  puke with "Parse error before *" if you put these types in 
//  directly instead of using the typedef. Utterly bizarre.
// Will probably go away just as randomly as it appeared as the 
//  code evolves.
typedef map<Gtk_Widget*,Layer*>::iterator stupid_compiler_workaround_a;
typedef map<Layer*,Gtk_Label*>::iterator stupid_compiler_workaround_b;

LayerStackEditImpl::~LayerStackEditImpl()
{
  // if there are any of these left, then we are not being
  //  destroyed as a result of the LayerStack being destroyed,
  //  since if a contained Layer is destroyed, the page is destroyed.
  // So it's our problem to delete them.
  stupid_compiler_workaround_a i = pages_.begin();
  while ( i != pages_.end() ) {
    delete (*i).first;
    ++i;
  }

  stupid_compiler_workaround_b j = labels_.begin();
  while ( j != labels_.end() ) {
    delete (*j).second;
    ++j;
  }

  delete add_menu_;
}

void 
LayerStackEditImpl::change_name(Layer & l, Gtk_Label* label)
{
  label->set(l.get_name());
}

void 
LayerStackEditImpl::add_layer(Layer & layer)
{
  Gtk_Widget* w = layer.make_editor();
  g_return_if_fail(w != 0);
  pages_[w] = &layer;
  Gtk_Label* l = new Gtk_Label(layer.get_name());
  labels_[&layer] = l;
  notebook_.append_page(*w,*l);
  connect_to_method(layer.destroy,
		    this,
		    &LayerStackEditImpl::delete_layer,
		    w);
  connect_to_method(layer.name_changed,
		    this,
		    &LayerStackEditImpl::change_name,
		    l);
  w->show();
  l->show();
}

void 
LayerStackEditImpl::delete_layer(Layer & layer, Gtk_Widget* page)
{
  Gtk_Label* label = labels_[&layer];

  page->hide();
  label->hide();

  gint p = notebook_.page_num(*page);
  if (p == -1) g_error("Notebook page not found - internal error - arrgh");
  notebook_.remove_page(p);

  pages_.erase(page);
  labels_.erase(&layer);

  // NO! page deletes itself, since it's tied to the Layer.
  ///  delete page; 
  delete label;
}

void
LayerStackEditImpl::remove_button_clicked()
{
  Gtk_Widget* child = notebook_.current_child();
  Layer* l = pages_[child];
  ls_.delete_layer(*l); // results in destroy signal from the layer removing page.
}

void 
LayerStackEditImpl::stack_destroyed(Layer& l, LayerStackEdit* lse)
{
  lse->delete_self();
}

