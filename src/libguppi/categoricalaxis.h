// -*- C++ -*-

/* 
 * categoricalaxis.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_CATEGORICALAXIS_H
#define GUPPI_CATEGORICALAXIS_H

#include "axis.h"

#include <vector>

class CategoricalAxis : public Axis 
{
public:
  CategoricalAxis();
  virtual ~CategoricalAxis();

  // OK to shrink or grow; extra strings just disappear.
  void set_size(size_t s);
  size_t size() const;

  // A category string can be 0, meaning
  //  not to display it.
  const char* get_category(size_t i);
  void        set_category(const char* s, size_t i);

  Signal1<const CategoricalAxis&> changed;

private:
  vector<char*>* categories_;

  void delete_data();

};

#endif
