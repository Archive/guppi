// -*- C++ -*-

/* 
 * categorical.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_CATEGORICAL_H
#define GUPPI_CATEGORICAL_H

// This is a counterpart of "ColumnDoc" for categorical data. 
//  I wish to rename ColumnDoc to ScalarData

#include "named.h"

#include <gtk--sig.h>

class CategoricalData : public Named, 
			public Gtk_Signal_Base
{
public:
  virtual ~CategoricalData() {}

  // Casting away the const qualifies you to be 
  //  tortured slowly. It may not be the main 
  //  representation kept by the object
  // These are not guaranteed to remain valid;
  //  they have to be used immediately and not saved 
  //  anywhere unless you copy them.
  virtual const char*const*  data() const = 0;
  virtual const char*   data(size_t i) const = 0;

  virtual size_t   size() const = 0;

  // Signals

  mutable Signal1<const CategoricalData &> data_changed;

  mutable Signal1<const CategoricalData &> destroy;

  mutable Signal1<const CategoricalData &> name_changed;

protected:
  CategoricalData(const gchar* default_name) : Named(default_name) {}

  virtual void named_callback(const string & name) = 0;
};

class SimpleCategorical : public CategoricalData
{
public:
  SimpleCategorical();
  SimpleCategorical(const char** data, size_t N);
  virtual ~SimpleCategorical();

  virtual const char*const *  data() const;
  virtual const char*   element(size_t i) const = 0;

  virtual size_t   size() const;

  void set_data(const char** data, size_t N);

private:
  void named_callback(const string & name) {
    name_changed(*this);
  }

  void delete_data();

  char** data_;
  size_t n_;
};


#endif

