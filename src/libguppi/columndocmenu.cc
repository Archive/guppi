// -*- C++ -*-

/* 
 * columndocmenu.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "columndocmenu.h"

ColumnDocMenu::ColumnDocMenu(const ColumnDocList & columndocs)
  : Gtk_Menu(), cdl_(&columndocs)
{
  make_menuitems(columndocs);

  cdl_->columndoclist_model.add_view(*this);
}

ColumnDocMenu::~ColumnDocMenu()
{
  if (cdl_) {
    cdl_->columndoclist_model.remove_view(*this);
  }
  clear_menuitems();
}

// Keeps the label accessible from outside.
class Labelled_MenuItem : public Gtk_MenuItem,
                          public ColumnDoc::View {
public:
  Labelled_MenuItem(const ColumnDoc& cd) : 
    label_(new Gtk_Label(cd.get_name())), cd_(&cd) {
    label_->set_alignment(0.0,0.5);
    add(label_);
    cd_->named_model.add_view(*this);
  }
  ~Labelled_MenuItem() {
    if (cd_) {
      cd_->named_model.remove_view(*this);
    }
    label_->hide();
    delete label_;
  }
  
  // not important
  void change_data(const ColumnDoc & cd) {}

  void destroy_model() {cd_ = 0;}

  void change_name(const string & name) {
    label_->set(name);
  }

  Gtk_Label* label() { return label_; }
private:
  Gtk_Label* label_;
  const ColumnDoc* cd_;

};

void
ColumnDocMenu::make_menuitem(const ColumnDoc* cd)
{
  g_return_if_fail(positions_.find(cd) == positions_.end());

  Gtk_MenuItem* mi = 0;
  if (cd) {
    Labelled_MenuItem* lmi = new Labelled_MenuItem(*cd);

    mi = lmi;
  }
  else{
    mi = new Gtk_MenuItem(_("None"));
  }

  append(*mi);
  menuitems_.push_back(mi);
  gint new_pos = positions_.size();
  positions_[cd] = new_pos;

  connect_to_method(mi->activate,
		    this,
		    &ColumnDocMenu::activate_callback,
		    cd);
  mi->show();
}

void 
ColumnDocMenu::make_menuitems(const ColumnDocList & columndocs)
{
  g_return_if_fail(menuitems_.empty());
  g_return_if_fail(positions_.empty());

  // "None" item
  make_menuitem(0);

  ColumnDocList::const_iterator i = columndocs.begin();

  while ( i != columndocs.end() ) {
    make_menuitem(*i);
    ++i;
  }
}

void 
ColumnDocMenu::clear_menuitems()
{
  list<Gtk_MenuItem*>::iterator i = menuitems_.begin();

  while (i != menuitems_.end()) {
    remove(*i);
    (*i)->hide();
    delete *i;
    ++i;
  }  
  menuitems_.clear();
  positions_.clear();
}

// NOTE for now this assumes add_column means append_column and
//  it will break otherwise.
void 
ColumnDocMenu::add_column(const ColumnDoc & cd)
{
  make_menuitem(&cd);
  queue_resize();
}

void 
ColumnDocMenu::delete_column(const ColumnDoc & cd)
{
  g_return_if_fail(cdl_ != 0);
  // Lazy but works for now. fix it later.
  clear_menuitems();
  make_menuitems(*cdl_);
  queue_resize();
}




