// -*- C++ -*-

/* 
 * plugin.cc 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */
  
#include "plugin.h"


void
Plugin::ref() const
{
#ifdef GNOME_ENABLE_DEBUG
  g_print("Bumping refcount for `%s' from %d to %d\n",
	  get_name(), refcount_, refcount_ + 1);
#endif
  ++refcount_;
}

void Plugin::unref() const
{
#ifdef GNOME_ENABLE_DEBUG
  int oldrefs = refcount_;
#endif
  if (refcount_ > 0) --refcount_;
#ifdef GNOME_ENABLE_DEBUG
  g_print("Dropping refcount for `%s' from %d to %d\n",
	  get_name(), oldrefs, refcount_);
#endif
}

bool 
Plugin::check_version() const
{
  return (plugin_version() == guppi_plugin_version);
}

unsigned int Plugin::guppi_plugin_version = GUPPI_PLUGIN_INTERFACE_VERSION;

#include <libgnome/libgnome.h>

char*
load_plugin(const char* filename, const Plugin** plugin, GModule** handle)
{
  g_return_val_if_fail(filename != 0, 0);
  g_return_val_if_fail(plugin != 0, 0);
  g_return_val_if_fail(handle != 0, 0);
  
  *handle = 0;
  *plugin = 0;

  GModule* module;

#ifdef GNOME_ENABLE_DEBUG
  // bind immediately, to catch missing symbols.
  module = g_module_open(filename, static_cast<GModuleFlags>(0));
#else
  module = g_module_open(filename, G_MODULE_BIND_LAZY);
#endif
  
  if (module == 0) {
    gchar buf[256];
    g_snprintf(buf, 255, _("Failed to load plugin %s\n  (%s)\n"), 
	       filename, g_module_error());
    return g_strdup(buf);
  }
  
#ifdef GNOME_ENABLE_DEBUG
  g_print("Opened plugin %s\n", filename);
#endif

  const Plugin* (*init_plugin)(void);

  if ( ! g_module_symbol(module, 
			 "init_plugin", 
			 (gpointer*)&init_plugin) ) {
    gchar buf[256];
    g_snprintf(buf, 255, _("Failed to load init_plugin symbol from %s\n  (%s)\n"), 
	       filename, g_module_error());
    return g_strdup(buf);
  }
  
  const Plugin* p = init_plugin();

  if (p == 0) {
    gchar buf[256];
    g_snprintf(buf, 255, _("init_plugin returned 0 in %s\n"), 
	       filename);
    return g_strdup(buf);
  }
  
  if (!p->check_version()) {
    gchar buf[256];
    g_snprintf(buf, 255, _("Bad version for %s\n"), 
	       filename);
    return g_strdup(buf);
  }
  
  *plugin = p;
  *handle = module;

  return 0;
}



