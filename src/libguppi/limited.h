// -*- C++ -*-

/* 
 * limited.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_LIMITED_H
#define GUPPI_LIMITED_H


///////////////  FIXME I suspect this should be mostly de-inlined.
#ifndef G_ENABLE_DEBUG
#define G_ENABLE_DEBUG
#endif
#include <gtk--draw.h>

class Limited : public Bounded {
public:
  Limited() : x_min_limit_(0.0), x_max_limit_(0.0),
    y_min_limit_(0.0), y_max_limit_(0.0), has_x_limit_(false),
    has_y_limit_(false) {}
  virtual ~Limited() {}

  // The min/max bounds.
  void set_x_limits(gdouble x0, gdouble x1) {
    has_x_limit_ = true;
    // This can be written more concisely with MIN/MAX,
    //  but I think this way is actually better.
    if (x0 < x1) {
      x_min_limit_ = x0;
      x_max_limit_ = x1;
    }
    else {
      x_min_limit_ = x1;
      x_max_limit_ = x0;
    }
    // somewhat inefficient, and avoidable if you just call
    //  limits before bounds always.
    // But I don't like having order of calling be important
    set_x_bounds(limited_x(x_min()), limited_x(x_max()));
  }
  void set_y_limits(gdouble y0, gdouble y1) {
    has_y_limit_ = true;
    if (y0 < y1) {
      y_min_limit_ = y0;
      y_max_limit_ = y1; 
    }
    else {
      y_min_limit_ = y1; 
      y_max_limit_ = y0;
    }
    set_y_bounds(limited_y(y_min()), limited_y(y_max()));
  }
  void set_limits(gdouble x0, gdouble x1, gdouble y0, gdouble y1) {
    set_x_limits(x0,x1);
    set_y_limits(y0,y1);
  }

  void set_x_bounds(gdouble x0, gdouble x1) {
    // limited_x() does nothing if there's no limit 
    Bounded::set_x_bounds(limited_x(x0), limited_x(x1));
  }
  void set_y_bounds(gdouble y0, gdouble y1) {
    // limited_y() does nothing if there's no limit 
    Bounded::set_y_bounds(limited_y(y0), limited_y(y1));
  }
  // This is the same as Bounded, maybe fix.
  void set_bounds(gdouble x0, gdouble x1, gdouble y0, gdouble y1) {
    set_x_bounds(x0,x1);
    set_y_bounds(y0,y1);
  }

  bool in_x_limits(gdouble x) const { 
    if (!has_x_limit_) return true;
    else return (x >= x_min_limit_) && (x <= x_max_limit_); 
  }
  bool in_y_limits(gdouble y) const {
    if (!has_y_limit_) return true;
    else return (y >= y_min_limit_) && (y <= y_max_limit_); 
  }

  gdouble x_min_limit() const { return x_min_limit_; }
  gdouble x_max_limit() const { return x_max_limit_; }
  gdouble y_min_limit() const { return y_min_limit_; }
  gdouble y_max_limit() const { return y_max_limit_; }

  bool has_x_limits() const { return has_x_limit_; }
  bool has_y_limits() const { return has_y_limit_; }

  void unlimit_x() { has_x_limit_ = false; }
  void unlimit_y() { has_y_limit_ = false; }

protected:
  gdouble limited_x(gdouble x) const {
    if (!has_x_limit_) return x;
    else if (x < x_min_limit_) return x_min_limit_;
    else if (x > x_max_limit_) return x_max_limit_;
    else return x;
  }

  gdouble limited_y(gdouble y) const {
    if (!has_y_limit_) return y;
    else if (y < y_min_limit_) return y_min_limit_;
    else if (y > y_max_limit_) return y_max_limit_;
    else return y;
  }
  
private:
  gdouble x_min_limit_, x_max_limit_, y_min_limit_, y_max_limit_;
  bool has_x_limit_;
  bool has_y_limit_;
};

#endif
