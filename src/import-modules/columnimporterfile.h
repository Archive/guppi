// -*- C++ -*-

/* 
 * columnimporterfile.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_COLUMNIMPORTERFILE_H
#define GUPPI_COLUMNIMPORTERFILE_H

#include "columnimporter.h"
#include <libgnome/libgnome.h> // i18n

// Get data from a file.

class ColumnImporterFileImpl;

class ColumnImporterFile : public ColumnImporter {
public:
  ColumnImporterFile(ColumnImporterData cid);
  virtual ~ColumnImporterFile();
  
  // user has to free the list and the ColumnDocs
  virtual list<ColumnDoc*> * get_docs(); 

private:
  ColumnImporterFileImpl* impl_;
};


#endif
