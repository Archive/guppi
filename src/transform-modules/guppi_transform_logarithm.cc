// -*- C++ -*-

/* 
 * guppi_transform_logarithm.cc 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


// This functionality is trivial; it's for demo purposes basically 

#include <libgnome/libgnome.h> // i18n
#include "transformplugin.h"
#include <math.h>

class LogarithmTransformPlugin : public TransformPlugin {
public:
  virtual const char* get_menu_name() const { 
    return _("Logarithm"); 
  }

  virtual const char* get_name() const { 
    return _("Natural Log Transform"); 
  }

  virtual const char* get_description() const { 
    return _("Takes ln of all data points"); 
  }

  virtual void     transform(ColumnDoc& cd) const {
    DataSet* ds = cd.ds();
    if (ds == 0) {
      // Can't do native transform
      if (cd.min() <= 0) {
	cd.clear();
	return;
      }

      const double* d = cd.data();
      const size_t n = cd.size();
      double* results = new double[n];

      for (size_t i=0; i<n; ++i)
	results[i] = log(d[i]);

      cd.set(0,results,n);

      delete [] results;

      return;
    }
    else {
      ds->log_transform();
      cd.columndoc_model.data_changed(cd); // manual since we went low-level with the data set.
    }
    gchar buf[256];
    g_snprintf(buf,255,_("ln of %s"),cd.get_name().c_str());
    cd.set_name(buf);
  }

protected:
  virtual unsigned int plugin_version() const { return GUPPI_PLUGIN_INTERFACE_VERSION;}
};

static LogarithmTransformPlugin* global_logarithm_transform;

extern "C" {

  // Symbol the Guppi module loader will look for.
G_MODULE_EXPORT  const Plugin* init_plugin() 
{
  // Can't just use a global; it segfaults on unload, 
  //  due to some _fini routine the compiler or linker 
  //  presumably adds.
  if (global_logarithm_transform == 0) 
    global_logarithm_transform = new LogarithmTransformPlugin;
  global_logarithm_transform->ref();
  return global_logarithm_transform;
}

G_MODULE_EXPORT void
g_module_unload (GModule *module)
{
#ifdef GNOME_ENABLE_DEBUG
  g_print ("Unloading logarithm transform plugin.\n");
#endif
  if (global_logarithm_transform) {
    g_assert(global_logarithm_transform->unloadable());
    delete global_logarithm_transform;
  }
}


}

