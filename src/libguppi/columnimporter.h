// -*- C++ -*-

/* 
 * columnimporter.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_COLUMNIMPORTER_H
#define GUPPI_COLUMNIMPORTER_H

#include "columndoc.h"
#include "importer.h"
#include "store.h"

class ColumnImporter : public Importer<ColumnDoc> 
{
public:
  //well, for now it's just a typedef
};

// This just makes things easier to extend in the future.
class ColumnImporterData {
public:
  ColumnImporterData(const Store<DataType> & dt) :
    datatypes(dt) {}
  // Copy so we don't have to worry about memory.
  ColumnImporterData(const ColumnImporterData & source) : 
    datatypes(source.datatypes) {}
  const Store<DataType> & datatypes;
};

#endif

