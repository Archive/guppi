// -*- C++ -*-

/* 
 * columndocmenu.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_COLUMNDOCMENU_H
#define GUPPI_COLUMNDOCMENU_H

#include <list>
#include <map>

#include <gnome--.h>
#include "columndoc.h"
#include "columndoclist.h"

class ColumnDocMenu : public Gtk_Menu,
                      public ColumnDocList::View {
public:
  ColumnDocMenu(const ColumnDocList & columndocs);
  ~ColumnDocMenu();

  // The "none" choice on the menu returns 0 as the ColumnDoc*
  Signal1<const ColumnDoc*> columndoc_activated;

  typedef map<const ColumnDoc*, gint>::iterator fucked_up_compiler;
  guint index(const ColumnDoc* cd) {
    fucked_up_compiler i = positions_.find(cd);
    if (i == positions_.end()) { 
      g_warning("ColumnDoc* %p not found in menu", cd);
      return 0;
    }
    return (*i).second;
  }


  void add_column(const ColumnDoc & cd);
  void delete_column(const ColumnDoc & cd);

  void destroy_model() { if (cdl_) { cdl_ = 0; delete_self(); } }

private:
  list<Gtk_MenuItem*> menuitems_;
  map<const ColumnDoc*, gint> positions_;
  const ColumnDocList* cdl_;

  void make_menuitem(const ColumnDoc* cd);

  void make_menuitems(const ColumnDocList & columndocs);

  void clear_menuitems();

  void activate_callback(const ColumnDoc* dt) {
    columndoc_activated(dt);
  }

};

#endif

