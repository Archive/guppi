// -*- C++ -*-

/* 
 * iconed.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_ICONED_H
#define GUPPI_ICONED_H

// Objects with icons.

#include <string>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-util.h>

class Iconed {
public:
  
  Iconed(const char * icon) : icon_(icon) {}
  Iconed(const string & icon) : icon_(icon) {} 
  Iconed() {}
  virtual ~Iconed() {}
  
  virtual void set_icon(const char * icon) { 
    icon_ = icon;
    iconed_model.icon_changed(icon_);
  }
  virtual void set_icon(const string & icon) { 
    set_icon(icon.c_str());
  }
  virtual void set_gnome_icon(const char * const icon) {
    gchar* p = gnome_pixmap_file(icon);
    if (p) {
      set_icon(p);
      g_free(p);
    }
    else set_icon(icon);
  }

  const string & get_icon() const { return icon_; }

  
  class View : public virtual ::View {
  public:
    virtual void change_icon(const string & icon) = 0;
    
  };

  class Model : public Model<Iconed::View*> {
  public:
    void icon_changed(const string & icon) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_icon(icon);
	++i;
      }
      unlock_views();
    }

  };
  
  Model iconed_model;

protected:

private:
  string icon_;
};

#endif
