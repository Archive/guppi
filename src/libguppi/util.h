// -*- C++ -*-

/* 
 * util.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_UTIL_H
#define GUPPI_UTIL_H

#include <algo.h>
#include <list>
#include <map>

template <class ValueT >
void move_frontward(list<ValueT> & container, const ValueT value)
{
  list<ValueT>::iterator i;
    
  i = find(container.begin(), container.end(), value);
  
  if (i == container.begin()) return; // at front already

  if ( i != container.end() ) {
    list<ValueT>::iterator prev;
    prev = --i;
    ++i; // back to original
    iter_swap(prev,i); // gratuitous standard algorithm use.
  }
}


template <class ValueT >
void move_backward(list<ValueT> & container, const ValueT value)
{
  list<ValueT>::iterator i;
    
  i = find(container.begin(), container.end(), value);
  
  if ( i != container.end() ) {
    list<ValueT>::iterator next;
    next = ++i;
    if (next == container.end()) return; // already at back 
    --i; // back to first item
    iter_swap(next,i); // gratuitous standard algorithm use.
  }
}


// iterate over the second part of map pairs
template <class K, class T>
class second_iterator : public map<K,T>::iterator {
public:
  second_iterator(map<K,T>::iterator i) :
    map<K,T>::iterator(i) {}
  
  T & operator* () const {
    // whew, now that is ugly
    return (map<K,T>::iterator::operator*()).second; 
  }
};

template <class K, class T>
class const_second_iterator : public map<K,T>::const_iterator {
public:
  const_second_iterator(map<K,T>::const_iterator i) :
    map<K,T>::const_iterator(i) {}
  
  const_second_iterator(map<K,T>::iterator i) : 
    map<K,T>::const_iterator(i) {}

  const T & operator* () const {
    return (map<K,T>::const_iterator::operator*()).second; 
  }
};



// iterate over the first part of map pairs
template <class K, class T>
class first_iterator : public map<K,T>::iterator {
public:
  first_iterator(map<K,T>::iterator i) :
    map<K,T>::iterator(i) {}
  
  T & operator* () const {
    // whew, now that is ugly
    return (map<K,T>::iterator::operator*()).first; 
  }
};

template <class K, class T>
class const_first_iterator : public map<K,T>::const_iterator {
public:
  const_first_iterator(map<K,T>::const_iterator i) :
    map<K,T>::const_iterator(i) {}
  
  const_first_iterator(map<K,T>::iterator i) : 
    map<K,T>::const_iterator(i) {}

  const T & operator* () const {
    return (map<K,T>::const_iterator::operator*()).first; 
  }
};

#endif
