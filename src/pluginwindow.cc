// -*- C++ -*-

/* 
 * pluginwindow.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "pluginwindow.h"

#include "plotplugin.h"
#include "columnimportplugin.h"
#include "transformplugin.h"

class PluginWindowImpl : public Gtk_VBox
{
public:
  PluginWindowImpl(PluginRegistry & pr);
  ~PluginWindowImpl();

private:
  PluginRegistry& pr_;

  Gtk_Label label_;
  Gtk_ScrolledWindow scroll_;
  Gtk_CList list_;

  void add_for_type(const char* ptype);
};

PluginWindow::PluginWindow(PluginRegistry & pr)
  : Gnome_Dialog(_("Plugins"), GNOME_STOCK_BUTTON_OK, NULL),
    impl_(new PluginWindowImpl(pr))
{
  set_policy(true,true,false);
  vbox()->pack_start(*impl_,true,true,GNOME_PAD_SMALL);
  impl_->show();
}

PluginWindow::~PluginWindow()
{
  delete impl_;
}

PluginWindowImpl::PluginWindowImpl(PluginRegistry & pr)
  : Gtk_VBox(false,0), pr_(pr), label_(_("Plugins")), list_(4)
{
  pack_start(label_,false,false,GNOME_PAD);
  pack_end(scroll_,true,true,0);
  scroll_.set_policy(GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);
  scroll_.add(list_);

  list_.set_usize(470,300);
  list_.set_column_width(0,120);
  list_.set_column_width(1,240);
  list_.set_column_width(2,45);
  list_.set_column_width(3,35);
  list_.set_column_title(0,_("Name"));
  list_.set_column_title(1,_("Description"));
  list_.set_column_title(2,_("Type"));
  list_.set_column_title(3,_("Loaded?"));
  list_.column_titles_show();
  list_.column_titles_passive();

  add_for_type(PLOT_PLUGIN_TYPE);
  add_for_type(COLUMN_IMPORT_PLUGIN_TYPE);
  add_for_type(TRANSFORM_PLUGIN_TYPE);

  list_.show();
  scroll_.show();
  label_.show();
}

PluginWindowImpl::~PluginWindowImpl()
{
  

}

void 
PluginWindowImpl::add_for_type(const char* ptype)
{
  PluginRegistry::const_iterator i;
  PluginRegistry::const_iterator end;
  
  const gchar* text[4];

  i = pr_.begin(ptype);
  end = pr_.end(ptype);
  while (i != end) {
    PluginHandle* ph = *i;
    text[0] = ph->get_name().c_str();
    text[1] = ph->get_description().c_str();
    text[2] = ph->get_type().c_str();
    text[3] = ph->loaded() ? _("Yes") : _("No");
    list_.append(text);
    ++i;
  }
}
