// -*- C++ -*-

/* 
 * columndoclist.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "columndoclist.h"

void 
ColumnDocList::add_column(ColumnDoc & c)
{
  columns_.insert(&c);

  columndoclist_model.column_added(c);
}

void 
ColumnDocList::delete_column(ColumnDoc* c)
{
  ColumnDoc* found = 0;

  set<ColumnDoc*>::iterator i = columns_.find(c);
  if (i != columns_.end() ) {
    g_assert(c == *i);
    found = *i;
    columns_.erase(i);
  }

  if (found != 0) {
    columndoclist_model.column_deleted(*c);
    //    not_saved();
    delete c;
  }
  else {
    g_warning("Column not found in ColumnDocList!\n");
  }
}

