// -*- C++ -*-

/* 
 * regressionlayer.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_REGRESSIONLAYER_H
#define GUPPI_REGRESSIONLAYER_H

#include "layer.h"
#include "colored.h"
#include "columndoclist.h"

#include <goose/LinearRegression.h>

class RegressionEdit;

class RegressionLayer : public XYLayer, public Colored {
public:
  RegressionLayer(const NumericAxis* x,
		  const NumericAxis* y,
		  const ColumnDocList & cdl);
  ~RegressionLayer();

  virtual Gtk_Widget* make_editor(); 

  virtual void        draw(Plot_Area& target) const;

  virtual void        set_x(const ColumnDoc* cd);
  virtual void        set_y(const ColumnDoc* cd);

  mutable Signal1<const RegressionLayer&> color_changed;

private:
  const ColumnDocList & cdl_;

  void color_changed_callback(const Colored& c) {
    color_changed(*this);
    need_draw(*this);
  }

  LinearRegression r_;

  friend class RegressionEdit;
};

#endif
