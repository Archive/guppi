// -*- C++ -*-

/* 
 * guppi_transform_linear.cc 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <gnome--.h> // i18n
#include "transformplugin.h"

class LinearTransformPlugin;

class LinearDialog : public Gnome_Dialog {
public:
  LinearDialog(ColumnDoc & cd, LinearTransformPlugin* plugin);
  ~LinearDialog();

private:
  ColumnDoc & cd_;
  LinearTransformPlugin* plugin_;

  Gtk_Label label_;
  Gtk_HBox  valuebox_;
  Gtk_Adjustment aadj_;
  Gtk_Label alabel_;
  Gtk_SpinButton a_;
  Gtk_Adjustment badj_;
  Gtk_Label blabel_;
  Gtk_SpinButton b_;

  void clicked_impl(gint button);
}; 

LinearDialog::LinearDialog(ColumnDoc & cd, LinearTransformPlugin* plugin) : 
  Gnome_Dialog(_("Linear Transform"), 
	       GNOME_STOCK_BUTTON_OK, 
	       GNOME_STOCK_BUTTON_CANCEL, 0),
	       cd_(cd),
	       plugin_(plugin),
	       label_("y = mx + b"),
	       valuebox_(FALSE,GNOME_PAD_SMALL),
	       aadj_(0.0,-(FLT_MAX),FLT_MAX, cd.range()/cd.size(), 
		     (cd.range()/cd.size())*5, 1.0),
	       alabel_(_("m")),
	       a_(aadj_),
	       badj_(0.0,-(FLT_MAX),FLT_MAX, cd.range()/cd.size(), 
		     (cd.range()/cd.size())*5, 1.0),
	       blabel_(_("b")),
	       b_(badj_)
{
  vbox()->pack_start(label_);
  vbox()->pack_end(valuebox_);
  
  valuebox_.pack_start(alabel_,TRUE,TRUE,GNOME_PAD_SMALL);
  valuebox_.pack_start(a_,TRUE,TRUE,GNOME_PAD_SMALL);
  valuebox_.pack_start(blabel_,TRUE,TRUE,GNOME_PAD_SMALL);
  valuebox_.pack_start(b_,TRUE,TRUE,GNOME_PAD_SMALL);

  show_all();
}

void 
LinearDialog::clicked_impl(gint button)
{
  double a,b;

  switch (button) {
  case GNOME_OK:
    DataSet* ds;
    a = a_.get_value_as_float();
    b = b_.get_value_as_float();
    ds = cd_.ds();
    if (ds) {
      ds->linear_transform(a,b);
      cd_.columndoc_model.data_changed(cd_); // since we used low-level dataset access hack
    }
    else {
      DataSet ds(((const ColumnDoc&)cd_).ds());
      ds.linear_transform(a,b);
      cd_.set(ds);
      // Hack: force copy-on-write, so there isn't a reference
      //  to an array in the module when it's unloaded.
      ds.add(1.0);
    }
    gchar buf[256];
    g_snprintf(buf,255,_("Linear transform of %s"),cd_.get_name().c_str());
    cd_.set_name(buf);
    // FALL THROUGH
  default:
    delete_self();
  }
}

class LinearTransformPlugin : public TransformPlugin {
public:
  virtual const char* get_menu_name() const { 
    return _("Linear"); 
  }

  virtual const char* get_name() const { 
    return _("Linear Transform"); 
  }

  virtual const char* get_description() const { 
    return _("Arbitrary linear transformation"); 
  }

  virtual void     transform(ColumnDoc& cd) const {
    LinearDialog* no_warnings_please = new LinearDialog(cd,(LinearTransformPlugin*)this);
    ++no_warnings_please;
  }

protected:
  virtual unsigned int plugin_version() const { return GUPPI_PLUGIN_INTERFACE_VERSION;}

};

LinearDialog::~LinearDialog()
{
  plugin_->unref();
}

static LinearTransformPlugin* global_linear_transform;

extern "C" {
 
// Symbol the Guppi module loader will look for.
G_MODULE_EXPORT  const Plugin* init_plugin() 
{
  // Can't just use a global; it segfaults on unload, 
  //  due to some _fini routine the compiler or linker 
  //  presumably adds.
  if (global_linear_transform == 0) 
    global_linear_transform = new LinearTransformPlugin;
  global_linear_transform->ref();
  return global_linear_transform;
}


G_MODULE_EXPORT void
g_module_unload (GModule *module)
{
#ifdef GNOME_ENABLE_DEBUG
  g_print ("Unloading linear transform plugin.\n");
#endif
  if (global_linear_transform) {
    g_assert(global_linear_transform->unloadable());
    delete global_linear_transform;
  }
}

}

