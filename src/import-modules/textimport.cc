// -*- C++ -*-

/* 
 * textimport.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "textimport.h"
#include "datatypemenu.h"
#include <limits.h>

class SeparatorProxy {
public:
  virtual const string& get_separator() = 0;
  virtual void set_separator(const string & s) = 0;
};

class ColumnSpecSeparatorProxy : public SeparatorProxy 
{
public:
  ColumnSpecSeparatorProxy(ColumnSpec & cs) : cs_(cs) {}
  virtual const string& get_separator() {return cs_.separator(); }
  virtual void set_separator(const string & s) { cs_.set_separator(s); }
private:
  ColumnSpecSeparatorProxy();
  ColumnSpec& cs_;
};

class ImportEngineSeparatorProxy : public SeparatorProxy
{
public:
  ImportEngineSeparatorProxy(ImportEngine & ie) : ie_(ie) {}
  virtual const string& get_separator() {return ie_.separator(); }
  virtual void set_separator(const string & s) { ie_.set_separator(s); }
private:
  ImportEngineSeparatorProxy();
  ImportEngine& ie_;
};

class SeparatorMenu : public Gtk_VBox {
public:
  SeparatorMenu(SeparatorProxy & sp);
  ~SeparatorMenu();

  enum Separators {
    NEWLINE,
    SPACE,
    TAB,
    CARRIAGE,
    CARRIAGE_NEWLINE,
    OTHER,
    SEPARATORS_LAST
  };

  static const char* const* const Separator_Labels;

  static const char* const* const Separator_Texts;

private:
  SeparatorProxy & sp_;

  Gtk_OptionMenu optionmenu_;
  Gtk_Menu menu_;
  Gtk_MenuItem* menuitems_[SEPARATORS_LAST];
  Gtk_Entry entry_;

  void mi_activated(Separators s);
  void entry_changed();
};

static const char* const __labels[] = {
  N_("Newline"),
  N_("Space"),
  N_("Tab"),
  N_("Carriage Return"),
  N_("Carriage Return - Newline"),
  N_("Other")
};

const char* const* const SeparatorMenu::Separator_Labels = __labels;

static const char* const __separators[] =  {
  "\n",
  " ",
  "\t",
  "\r",
  "\r\n",
  ""
};

const char* const* const SeparatorMenu::Separator_Texts = __separators;

SeparatorMenu::SeparatorMenu(SeparatorProxy & sp) 
  : Gtk_VBox(FALSE,GNOME_PAD_SMALL),
    sp_(sp),
    optionmenu_(),
    menu_(),
    entry_()
{
  optionmenu_.set_menu(menu_);
  
  int i = 0;
  bool use_other = true;
  while ( i < SEPARATORS_LAST ) {
    menuitems_[i] = new Gtk_MenuItem(Separator_Labels[i]);
    connect_to_method(menuitems_[i]->activate,
		      this,
		      &SeparatorMenu::mi_activated,
		      (Separators)i);
    menu_.append(*menuitems_[i]);
    if ( sp_.get_separator() == Separator_Texts[i] ) {
      optionmenu_.set_history(i);
      mi_activated((Separators)i);
      use_other = false;
    }
    menuitems_[i]->show();
    ++i;
  }

  if (use_other) {
    optionmenu_.set_history((int)OTHER);
    entry_.set_text(sp_.get_separator());
  }

  connect_to_method(entry_.changed,
		    this,
		    &SeparatorMenu::entry_changed);

  pack_start(optionmenu_,TRUE,TRUE);
  pack_start(entry_,TRUE,TRUE);

  optionmenu_.show();
  entry_.show();
}

SeparatorMenu::~SeparatorMenu()
{
  menu_.hide();
  optionmenu_.remove_menu();

  int i = 0;

  while ( i < SEPARATORS_LAST ) {
    menuitems_[i]->hide();
    delete menuitems_[i];
    ++i;
  }
}

void 
SeparatorMenu::mi_activated(Separators s)
{
  if ( s == OTHER ) {
    entry_.set_sensitive(true);
  }
  else {
    entry_.set_sensitive(false);
  }
  // triggers entry_changed
  entry_.set_text(Separator_Texts[s]);
}

void
SeparatorMenu::entry_changed()
{
  sp_.set_separator(entry_.get_text());
}

//////////////////////////////////////////////////////////////

class NumItems : public Gtk_HBox {
public:
  NumItems(const string & label);
  ~NumItems();

  Signal0 changed;

  guint get_value() const { return (guint) spin_.get_value_as_int(); }
  void  set_value(guint v) { adj_.set_value(v); }
  
private:
  Gtk_Label      label_;

  Gtk_Adjustment adj_;

  mutable Gtk_SpinButton spin_;

  NumItems();
};

NumItems::NumItems(const string & label) 
  : Gtk_HBox(FALSE,GNOME_PAD_SMALL),
    label_(label),
    // since we're getting the value as an int, we need a limit of INT_MAX
    adj_(0.0,0.0,INT_MAX,1.0,1.0,1.0),
    spin_(adj_)
{
  set_border_width(GNOME_PAD_SMALL);
  label_.set_alignment(0.0,0.5);

  pack_start(label_,TRUE,TRUE);
  pack_end(spin_,TRUE,TRUE);
  
  label_.show();
  spin_.show();

  connect_to_signal(spin_.changed,
		    changed);
}

NumItems::~NumItems() 
{

}

//////////////////////////////////////////////////////////////////

class ColumnEditor : public Gtk_VBox {
public:
  ColumnEditor(ColumnSpec & cs, const Store<DataType> & datatypes);
  ~ColumnEditor();

  ColumnSpec & cs() { return cs_; }

  Signal1<ColumnEditor*> add_after_requested;
  Signal1<ColumnEditor*> remove_requested;

private:
  ColumnSpec & cs_;
  ColumnSpecSeparatorProxy cssp_;

  Gtk_Label       sep_label_;

  SeparatorMenu  separators_;

  NumItems    num_elements_;
  
  Gtk_CheckButton no_valuelimit_;

  Gtk_OptionMenu typeoptionmenu_;
  DataTypeMenu   typemenu_;

  Gtk_Button     add_after_;
  Gtk_Button     remove_;

  void num_elements_changed() {
    cs_.set_max_elements(num_elements_.get_value());
  }
  void toggle_no_valuelimit() {
    cs_.set_has_max(!no_valuelimit_.get_state());
  }

};

ColumnEditor::ColumnEditor(ColumnSpec & cs, const Store<DataType> & datatypes)
  : Gtk_VBox(FALSE,GNOME_PAD_SMALL),
    cs_(cs),
    cssp_(cs),
    sep_label_(_("Separated from next element by: ")),
    separators_(cssp_),
    num_elements_(_("Maximum number of values: ")),
    no_valuelimit_(_("No limit on number of values")),
    typeoptionmenu_(),
    typemenu_(datatypes),
    add_after_(_("Add column after")),
    remove_(_("Remove this column"))
{
  pack_start(sep_label_);
  pack_start(separators_);
  pack_start(num_elements_);
  pack_start(no_valuelimit_);
  pack_start(typeoptionmenu_);
  pack_start(add_after_);
  pack_start(remove_);

  typeoptionmenu_.set_menu(typemenu_);

  num_elements_.set_value(cs_.max_elements());
  connect_to_method(num_elements_.changed,
		    this,
		    &ColumnEditor::num_elements_changed);
	      
  no_valuelimit_.set_state(!cs_.has_max());
  connect_to_method(no_valuelimit_.toggled,
		    this,
		    &ColumnEditor::toggle_no_valuelimit);

  // add_after doesn't work now; requires DataImport extensions,
  //  and is annoying to implement with a Gtk_Box.
  add_after_.set_sensitive(false);
  connect_to_signal(add_after_.clicked,
		    add_after_requested,
		    this);
  connect_to_signal(remove_.clicked,
		    remove_requested,
		    this);
}

ColumnEditor::~ColumnEditor()
{

}



//////////////////////////////////////////////////////////////

class TextImportDialogImpl : public Gtk_VBox {
public:
  TextImportDialogImpl(const string & text, 
		       ImportEngine & ie,
		       const Store<DataType> & datatypes);
  ~TextImportDialogImpl();

private:
  const string & text_;
  ImportEngine & ie_;
  const Store<DataType> & datatypes_;

  Gtk_HBox  column_top_box_;
  Gtk_Label column_label_;
  Gtk_Button column_add_button_;
  Gtk_HBox column_box_;
  Gtk_ScrolledWindow column_window_;
  ImportEngineSeparatorProxy iesp_;
  SeparatorMenu separator_;
  Gtk_Label     separator_label_;
  Gtk_HBox      separator_hbox_;


  list<ColumnEditor*> editors_;

  void display_column(ColumnSpec* cs);
  void add_column_cb();
  void remove_requested_cb(ColumnEditor* ce);
};

TextImportDialog::TextImportDialog(const string & text, 
				   ImportEngine & ie,
				   const Store<DataType> & datatypes)
  : Gnome_Dialog(_("Importing text file"),
		 GNOME_STOCK_BUTTON_OK,
		 GNOME_STOCK_BUTTON_CANCEL,
		 0),
    impl_(new TextImportDialogImpl(text,ie,datatypes))
{
  set_policy(FALSE,TRUE,FALSE);
  close_hides(true);
  vbox()->pack_start(*impl_,TRUE,TRUE);
  impl_->show_all();
}

TextImportDialog::~TextImportDialog()
{
  impl_->hide();
  delete impl_;
}

TextImportDialogImpl::TextImportDialogImpl(const string & text, 
					   ImportEngine & ie,
					   const Store<DataType> & datatypes)
  : Gtk_VBox(FALSE,GNOME_PAD), 
    text_(text), ie_(ie),
    datatypes_(datatypes),
    column_top_box_(FALSE,GNOME_PAD_SMALL),
    column_label_(_("Columns in the text file:")),
    column_add_button_(_("Add another column")),
    column_box_(TRUE,GNOME_PAD_SMALL),
    column_window_(),
    iesp_(ie),
    separator_(iesp_),
    separator_label_(_("Lines are separated by: ")),
    separator_hbox_(FALSE,GNOME_PAD_SMALL)
{
  size_t N = ie_.column_count();
  size_t i = 0;
  while ( i < N ) {
    display_column(ie_.column(i));
    ++i;
  }

  connect_to_method(column_add_button_.clicked,
		    this,
		    &TextImportDialogImpl::add_column_cb);

  pack_start(column_top_box_,FALSE,FALSE);
  column_top_box_.pack_start(column_label_,TRUE,TRUE);
  column_top_box_.pack_end(column_add_button_,TRUE,TRUE);
  
  column_window_.set_policy(GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);  
  pack_start(column_window_,TRUE,TRUE);
  column_window_.add(&column_box_);

  pack_start(separator_hbox_,FALSE,FALSE);
  separator_hbox_.pack_start(separator_label_);
  separator_hbox_.pack_end(separator_);
}

void 
TextImportDialogImpl::add_column_cb()
{
  ColumnSpec* cs = new ColumnSpec;
  ie_.add_column(cs);
  display_column(cs);
}

void 
TextImportDialogImpl::display_column(ColumnSpec* cs)
{
  ColumnEditor* ce = new ColumnEditor(*cs, datatypes_);
  column_box_.pack_start(*ce);
  editors_.push_back(ce);
  connect_to_method(ce->remove_requested,
		    this,
		    &TextImportDialogImpl::remove_requested_cb);
  ce->show_all();
}

void 
TextImportDialogImpl::remove_requested_cb(ColumnEditor* ce)
{
  ColumnSpec* cs = &ce->cs();
  ie_.remove_column(cs);
  ce->hide();
  ce->delete_self();
  delete cs;
}

TextImportDialogImpl::~TextImportDialogImpl()
{
  list<ColumnEditor*>::iterator i = editors_.begin();
  while ( i != editors_.end() ) {
    (*i)->hide();
    delete *i;
    ++i;
  }
}

