// -*- C++ -*-

/* 
 * plotplugin.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PLOTPLUGIN_H
#define GUPPI_PLOTPLUGIN_H

#include "plotdoc.h"
#include "plugin.h"
#include "plotcontext.h"

#define PLOT_PLUGIN_TYPE "plot"

class PlotPlugin : public Plugin {
public:
  PlotPlugin() {}
  virtual ~PlotPlugin() {}
  
  // Key used internally to classify the plugin (e.g. "plot")
  virtual const char* get_type() const;

  // Name to display in menus (e.g. "2D")
  virtual const char* get_menu_name() const = 0;

  // Human-readable name of the plugin (e.g. "Two Dimensional Plotting")
  virtual const char* get_name() const = 0;

  // Longer one-liner describing the plugin (e.g. "Draws 2D scatter
  // and line plots.")
  virtual const char* get_description() const = 0;

  // Make the PlotDoc type the plugin provides.
  virtual PlotDoc* make_plotdoc(PlotContext & pc) const = 0;

  virtual bool unloadable() const = 0;

protected:
  virtual unsigned int plugin_version() const = 0;
};

#endif
