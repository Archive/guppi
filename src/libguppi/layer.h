// -*- C++ -*-

/* 
 * layer.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_LAYER_H
#define GUPPI_LAYER_H

#include <list>
#include <algo.h>

#include <gnome--.h>
#include <gtk--plot.h>

#include "numericaxis.h"
#include "columndoc.h"
#include "named.h"

// A "Layer" is a layer of a single plot in a plot array.
// So a layer might draw the scatter of one set of data, or a regression
//  line, or what have you. It is in the "Doc" category along with Axis, 
//  but I'm moving away from that suffix.

class Layer : public Gtk_Signal_Base,
	      public Named
{
public:
  virtual ~Layer() { destroy(*this); }

  virtual Gtk_Widget* make_editor() = 0; 

  virtual void        draw(Plot_Area& target) const = 0;

  mutable Signal1<Layer&> destroy;

  // Need to call draw because something(s) changed.
  mutable Signal1<Layer&> need_draw;
  
  mutable Signal1<Layer&> name_changed;

protected:
  Layer() : Named(_("Untitled Layer")) {}

private:
  void named_callback(const string & name) {
    name_changed(*this);
  }

};

class XYLayer : public Layer {
public:
  virtual ~XYLayer() {}

  virtual Gtk_Widget* make_editor() = 0; 

  virtual void        draw(Plot_Area& target) const = 0;

  // OK to set to 0 meaning "none"
  virtual void set_x(const ColumnDoc* cd);
  
  virtual void set_y(const ColumnDoc* cd);
  
  // Can return 0!
  const ColumnDoc* x() const { return x_data_; }
  const ColumnDoc* y() const { return y_data_; }

  const NumericAxis* x_axis() const { return x_axis_; }
  const NumericAxis* y_axis() const { return y_axis_; }

  Signal1<const ColumnDoc*> x_changed;
  Signal1<const ColumnDoc*> y_changed;

protected:
  XYLayer(const NumericAxis* x, const NumericAxis* y);

  // get the string "<Name of X> vs. <Name of Y>"
  static string get_vs_string(const ColumnDoc* x, const ColumnDoc* y);

private:
  const NumericAxis* x_axis_;
  const NumericAxis* y_axis_;

  const ColumnDoc* x_data_;
  const ColumnDoc* y_data_;

  // Used to initialize.
  static const ColumnDoc* default_x_;
  static const ColumnDoc* default_y_;

  void x_axis_change_size(const NumericAxis & na) {
    need_draw(*this);
  }

  void y_axis_change_size(const NumericAxis & na) {
    need_draw(*this);
  }

  XYLayer();
};

class LayerStack;

// Used so the LayerStack doesn't have to know 
//  which kinds of layers are available.
class LayerSource {
public:

  virtual Gtk_Menu* 
  layer_menu(LayerStack& ls) = 0;

};

// LayerStack owns any layers you put in it.
class LayerStack : public Layer {
public:
  typedef list<Layer*>::iterator iterator;

  // begin() starts with the top layer
  iterator begin() { return layers_.begin(); }
  iterator end()   { return layers_.end();   }

  LayerStack(LayerSource & source) : source_(source) {}
  virtual ~LayerStack() {
    iterator i = layers_.begin();
    while ( i != layers_.end() ) {
      delete *i;
      ++i;
    }
  }
  
  virtual Gtk_Widget* make_editor(); 
  virtual Gtk_Menu*   make_layer_menu() { return source_.layer_menu(*this); }

  virtual void        draw(Plot_Area& target) const;

  void push_back(Layer & layer);

  void delete_layer(Layer & layer);

  Signal1<Layer&> layer_added;
  Signal1<Layer&> layer_deleted;

private:
  LayerSource & source_;

  // The layers are drawn starting from the end of the list;
  //  so the first element is "on top" of the drawing, but on
  //  the bottom of the stack.
  list<Layer*> layers_;

  LayerStack();
  LayerStack(const LayerStack&);
};


#endif
