// -*- C++ -*-

/* 
 * categorical.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libgnome/libgnome.h>

#include "categorical.h"

SimpleCategorical::SimpleCategorical() 
  : CategoricalData(_("Untitled Categorical")), 
    data_(0), n_(0)
{
  
}

SimpleCategorical::SimpleCategorical(const char** data, size_t N)
  : CategoricalData(_("Untitled Categorical")),
    data_(0), n_(0)
{
  set_data(data,N);
}

SimpleCategorical::~SimpleCategorical()
{
  delete_data();
}

const char*const*  
SimpleCategorical::data() const
{
  return data_;
}


const char*
SimpleCategorical::element(size_t i) const
{
  g_return_val_if_fail(i < n_, 0);
  return data_[i];
}


size_t   
SimpleCategorical::size() const
{
  return n_;
}

void 
SimpleCategorical::set_data(const char** data, size_t N)
{
  g_return_if_fail(data != 0);

  if (data_ != 0) delete_data();

  data_ = new char*[N];

  size_t i = 0;
  while ( i < N ) {
    g_return_if_fail(data[i] != 0);
    
    size_t len = strlen(data[i]);

    data_[i] = new char[len+1];
    strncpy(data_[i],data[i],len+1);
    ++i;
  }
  n_ = N;
}

void 
SimpleCategorical::delete_data()
{
  if (data_ == 0) return;

  size_t i = 0;
  while ( i < n_ ) {
    delete [] data_[i];
    ++i;
  }
  delete [] data_;
  n_ = 0;
  data_ = 0;
}




