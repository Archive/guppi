// -*- C++ -*-

/* 
 * canvasnumericaxis.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "canvasnumericaxis.h"
#include <gtk--plot.h>

CanvasNumericAxis::CanvasNumericAxis(Axis_Base::alignment_t a, NumericAxis* axis, 
				     Gnome_CanvasGroup& parent)
  : CanvasAxis(parent),
    axis_(axis), axis_base_(a), 
    labeller_(&axis_base_), axis_connected_(false)
{
  set_axis(axis_); // the above initialization is redundant
}

CanvasNumericAxis::~CanvasNumericAxis()
{

}

void 
CanvasNumericAxis::set_axis(NumericAxis* axis)
{
  axis_ = axis;

  if (axis_connected_) {
    c_[0].disconnect();
    c_[1].disconnect();
    axis_connected_ = false;
  }

  if (axis_) {
    c_[0] = connect_to_method(axis_->size_changed,
			      this,
			      &CanvasNumericAxis::change_size);
    c_[1] = connect_to_method(axis_->name_changed,
			      this,
			      &CanvasNumericAxis::change_name);
    axis_connected_ = true;
  }

  queue_buffer_draw();
}

NumericAxis* 
CanvasNumericAxis::get_axis()
{
  return axis_;
}

const NumericAxis* 
CanvasNumericAxis::get_axis() const
{
  return axis_;
}

void 
CanvasNumericAxis::get_size_hints(double* x, double* y, double* w, double* h)
{
  if (x) *x = 0.0;
  if (y) *y = 0.0;
  // hack hack hack for now
  if (w) *w = axis_base_.horizontal() ? 0.0 : 30.0;
  if (h) *h = axis_base_.vertical()   ? 0.0 : 30.0;
}

void 
CanvasNumericAxis::set_size      (double x, double y, double w, double h)
{
  double my_x2 = x + w;
  double my_y2 = y + h;

  x1_ = MIN(x, my_x2);
  x2_ = MAX(x, my_x2);
  y1_ = MIN(y, my_y2);
  y2_ = MAX(y, my_y2);

  reconfigure_impl();
}

void 
CanvasNumericAxis::set_bounds(double start, double stop)
{
  if (axis_) axis_->set_bounds(start,stop);
}

void 
CanvasNumericAxis::draw_on_buffer(Plot_Area& pa)
{
  if (axis_) {
    Gdk_Draw* draw = pa.drawing_target();
    g_return_if_fail(draw != 0);
    axis_base_.set_drawing_target(draw);
    axis_base_.set_bounds(axis_->get_start(), axis_->get_stop());
    draw->gc_save();
    draw->gc()->set_foreground("black");
    labeller_.label();
    draw->gc_restore();
  }
}

void 
CanvasNumericAxis::change_size(const NumericAxis& a)
{
  queue_buffer_draw();
}

void 
CanvasNumericAxis::change_name(const Axis& a)
{
  queue_buffer_draw();
}









