// -*- C++ -*-

/* 
 * messages.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_MESSAGES_H
#define GUPPI_MESSAGES_H

#include <gtk--.h>

// This is fairly inflexible; the idea is to allow an object to export
//  a list of messages it can receive. They're strings so that the 
//  Receiver and sender don't have to be in the same process (i.e. CORBA).
// A typical use creates a menu, where each item on the menu sends
//  one of the messages.

// It decouples things at compile time, too.

// Yes, it's a lot like signals; but more convenient in 
//  many ways for this particular purpose.

class Receiver {
protected:
  Receiver() {}

public:
  virtual ~Receiver() {}

  struct Message {
    const char* const id;    
    const char* const menu_name;
  };
  
  typedef const Message* const_iterator;

  // these can just return a pointer to a static array 
  virtual const_iterator begin() const = 0;
  virtual const_iterator end() const = 0;
  
  virtual void send(const char* id) = 0;
  
  // These return allocated objects
  Gtk_MenuItem* make_menuitem(const Message & m);
  Gtk_Menu*     make_menu    (const_iterator b, const_iterator e);

private:
  
};

#endif

