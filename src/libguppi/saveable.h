// -*- C++ -*-

/* 
 * saveable.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_SAVEABLE_H
#define GUPPI_SAVEABLE_H

#include <string>

// Something that can be saved.

class Saveable {
public:
  Saveable() : need_save_(true), filename_("") {}
  ~Saveable() {}

  // do we need to save
  bool need_save() { return need_save_; }
  // called when it's saved, so we no longer need to
  void saved()     { need_save_ = false; }

  void set_filename(const string & s) {
    filename_ = s;
  }
  void set_filename(const char * s) {
    filename_ = s;
  }

  const string & get_filename() {
    return filename_;
  }

protected:
  void not_saved() {
    need_save_ = false;
  }

private:
  bool need_save_;
  string filename_;
};


#endif
