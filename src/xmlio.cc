// -*- C++ -*-

/* 
 * xmlio.cc 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * XML code uses the Gnome XML library by Daniel Veillard, and 
 *  copies his code from Gnumeric.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "xmlio.h"

#include <goose/Exception.h>
#include <goose/DataType.h>

#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>

class ParseContext {
public:
  ParseContext(xmlDocPtr doc, xmlNsPtr ns) : 
    doc_(doc), ns_(ns)
    {}
  ~ParseContext() { 
    xmlFreeDoc(doc_); // this way it gets destroyed when the object goes out of scope on Exception
  }

  xmlDocPtr doc() { return doc_; }
  xmlNsPtr ns() { return ns_; }

private:
  xmlDocPtr doc_;
  xmlNsPtr  ns_;

};

GuppiDoc* 
read_guppidoc(ParseContext & pc, xmlNodePtr node)
{

  return 0;
}

GuppiDoc* 
load_guppidoc(const char* filename)
{
  g_return_val_if_fail(filename != 0, 0);

  struct stat sbuf;

  if (stat(filename, &statbuf) == -1) {
    throw new Exception(g_strerror(errno));
  }

  xmlDocPtr doc;

  doc = xmlParseFile (filename);
  
  if (doc == 0) {
    throw new Exception(_("Guppi was unable to parse this file.\n Perhaps it is not a Guppi file?\n You may wish to try the Import feature."));
  }
  
  if (doc->root == 0) {
    throw new Exception(_("Guppi found no data it could understand in this file.\n"
			  "Consider using Import if you did not create this file with Guppi."));
  }

  // Check our namespace and root element, which tells us whether this is really
  //  a GuppiDoc file.
  xmlNsPtr gup;
  
  gup = xmlSearchNsByHref(doc, doc->root, "http://www.gnome.org/guppi/");
  if (gup == 0) {
    xmlFreeDoc(doc);
    throw new Exception(_("This does not look like a Guppi file. If you didn't create\n"
			  "the file with Guppi, you should use the Import feature."));
  }

  // FIXME We could check the name of the toplevel element here before 
  //  reading it as if it were a GuppiDoc
 
  ParseContext pc(doc,gup);

  GuppiDoc* gd = read_guppidoc(pc, doc->root);

  // If an exception is thrown while reading, we just let it go up the call stack.

  if (gd == 0) {
    // shouldn't happen
    throw new Exception(_("Unknown problem reading Guppi file.")); // gee aren't we user-friendly
  }

  gd->set_filename(filename);

  // doc is destroyed when ParseContext goes out of scope.

  return gd;
}

void      
save_guppidoc(GuppiDoc & gd, const char* filename)
{

}

