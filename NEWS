
 *** Guppi 0.0.2 Released ***

Short version: the Guppi 0.0.2 source code can be found at:
http://www.gnome.org/guppi/

To compile Guppi, you will need gnome-libs from CVS and its
dependencies, plus the following:

From Gnome CVS (see http://www.gnome.org/):
 gtk--      (with Gnome support and Gtk--Draw)
 
From the Guppi ftp directory (ftp.gnome.org/pub/guppi):
 libgtkmmplot-0.0.1.tar.gz
 libgoose-0.0.4.tar.gz
 guppi-0.0.2.tar.gz

Note: For now Guppi is more fun if you don't make install. Just run
it in-place from the src directory. It will complain about undefined
plugin symbols and not find the transform plugins if you install,
this will be fixed soon. It is also a good idea to use:

CXXFLAGS='-Wall -ggdb -DGNOME_ENABLE_DEBUG' ./configure

This will give you all the debug spew and also provide sample
data columns to play with.

If you need help or spot problems, please mail guppi-list@gnome.org.
Check src/TODO for known bugs.

Long version:

This is the second public release of GUPPI, the GNOME/GNU Useful Plot
Production Interface. Guppi allows you to analyze data with
statistical manipulation and on-screen plots and charts. The goal of
the project is an easy-to-use program enabling researchers to
painlessly "play" with simple statistics and plots of their data.
We also wish to export plotting facilities via CORBA for use by 
Gnumeric and other interested Gnome programs.

0.0.2 is a pre-alpha release intended for developers only. End users
should not try this version. Guppi 0.0.2 is a near-complete rewrite of
Guppi 0.0.1.

Changes from 0.0.1 include:

 - Library shuffling: the drawing parts of Gtk--Draw have moved into CVS 
   Gtk--, and the plot parts are now in Gtk--Plot. Guppi uses both.

 - Scatter plots now have layers; they can also include regression lines 
   and line plots. So you can overlay a regression line on a scatter plot, 
   or overlay two different scatterplots.

 - Plugin infrastructure: libguppi, containing the basic Guppi objects
   and routines, is used by both plugins and the Guppi
   executable. Guppi proper demand-loads plugins to perform real work.
 
 - Data transformation via plugins; two sample plugins take the log of a 
   data set and perform an arbitrary linear transformation.

 - Improved text import: The main routines have been moved to Goose; 
   things are now less stable but more flexible.

 - Flirtation with CORBA; the build looks for it and we have an idea
   where we're going, but we haven't gone there yet.

 - Library enhancements: Goose and Gtk--Plot have new features used
   in Guppi. Most notably, Gtk--Plot now has a PlotLayout widget 
   which lays out an array of plots and axes. (However, this widget
   and its children (Gtk_Axis, Gtk_Plot) are already scheduled for a
   rewrite.)

 - and, of course: Breakage. See src/TODO in the distribution for more
   bugs and missing features.

Guppi has a mailing list, guppi-list@gnome.org. To subscribe, send
email to guppi-list-request@gnome.org with the subject "subscribe".
People interested in Guppi development may want to read the src/TODO 
file as well.

Guppi is released under the GNU General Public License, Goose and
Gtk--Plot fall under the LGPL. Everyone is welcome to participate in
these projects. Havoc Pennington <hp@pobox.com> is the primary author
of Guppi, and Jon Trowbridge <trow@emccta.com> is the primary author
of Goose and Gtk--Plot. Bug reports should go to us or to the Guppi
mailing list. Please check src/TODO to see if your bug is known.

Enjoy!


 *** Guppi 0.0.1 Released ***

This is the first public release of GUPPI, the GNOME/GNU Useful Plot
Production Interface. Guppi allows you to analyze data with
statistical manipulation and on-screen plots and charts. The goal of
the project is an easy-to-use program enabling researchers to
painlessly "play" with simple statistics and plots of their data.

0.0.1 is a pre-alpha release intended for developers only. End users
should not try this version. Guppi 0.0.1 doesn't do very much, but is
capable of importing various kinds of numeric data and creating stacks
or grids of axis-sharing scatter plots.

Guppi is written in C++, using the Gtk-- and Gnome-- GUI libraries. It
also relies on two custom libraries; the Gtk--Draw library provides
drawing and plotting primitives, and the Goose library provides simple
statistical procedures and data manipulation. Guppi should compile
with the latest gnome-libs release or the one in Gnome CVS, and the
'gtk--' module also from Gnome CVS. See http://www.gnome.org/ for
details on downloading this software.

Guppi is released under the GNU General Public License, and everyone
is welcome to participate in its development. The developers
responsible for this release are Jon Trowbridge <trow@emccta.com> and
Havoc Pennington <hp@pobox.com>.

Guppi does not yet have a mailing list, but we are working on it.

The Guppi source code can be found at:
http://www.gnome.org/guppi/

Enjoy!


