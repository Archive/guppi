// -*- C++ -*-

/* 
 * layerdialog.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "layerdialog.h"

LayerDialog::LayerDialog(Layer& l)
  : layer_(l)
{
  editor_ = layer_.make_editor();

  vbox()->pack_start(*editor_, true, true, 0);

  vbox()->show_all();
}

LayerDialog::~LayerDialog()
{
  if (editor_) editor_->hide(); // causes unrealize callback below.
}

void
LayerDialog::editor_unrealized()
{
  // This is a bad hack; but the editor can delete itself if the layer
  //  is deleted, so we need to fix it here. Awful relics of 
  //  previous architecture.
  editor_->delete_self(); // does nothing if it's already happened
  editor_ = 0;
}  
