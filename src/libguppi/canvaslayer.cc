// -*- C++ -*-

/* 
 * canvaslayer.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "canvaslayer.h"
#include "layerdialog.h"

CanvasLayer::CanvasLayer(Gnome_CanvasGroup & parent)
  : CanvasDrawable(parent, NULL),
    layer_(0), layer_connected_(false)
{
  // hack for now
  set_color("white");
}

CanvasLayer::~CanvasLayer()
{

}

void 
CanvasLayer::set_layer(Layer* layer)
{
  layer_ = layer;

  if (layer_connected_) {
    c_.disconnect();
    layer_connected_ = false;
  }

  if (layer_) {
    c_ = connect_to_method(layer_->need_draw,
			   this,
			   &CanvasLayer::draw_layer);
    layer_connected_ = true;
  }
  queue_buffer_draw();
}

Layer* 
CanvasLayer::get_layer()
{
  return layer_;
}

const Layer* 
CanvasLayer::get_layer() const
{
  return layer_;
}

void 
CanvasLayer::draw_on_buffer(Plot_Area& buffer)
{
  if (layer_) {
    layer_->draw(buffer);
  }
}

void 
CanvasLayer::get_size_hints(double* x, double* y, double* w, double* h)
{
  // We can be any old size.
  if (x) *x = 0.0;
  if (y) *y = 0.0;
  if (w) *w = 0.0;
  if (h) *h = 0.0;
}

void 
CanvasLayer::set_size      (double x, double y, double w, double h)
{
  double my_x2 = x + w;
  double my_y2 = y + h;

  x1_ = MIN(x, my_x2);
  x2_ = MAX(x, my_x2);
  y1_ = MIN(y, my_y2);
  y2_ = MAX(y, my_y2);

  reconfigure_impl();
}

void 
CanvasLayer::draw_layer(Layer& l)
{
  queue_buffer_draw();
}

gint 
CanvasLayer::event_impl(GdkEvent* event)
{
  if (event->type == GDK_BUTTON_PRESS)
    {
      g_assert(layer_);
      Gtk_Widget* dialog = new LayerDialog(*layer_);
      dialog->show();
    }
  g_print("event!\n");
  return TRUE;
}
