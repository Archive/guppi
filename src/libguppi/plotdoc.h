// -*- C++ -*-

/* 
 * plotdoc.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PLOTDOC_H
#define GUPPI_PLOTDOC_H

// A "document" which records information necessary to reconstruct
//  a particular plot. Does not store the DataColumn, since the
//  owning GuppiDoc will do that.

// PlotWindow needs to be changed to act as the "view" for one of these.
//  (which implies the actors are a "view" for the layers)

#include <list>

#include "named.h"
#include "iconed.h"

class Plot;

class PlotDoc : public Named,
		public Iconed
{
public:

  PlotDoc();
  virtual ~PlotDoc();
  
  // This Plot must be freed by the requester;
  //  it does not self-free when the plotdoc dies.
  virtual Plot*       make_plot  ()   = 0;
    
  class View : public Iconed::View,
	       public Named::View
  {
  public:
    virtual void destroy_model() = 0;
    
  };

  class Model : public Model<PlotDoc::View*> {
  public:

  };
  
  Model plotdoc_model;

private:

};

#endif
