// -*- C++ -*-

/* 
 * linelayer.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "linelayer.h"
#include "columndocmenu.h"

class LineEdit : public Gtk_Frame {
public:
  LineEdit(LineLayer& ll);
  ~LineEdit();

private:
  LineLayer& ll_;

  Gtk_VBox box_;

  Gtk_HBox colorbox_;
  Gtk_Label colorlabel_;
  Gnome_ColorPicker color_;

  Gtk_HBox       xbox_;
  Gtk_Label      xlabel_;
  Gtk_OptionMenu xoption_;
  ColumnDocMenu  xmenu_;
  Gtk_HBox       ybox_;
  Gtk_Label      ylabel_;
  Gtk_OptionMenu yoption_;
  ColumnDocMenu  ymenu_;

  void change_color(const LineLayer & layer);
  void set_color(guint r, guint g, guint b, guint a);
  void ll_destroyed(Layer & l);
  void x_chosen(const ColumnDoc* cd);
  void y_chosen(const ColumnDoc* cd);
  void x_changed(const ColumnDoc* cd);
  void y_changed(const ColumnDoc* cd);

  bool changing_column_;

  LineEdit();
};

LineEdit::LineEdit(LineLayer& ll) 
  : ll_(ll), box_(false,GNOME_PAD_SMALL),
    colorbox_(false,GNOME_PAD_SMALL),
    colorlabel_(_("Line color")),
    color_(),
    xbox_(false,0), xlabel_(_("X")), xoption_(), xmenu_(ll.cdl_),
    ybox_(false,0), ylabel_(_("Y")), yoption_(), ymenu_(ll.cdl_),
    changing_column_ (false)
{
  set_shadow_type(GTK_SHADOW_IN);
  add(&box_);

  change_color(ll_);

  connect_to_method(ll_.color_changed,
		    this,
		    &LineEdit::change_color);

  connect_to_method(color_.color_set,
		    this,
		    &LineEdit::set_color);

  connect_to_method(ll_.destroy,
		    this,
		    &LineEdit::ll_destroyed);

  // callbacks for future selections.
  connect_to_method(xmenu_.columndoc_activated,
		    this,
		    &LineEdit::x_chosen);

  connect_to_method(ymenu_.columndoc_activated,
		    this,
		    &LineEdit::y_chosen);

  connect_to_method(ll_.x_changed,
		    this,
		    &LineEdit::x_changed);

  connect_to_method(ll_.y_changed,
		    this,
		    &LineEdit::y_changed);

  colorbox_.pack_start(colorlabel_,true,true);
  colorbox_.pack_end(color_,false,false);
  box_.pack_start(colorbox_,false,false);
  colorbox_.show_all();

  xbox_.pack_start(xlabel_,false,false);
  xbox_.pack_end(xoption_,true,true);
  xbox_.set_border_width(GNOME_PAD_SMALL/2);
  xoption_.set_menu(xmenu_);
  ybox_.pack_start(ylabel_,false,false);
  ybox_.pack_end(yoption_,true,true);
  ybox_.set_border_width(GNOME_PAD_SMALL/2);
  yoption_.set_menu(ymenu_);
  box_.pack_start(xbox_,false,false);
  box_.pack_start(ybox_,false,false);

  // set menu to correct state. Must happen after set_menu on the 
  //  option menu.
  x_changed(ll_.x());
  y_changed(ll_.y());

  box_.show_all();
}

LineEdit::~LineEdit()
{

}

void 
LineEdit::change_color(const LineLayer & colored)
{
  const Gdk_Color & c(colored.get_color());
  color_.set(c.red(),c.green(),c.blue(),0);
}

void 
LineEdit::set_color(guint r, guint g, guint b, guint a)
{
  ll_.set_color_rgb(r,g,b);
}

void
LineEdit::ll_destroyed(Layer & l)
{
  delete_self();
}

void 
LineEdit::x_chosen(const ColumnDoc* cd)
{
  changing_column_ = true;
  ll_.set_x(cd);
  changing_column_ = false;
}

void 
LineEdit::y_chosen(const ColumnDoc* cd)
{
  changing_column_ = true;
  ll_.set_y(cd);
  changing_column_ = false;
}

void 
LineEdit::x_changed(const ColumnDoc* cd)
{
  if (changing_column_) return; // already active due to user intervention.
  if (cd) xoption_.set_history(xmenu_.index(cd));
}

void 
LineEdit::y_changed(const ColumnDoc* cd)
{
  if (changing_column_) return;
  if (cd) yoption_.set_history(ymenu_.index(cd));
}

////////////////////////////////////////////////

LineLayer::LineLayer(const NumericAxis* xa,
		     const NumericAxis* ya,
		     const ColumnDocList & cdl)
  : XYLayer(xa,ya), cdl_(cdl)
{
  set_name(get_vs_string(x(),y()) + _(" (line)"));
}


LineLayer::~LineLayer()
{
  
}

void 
LineLayer::set_x(const ColumnDoc* cd)
{
  set_name(get_vs_string(cd,y()) + _(" (line)"));
  XYLayer::set_x(cd);
}

void 
LineLayer::set_y(const ColumnDoc* cd)
{
  set_name(get_vs_string(x(),cd) + _(" (line)"));
  XYLayer::set_y(cd);
}

Gtk_Widget* 
LineLayer::make_editor()
{
  return new LineEdit(*this);
}

void        
LineLayer::draw(Plot_Area& target) const
{
  Gdk_Draw* draw = target.drawing_target();
  g_return_if_fail(draw != 0);

  draw->gc_save();

  draw->gc()->set_foreground(get_color());

  gchar* error_string = 0;
  if (x() == 0 && 
      y() == 0) { 
    error_string = _("No Data"); 
  }
  else if (x() == 0) { 
    error_string = _("No X Data");
  }
  else if (y() == 0) { 
    error_string = _("No Y Data"); 
  }

  const NumericAxis* xa, *ya;

  xa = x_axis();
  ya = y_axis();

  if (xa == 0 && ya == 0) {
    error_string = _("No axes");
  }
  else if (xa == 0) {
    error_string = _("No X axis");
  }
  else if (ya == 0) {
    error_string = _("No Y axis");
  }

  if (error_string) {
    draw->draw_string(30,30,error_string);
    draw->gc_restore();
    return;
  } 
 
  g_assert(xa);
  g_assert(ya);
  g_assert(x());
  g_assert(y());
  
  target.set_x_bounds(xa->get_start(), xa->get_stop());
  target.set_y_bounds(ya->get_start(), ya->get_stop());
 
  // Can only plot the smallest number of points on either axis
  size_t minsize = MIN(x()->ds().size(), y()->ds().size());
    
  Plot_Data xpd(x()->ds().data(),
		minsize);
  
  Plot_Data ypd(y()->ds().data(),
		minsize);
  
  target.draw_line_plot(xpd,ypd);

  draw->gc_restore();
  return;
}



