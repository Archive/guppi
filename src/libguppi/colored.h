// -*- C++ -*-

/* 
 * colored.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_COLORED_H
#define GUPPI_COLORED_H

// Objects with a color.

#include <string>
#include <gtk--draw.h>
#include <libgnome/gnome-util.h>

class Colored {
public:
  Colored() : color_("black") {}
  virtual ~Colored() {}

  // for reading
  const Gdk_Color& get_color() const { return color_; }

  void set_color_grey(gushort g) {
    color_.set_grey(g);
    color_changed_callback(*this);
  }

  void set_color_grey_p(gdouble g) {
    color_.set_grey_p(g);
    color_changed_callback(*this);
  }

  void set_color_rgb(gushort red, gushort green, gushort blue) {
    color_.set_rgb(red,green,blue);
    color_changed_callback(*this);
  }

  void set_color_rgb_p(gdouble red, gdouble green, gdouble blue) {
    color_.set_rgb_p(red,green,blue);
    color_changed_callback(*this);
  }

  void set_color_hsv(gdouble h, gdouble s, gdouble v) {
    color_.set_hsv(h,s,v);
    color_changed_callback(*this);
  }

  void set_color_hsl(gdouble h, gdouble s, gdouble l) {
    color_.set_hsl(h,s,l);
    color_changed_callback(*this);
  }

  void set_color(const gchar* s) {
    color_.set(s);
    color_changed_callback(*this);
  }

protected:
  virtual void color_changed_callback(const Colored& c) = 0;

private:
  Gdk_Color color_;
};

#endif
