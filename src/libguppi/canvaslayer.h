// -*- C++ -*-

/* 
 * canvaslayer.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_CANVASLAYER_H
#define GUPPI_CANVASLAYER_H

/* Layer canvas item */

#include <gtk--plot/arraylayout.h>
#include "layer.h"
#include "canvasdrawable.h"

class CanvasLayer : public CanvasDrawable,
		    public PlotLayoutHandle
{
public:
  CanvasLayer(Gnome_CanvasGroup& parent);
  virtual ~CanvasLayer();

  void set_layer(Layer* layer);
  Layer* get_layer();
  const Layer* get_layer() const;

  // LayoutHandle interface
  virtual void get_size_hints(double* x, double* y, double* w, double* h);
  virtual void set_size      (double x, double y, double w, double h);

  // canvas overrides
  virtual gint event_impl(GdkEvent* event);

protected:
  virtual void draw_on_buffer(Plot_Area& buffer);

  void draw_layer(Layer& l);

private:
  Layer* layer_;

  Connection c_;
  bool layer_connected_;

};

#endif
