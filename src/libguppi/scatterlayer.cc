// -*- C++ -*-

/* 
 * scatterlayer.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "scatterlayer.h"
#include "markermenu.h"
#include "columndocmenu.h"

class ScatterEdit : public Gtk_Frame {
public:
  ScatterEdit(ScatterLayer& sl);
  ~ScatterEdit();

private:
  ScatterLayer& sl_;

  Gtk_VBox box_;
  Gtk_OptionMenu markeroption_;
  MarkerMenu mm_;
  Gtk_HBox   markerbox_;
  Gtk_Label  markerlabel_;
  Gnome_ColorPicker markercolor_;
  //Gnome_ColorSelector bgcolor_;
  Gtk_HBox       xbox_;
  Gtk_Label      xlabel_;
  Gtk_OptionMenu xoption_;
  ColumnDocMenu  xmenu_;
  Gtk_HBox       ybox_;
  Gtk_Label      ylabel_;
  Gtk_OptionMenu yoption_;
  ColumnDocMenu  ymenu_;


  void marker_chosen(Plot_Area::marker_t);
  void change_marker(const ScatterLayer & sl);
  void change_color(const ScatterLayer & layer);
  void set_color(guint r, guint g, guint b, guint a);
  void sl_destroyed(Layer & l);
  void x_chosen(const ColumnDoc* cd);
  void y_chosen(const ColumnDoc* cd);
  void x_changed(const ColumnDoc* cd);
  void y_changed(const ColumnDoc* cd);

  bool changing_column_;

  ScatterEdit();
};

ScatterEdit::ScatterEdit(ScatterLayer& sl) 
  : sl_(sl), box_(false,GNOME_PAD_SMALL),
    markeroption_(), mm_(),
    markerbox_(false,GNOME_PAD_SMALL),
    markerlabel_(_("Marker color: ")),
    markercolor_(),
    xbox_(false,0), xlabel_(_("X")), xoption_(), xmenu_(sl.cdl_),
    ybox_(false,0), ylabel_(_("Y")), yoption_(), ymenu_(sl.cdl_),
    changing_column_ (false)
{
  set_shadow_type(GTK_SHADOW_IN);
  add(&box_);
  box_.pack_start(markeroption_,false,false);

  markeroption_.set_menu(mm_);

  change_marker(sl_);
  change_color(sl_);

  connect_to_method(sl_.marker_changed,
		    this,
		    &ScatterEdit::change_marker);

  connect_to_method(mm_.marker_activated,
		    this,
		    &ScatterEdit::marker_chosen);

  connect_to_method(sl_.color_changed,
		    this,
		    &ScatterEdit::change_color);

  connect_to_method(markercolor_.color_set,
		    this,
		    &ScatterEdit::set_color);

  connect_to_method(sl_.destroy,
		    this,
		    &ScatterEdit::sl_destroyed);

  // callbacks for future selections.
  connect_to_method(xmenu_.columndoc_activated,
		    this,
		    &ScatterEdit::x_chosen);

  connect_to_method(ymenu_.columndoc_activated,
		    this,
		    &ScatterEdit::y_chosen);

  connect_to_method(sl_.x_changed,
		    this,
		    &ScatterEdit::x_changed);

  connect_to_method(sl_.y_changed,
		    this,
		    &ScatterEdit::y_changed);

  markerbox_.pack_start(markerlabel_,true,true);
  markerbox_.pack_end(markercolor_,false,false);
  box_.pack_start(markerbox_,false,false);
  markerbox_.show_all();

  xbox_.pack_start(xlabel_,false,false);
  xbox_.pack_end(xoption_,true,true);
  xbox_.set_border_width(GNOME_PAD_SMALL/2);
  xoption_.set_menu(xmenu_);
  ybox_.pack_start(ylabel_,false,false);
  ybox_.pack_end(yoption_,true,true);
  ybox_.set_border_width(GNOME_PAD_SMALL/2);
  yoption_.set_menu(ymenu_);
  box_.pack_start(xbox_,false,false);
  box_.pack_start(ybox_,false,false);

  // set menu to correct state. Must happen after set_menu on the 
  //  option menu.
  x_changed(sl_.x());
  y_changed(sl_.y());

  box_.show_all();
}

ScatterEdit::~ScatterEdit()
{
  markeroption_.remove_menu();
}

void 
ScatterEdit::marker_chosen(Plot_Area::marker_t m)
{

  sl_.set_marker(m);
}

void 
ScatterEdit::change_marker(const ScatterLayer & sl)
{
  markeroption_.set_history(sl.get_marker());
}

void 
ScatterEdit::change_color(const ScatterLayer & colored)
{
  const Gdk_Color & c(colored.get_color());
  markercolor_.set(c.red(),c.green(),c.blue(),0);
}

void 
ScatterEdit::set_color(guint r, guint g, guint b, guint a)
{
  sl_.set_color_rgb(r,g,b);
}

void
ScatterEdit::sl_destroyed(Layer & l)
{
  delete_self();
}

void 
ScatterEdit::x_chosen(const ColumnDoc* cd)
{
  changing_column_ = true;
  sl_.set_x(cd);
  changing_column_ = false;
}

void 
ScatterEdit::y_chosen(const ColumnDoc* cd)
{
  changing_column_ = true;
  sl_.set_y(cd);
  changing_column_ = false;
}

void 
ScatterEdit::x_changed(const ColumnDoc* cd)
{
  if (changing_column_) return; // already active due to user intervention.
  if (cd) xoption_.set_history(xmenu_.index(cd));
}

void 
ScatterEdit::y_changed(const ColumnDoc* cd)
{
  if (changing_column_) return;
  if (cd) yoption_.set_history(ymenu_.index(cd));
}

////////////////////////////////////////////////

ScatterLayer::ScatterLayer(const NumericAxis* xa,
			   const NumericAxis* ya,
			   const ColumnDocList & cdl)
  : XYLayer(xa,ya), cdl_(cdl)
{
  set_name(get_vs_string(x(),y()) + _(" (scatter)"));
}


ScatterLayer::~ScatterLayer()
{
  
}

void 
ScatterLayer::set_x(const ColumnDoc* cd)
{
  set_name(get_vs_string(cd,y()) + _(" (scatter)"));
  XYLayer::set_x(cd);
}

void 
ScatterLayer::set_y(const ColumnDoc* cd)
{
  set_name(get_vs_string(x(),cd) + _(" (scatter)"));
  XYLayer::set_y(cd);
}

Gtk_Widget* 
ScatterLayer::make_editor()
{
  return new ScatterEdit(*this);
}

void        
ScatterLayer::draw(Plot_Area& target) const
{
  Gdk_Draw* draw = target.drawing_target();
  g_return_if_fail(draw != 0);

  draw->gc_save();

  draw->gc()->set_foreground(get_color());

  gchar* error_string = 0;
  if (x() == 0 && 
      y() == 0) { 
    error_string = _("No Data"); 
  }
  else if (x() == 0) { 
    error_string = _("No X Data");
  }
  else if (y() == 0) { 
    error_string = _("No Y Data"); 
  }

  const NumericAxis* xa, *ya;

  xa = x_axis();
  ya = y_axis();

  if (xa == 0 && ya == 0) {
    error_string = _("No axes");
  }
  else if (xa == 0) {
    error_string = _("No X axis");
  }
  else if (ya == 0) {
    error_string = _("No Y axis");
  }

  if (error_string) {
    draw->draw_string(30,30,error_string);
    draw->gc_restore();
    return;
  } 
 
  g_assert(xa);
  g_assert(ya);
  g_assert(x());
  g_assert(y());

  target.set_x_bounds(xa->get_start(), xa->get_stop());
  target.set_y_bounds(ya->get_start(), ya->get_stop());
  
  // Can only plot the smallest number of points on either axis
  size_t minsize = MIN(x()->ds().size(), y()->ds().size());
    
  Plot_Data xpd(x()->ds().data(),
		minsize);
  
  Plot_Data ypd(y()->ds().data(),
		minsize);
  
  target.draw_scatter_plot(xpd,
			   ypd,
			   marker_);

  draw->gc_restore();
  return;
}

