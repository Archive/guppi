// -*- C++ -*-

/* 
 * plot.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PLOT_H
#define GUPPI_PLOT_H

// The "view" for a PlotDoc.
// It is also a "model" observed by the
//  window it's embedded in.

#include <gtk--.h>

#include <list>
#include "plotdoc.h"

class Plot : public PlotDoc::View {
public:
  Plot();
  virtual ~Plot();

  // Any returned widgets are deleted when the Plot is deleted
  virtual list<Gtk_MenuItem*>& menu() = 0;    // menu to display to user; not to be freed
  virtual Gtk_Widget* widget() = 0;
      
  class View : public ::View 
  {
  public:
    virtual void change_status(const gchar* status) = 0;
    
  };

  class Model : public Model<Plot::View*> {
  public:
    void status_changed(const gchar* status) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_status(status);
	++i;
      }
      unlock_views();
    }
  };
  
  Model plot_model;

protected:
  // by default, this is empty; each subclass can add stuff to it.
  //  then the menu() method will generally return it.
  list<GtkMenuItem*> menu_;

private:

};

#endif
