// -*- C++ -*-

/* 
 * datatypes.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_DATATYPES_H
#define GUPPI_DATATYPES_H

#include <gnome--.h> // i18n
#include "store.h"
#include <goose/DataType.h>

class GuppiRealType : public RealType
{
public:
  virtual string name () const { return _("Real"); }

};

// Register an instance of each type with this 
// Store<DataType>
void register_all_builtin_datatypes(Store<DataType> & store);

#endif

