// -*- C++ -*-

/* 
 * transformplugin.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_TRANSFORMPLUGIN_H
#define GUPPI_TRANSFORMPLUGIN_H

#include "plugin.h"
#include "columndoc.h"

#define TRANSFORM_PLUGIN_TYPE "transform"

class TransformPlugin : public Plugin {
public:
  TransformPlugin() {}
  virtual ~TransformPlugin() {}
  
  virtual const char* get_type() const;

  virtual const char* get_menu_name() const = 0;

  virtual const char* get_name() const = 0;

  virtual const char* get_description() const = 0;

  virtual bool unloadable() const { return refcount_ == 0; }

  // If you don't want to affect the original ColumnDoc you need
  //  to copy it before passing it in.
  virtual void     transform(ColumnDoc& cd) const = 0;
  
protected:

protected:
  virtual unsigned int plugin_version() const = 0;

};

#endif
