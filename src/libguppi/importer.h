// -*- C++ -*-

/* 
 * importer.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_IMPORTER_H
#define GUPPI_IMPORTER_H

#include <list>
#include <string>
#include <gtk--sig.h>
#include <gtk--/object.h> // Trashcan

// Base class for importers of any doc type

template <class DocType>
class Importer : public Gtk_Signal_Base {
public:
  Importer() {}
  virtual ~Importer() {destroy();}
  
  void delete_self() { trashcan.throw_away(this); }

  // user has to free the list and the docs
  virtual list<DocType*> * get_docs() = 0; 

  // Subclasses have to have a static get_name method; 
  //  how to express that in the toplevel class?

  // By using signals we avoid modal dialogs and the like.

  Signal0 destroy;

  // When the import is done
  Signal1<Importer<DocType>*> finished;

  // If it doesn't get done
  Signal1<Importer<DocType>*> cancelled;

private:
  static Gtk_Trashcan<Importer<DocType> > trashcan;
};

template <class DocType>
Gtk_Trashcan<Importer<DocType> > Importer<DocType>::trashcan;

#endif
