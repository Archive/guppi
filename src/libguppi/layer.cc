// -*- C++ -*-

/* 
 * layer.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "layer.h"
#include "layerstackedit.h"

// These will just be whatever the last value set was.
const ColumnDoc* XYLayer::default_x_ = 0;
const ColumnDoc* XYLayer::default_y_ = 0;

XYLayer::XYLayer(const NumericAxis* x, const NumericAxis* y) : 
  x_axis_(x), y_axis_(y), x_data_(default_x_), y_data_(default_y_) 
{
  // FIXME this conditional is a temporary hack; we used to require an axis
  //  at all times, now we need set functions.
  if (x && y) {
    connect_to_method(x_axis_->size_changed,
                      this,
                      &XYLayer::x_axis_change_size);
    
    connect_to_method(y_axis_->size_changed,
                      this,
                      &XYLayer::y_axis_change_size);
  }
}

string 
XYLayer::get_vs_string(const ColumnDoc* x, const ColumnDoc* y)
{
  string xs = x ? x->get_name() : "_________";
  string ys = y ? y->get_name() : "_________";
  return xs + _(" vs. ") + ys; 
}

void
XYLayer::set_x(const ColumnDoc* cd) 
{
  x_data_ = cd;
  default_x_ = cd;
  x_changed(cd);
  need_draw(*this);
}

void
XYLayer::set_y(const ColumnDoc* cd) 
{
  y_data_ = cd;
  default_y_ = cd;
  y_changed(cd);
  need_draw(*this);
}
  
Gtk_Widget* 
LayerStack::make_editor()
{
  return new LayerStackEdit(*this);
}

void        
LayerStack::draw(Plot_Area& target) const 
{
  // going backward to draw first element last, as the top layer.
  list<Layer*>::const_reverse_iterator i = layers_.rbegin();
  while ( i != layers_.rend() ) {
    (*i)->draw(target);
    ++i;
  }
}

void 
LayerStack::push_back(Layer & layer) 
{
  layers_.push_back(&layer);
  connect_to_signal(layer.need_draw,
		    need_draw);
  layer_added(layer);
  need_draw(*this);
}

void 
LayerStack::delete_layer(Layer & layer) 
{
  iterator i = find(begin(),end(),&layer);
  if (i != end()) {
    Layer * l = *i;
    layers_.erase(i);
    layer_deleted(layer);
    delete l;
    need_draw(*this);      
  }
}
