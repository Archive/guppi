// -*- C++ -*-

/* 
 * markermenu.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "markermenu.h"

MarkerMenu::MarkerMenu() : Gtk_Menu()
{
  make_menuitems();
}

MarkerMenu::~MarkerMenu()
{
#if 0
  list<Gtk_MenuItem*>::iterator i = menuitems_.begin();

  while (i != menuitems_.end()) {
    remove(*i);
    (*i)->hide();
    delete *i;
    ++i;
  }
#endif
}

// replace with XPM's eventually
static const char* const item_strings[] = {
    "No marker",    
    "Dot",     
    "Circle",  
    "Filled Circle",  
    "Box", 
    "Filled Box",
    "Diamond", 
    "Filled Diamond", 
    "Plus sign",           
    "Times sign",          
    "Horizontal tick",
    "Vertical tick",  
    "Horizontal rule",
    "Vertical rule",  
    "Cross rule"
};

static const size_t num_items = sizeof(item_strings)/sizeof(item_strings[0]);

void
MarkerMenu::make_menuitems()
{
  size_t i = 0;

  while ( i < num_items ) {
    Gtk_MenuItem* mi = new Gtk_MenuItem(item_strings[i]);
    append(*mi);
    mi->show();
    
    connect_to_method( mi->activate,
		       this,
		       &MarkerMenu::activate_callback,
		       (Plot_Area::marker_t)i );
    
    menuitems_.push_back(mi);
    ++i;
  }
}

