// -*- C++ -*-

/* 
 * columnwindow.cc 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "columnwindow.h"
#include "guppi.h"
#include "statsdatabase.h"

#include <goose/Exception.h>

class ColumnWindowImpl : public Gtk_Notebook,
                         public ColumnDoc::View {
public:
  ColumnWindowImpl(ColumnDoc& cd, ColumnWindow& dialog);
  ~ColumnWindowImpl();

private:
  ColumnDoc* cd_;
  ColumnWindow& dialog_;

  Gtk_Label title_;

  Gtk_Entry name_entry_;

  Gtk_VBox  statsbox_;
  Gtk_Label statslabel_;
  Gtk_ScrolledWindow statsscroll_;
  Gtk_CList stats_;
  
  Gtk_VBox  databox_;
  Gtk_Label datalabel_;
  Gtk_ScrolledWindow datascroll_;
  Gtk_CList data_;

  bool changing_name_entry_;

  void change_name(const string & name);
  void change_data(const ColumnDoc & cd);
  
  void destroy_model() { if (cd_) {cd_ = 0; dialog_.delete_self();} }

  void name_entry_change();
#if 0
  void name_entry_focus_out(GdkEventFocus* ef) {
    name_entry_activate();
  }
#endif

  typedef double (DataSet::*statfunc)() const;
};

ColumnWindow::ColumnWindow(ColumnDoc& cd)
  :  Gnome_Dialog("", GNOME_STOCK_BUTTON_CLOSE, NULL), 
     impl_(0)
{
  impl_ = new ColumnWindowImpl(cd,*this);
  set_policy(FALSE,TRUE,FALSE);
}

ColumnWindow::~ColumnWindow()
{
  delete impl_;
}

ColumnWindowImpl::ColumnWindowImpl(ColumnDoc& cd, ColumnWindow& dialog) :
  //  Gtk_HBox(FALSE,GNOME_PAD),
  cd_(&cd),
  dialog_(dialog),
  title_(_("Column Information")),
  name_entry_(),
  statsbox_(FALSE,GNOME_PAD_SMALL),
  statslabel_(_("Statistics")),
  statsscroll_(),
  stats_(2),
  databox_(FALSE,GNOME_PAD_SMALL),
  datalabel_(_("Data")),
  datascroll_(),
  data_(2),
  changing_name_entry_(false)
{
  dialog.vbox()->pack_start(title_,FALSE,FALSE,GNOME_PAD_SMALL);
  title_.show();
  dialog.vbox()->pack_start(name_entry_,FALSE,FALSE,GNOME_PAD_SMALL);
  name_entry_.show();

  dialog.vbox()->pack_start(*this,TRUE,TRUE,0);

  connect_to_method(name_entry_.changed,
		    this,
		    &ColumnWindowImpl::name_entry_change);

  data_.set_usize(200,100);
  data_.set_column_width(0,80);
  data_.set_column_width(1,120);
  data_.set_column_title(0,_("Observation"));
  data_.set_column_title(1,_("Value"));
  data_.column_titles_show();
  data_.column_titles_passive();
  datascroll_.set_policy(GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);
  datascroll_.add(data_);
  databox_.pack_start(datascroll_,TRUE,TRUE);
  append_page(databox_,datalabel_);

  stats_.set_usize(280,100);
  stats_.set_column_title(0,_("Statistic"));
  stats_.set_column_width(0,150);
  stats_.set_column_title(1,_("Value"));
  stats_.set_column_width(1,80);
  stats_.column_titles_show();
  statsscroll_.set_policy(GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);
  statsscroll_.add(stats_);
  statsbox_.pack_start(statsscroll_,TRUE,TRUE);
  append_page(statsbox_,statslabel_);

  change_name(cd_->get_name());
  change_data(*cd_);

  cd_->named_model.add_view(*this);
  cd_->columndoc_model.add_view(*this);

  show_all();
}

ColumnWindowImpl::~ColumnWindowImpl()
{
  if (cd_) {
    cd_->named_model.remove_view(*this);
    cd_->columndoc_model.remove_view(*this);
  }
}

void
ColumnWindowImpl::change_data(const ColumnDoc & cd)
{
  const DataSet& ds(cd.ds());

  // First fill in the data box.
  data_.clear();

  size_t i = 0;
  size_t n = ds.size();

  while ( i < n ) {
    gchar buf1[100];
    // start counting at 1
    g_snprintf(buf1,100,"%u",i+1);
    // this will need formatting according
    //  to data type.
    gchar buf2[100];
    g_snprintf(buf2,100,"%.4f",ds.data(i));
    // bah
    const gchar* text[2];
    text[0] = &buf1[0];
    text[1] = &buf2[0];
    
    data_.append(text);
    ++i;
  }

  // now the statistical info.

  stats_.clear();

  const gchar* text[2];


  gchar buf[256];

  g_snprintf(buf,100,"%u",ds.size());
  text[0] = _("N");
  text[1] = buf;
  stats_.append(text);

  StatsDB::const_iterator j = global_guppi_statsdb.single_begin();
  while ( j != global_guppi_statsdb.single_end()) {
    const string name(*j);
    gchar buf2[256];
    
    g_snprintf(buf2, 255, "%s", name.c_str());
    text[0] = buf2;

    try {
      g_snprintf(buf,255,"%f",global_guppi_statsdb.statistic(name,ds));
    }
    catch (Exception & e) {
#ifdef GNOME_ENABLE_DEBUG
      cerr << "Goose exception caught!" << endl;
#endif
      g_snprintf(buf, 255, "%s", e.what().c_str());
    }

    text[1] = buf;
    stats_.append(text);
    ++j;
  }
}

void
ColumnWindowImpl::change_name(const string & name)
{
  changing_name_entry_ = true;
  name_entry_.set_text(name);
  dialog_.set_title(name);
  changing_name_entry_ = false;
}

void
ColumnWindowImpl::name_entry_change()
{
  if (changing_name_entry_) return; // we set the entry, not the user

  cd_->set_name(name_entry_.get_text());
}

