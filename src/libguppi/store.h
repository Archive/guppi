// -*- C++ -*-

/* 
 * store.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_STORE_H
#define GUPPI_STORE_H

#include <vector>

template <class ProxyType>
class Store {
public:
  Store() {}
  ~Store() {}

  typedef vector<const ProxyType*>::const_iterator const_iterator;

  const_iterator begin() const { return proxies_.begin(); }
  const_iterator end()   const { return proxies_.end(); }
  size_t         size()  const { return proxies_.size(); }


  const ProxyType&
  get_proxy_number(size_t i) const { 
    return *(proxies_[i]);
  }  
  
  void 
  register_proxy(const ProxyType & p) {
    proxies_.push_back(&p);
  }

private:
  vector<const ProxyType*> proxies_;
  Store(const Store& source); // not used.
  Store& operator=(const Store& source); // not used.
};

#endif

