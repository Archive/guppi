// -*- C++ -*-

/* 
 * xydoc.cc 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "xydoc.h"
#include "xyplot.h"
#include "layer.h"
#include "scatterlayer.h"
#include "regressionlayer.h"
#include "linelayer.h"

//////////////////////////////////////////////////////
class XYSource : public LayerSource {
public:
  XYSource(ColumnDocList & cdl) : cdl_(cdl) {}
  
  virtual Gtk_Menu* 
  layer_menu(LayerStack& ls);

protected:
  void scatter_cb(LayerStack* ls) {
    ls->push_back(*(new ScatterLayer(0,0,cdl_)));
  }

  void regression_cb(LayerStack* ls) {
    ls->push_back(*(new RegressionLayer(0,0,cdl_)));
  }

  void line_cb(LayerStack* ls) {
    ls->push_back(*(new LineLayer(0,0,cdl_)));
  }

private:
  ColumnDocList& cdl_;

  friend class XYLayerMenu;
};

// Allows deletion of the returned Gtk_Menu to do 
//  the right thing (delete children too)
class XYLayerMenu : public Gtk_Menu {
public:
  XYLayerMenu(XYSource & xys, LayerStack & ls);
  virtual ~XYLayerMenu();

private:
  Gtk_MenuItem scatter_;
  Gtk_MenuItem regression_;
  Gtk_MenuItem line_;
};

XYLayerMenu::XYLayerMenu(XYSource & xys, LayerStack & ls)
  : scatter_(_("Scatter")),
    regression_(_("Regression")),
    line_(_("Line"))
{
  append(scatter_);
  append(regression_);
  append(line_);

  connect_to_method(scatter_.activate,
		    &xys,
		    &XYSource::scatter_cb,
		    &ls);
  connect_to_method(regression_.activate,
		    &xys,
		    &XYSource::regression_cb,
		    &ls);
  connect_to_method(line_.activate,
		    &xys,
		    &XYSource::line_cb,
		    &ls);

  scatter_.show();
  regression_.show();
  line_.show();
}

XYLayerMenu::~XYLayerMenu()
{

}

Gtk_Menu* 
XYSource::layer_menu(LayerStack & ls)
{
  return new XYLayerMenu(*this, ls);
}

////////////////////////////////////////////////////

XYDoc::XYDoc(PlotContext & pc) 
  : ArrayDoc(pc),
    source_(new XYSource(pc.cdl))
{

}


XYDoc::~XYDoc()
{
  // all layers referring to this should have been deleted
  delete source_;
}

Plot*
XYDoc::make_plot()
{
  return new XYPlot(*this);
}

LayerStack* 
XYDoc::make_layerstack()
{
  LayerStack* ls = new LayerStack(*source_);
  return ls;
}

