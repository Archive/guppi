// -*- C++ -*-

/* 
 * arraydoc.cc 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "arraydoc.h"


ArrayDoc::ArrayDoc(PlotContext& pc) 
  : pc_(pc)
{

}

ArrayDoc::~ArrayDoc()
{

}


LayerStack* 
ArrayDoc::get_layer(size_t row, size_t col) { 
  g_return_val_if_fail(row < cells_.rows(),0);
  g_return_val_if_fail(col < cells_.columns(),0);

  if (cells_[row][col] == 0) return 0;
  return cells_[row][col]->layerstack;
}

const LayerStack* 
ArrayDoc::get_layer(size_t row, size_t col) const {
  g_return_val_if_fail(row < cells_.rows(),0);
  g_return_val_if_fail(col < cells_.columns(),0);

  if (cells_[row][col] == 0) return 0;
  return cells_[row][col]->layerstack;
}

Axis* 
ArrayDoc::get_axis(size_t row, size_t col, Axis_Base::alignment_t a) {
  g_return_val_if_fail(row < cells_.rows(),0);
  g_return_val_if_fail(col < cells_.columns(),0);

  if (cells_[row][col] == 0) return 0;
  return cells_[row][col]->axes[a];
}

const Axis* 
ArrayDoc::get_axis(size_t row, size_t col, Axis_Base::alignment_t a) const {
  g_return_val_if_fail(row < cells_.rows(),0);
  g_return_val_if_fail(col < cells_.columns(),0);

  if (cells_[row][col] == 0) return 0;
  return cells_[row][col]->axes[a];
}

// This will not delete stuff if it shrinks the array; you need to do that 
//  first.
void 
ArrayDoc::resize(size_t newrows, size_t newcols) {
  size_t r = cells_.rows();
  size_t c = cells_.columns();
  
  // The simplest case; optimize it
  if (newrows >= r && newcols >= c) {
    cells_.reserve(newrows, newcols);    
    return;
  }

  // Create the cells on demand, simpler and less error prone.
  // both can't be true due to above test
  if (newrows > r) {
    cells_.reserve(newrows, c);
  }
  else if (newcols > c) {
    cells_.reserve(r, newcols);
  }
  
  if (newrows < r || newcols < c) {
    size_t j = r;
    while (j > newrows) {      // j is at least 1 since newrows is at least 0
      // kill Cell object but not its contents
      size_t i = 0;
      while (i < c) {
	if (cells_[j-1][i]) {
	  cells_[j-1][i]->clear();
	  delete cells_[r][i];
	  cells_[j-1][i] = 0;
	}
	++i;
      }
      
      cells_.delete_row(j-1);
      --j;
    }

    j = c;
    while (c > newcols) {
      size_t i = 0;
      while (i < r) {
	if (cells_[i][j-1]) {
	  cells_[i][j-1]->clear(); 
	  delete cells_[i][j-1];
	  cells_[i][j-1] = 0;
	}
	++i;
      }
      
      cells_.delete_column(j-1);
      --j;
    }

    cells_.shrink(newrows, newcols);
  }
  
  g_assert(cells_.rows() == newrows);
  g_assert(cells_.columns() == newcols);
  
  arraydoc_model.size_changed(newrows, newcols);
}

// ArrayDoc will delete anything it contains at destruct time; 
//  it will not delete things that are replaced.

void 
ArrayDoc::set_axis(size_t row, size_t col, Axis* axis, Axis_Base::alignment_t a) 
{
  g_return_if_fail(row < cells_.rows());
  g_return_if_fail(col < cells_.columns());

  if (cells_[row][col] == 0) {
    cells_[row][col] = new Cell;
  }

  cells_[row][col]->axes[a] = axis;
  arraydoc_model.axis_changed(row,col,a);
}

void 
ArrayDoc::set_layer(size_t row, size_t col, LayerStack* layerstack) {
  g_return_if_fail(row < cells_.rows());
  g_return_if_fail(col < cells_.columns());

  if (cells_[row][col] == 0) {
    cells_[row][col] = new Cell;
  }
  cells_[row][col]->layerstack = layerstack;
  arraydoc_model.layer_changed(row,col);
}

ArrayDoc::Cell::Cell() : layerstack(0) {
  size_t i = 0;
  while (i < 4) {
    axes[i] = 0;
    ++i;
  }
}

void
ArrayDoc::Cell::clear() {
  layerstack = 0;
  size_t i = 0;
  while (i < 4) {
    axes[i] = 0;
    ++i;
  }
}

ArrayDoc::Cell::~Cell() {
  if (layerstack) delete layerstack;
  size_t i = 0;
  while (i < 4) {
    if (axes[i]) delete axes[i];
    ++i;
  }
}


