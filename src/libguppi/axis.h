// -*- C++ -*-

/* 
 * axis.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_AXIS_H
#define GUPPI_AXIS_H

#include <gtk--sig.h>
#include <libgnome/libgnome.h>

#include "named.h"

#include "canvasaxis.h"

class Axis : public Named,
	     public Gtk_Signal_Base
{
public:
  Axis() 
    : Named(_("Unlabelled Axis")) 
    {}
  virtual ~Axis() { destroy(); }

  virtual CanvasAxis* make_canvas_axis(Gnome_CanvasGroup& parent, 
				       Axis_Base::alignment_t a) = 0;

  mutable Signal0 destroy;

  mutable Signal1<const Axis &> name_changed;

protected:
  
private:
  
  void named_callback(const string & name) {
    name_changed(*this);
  }  
};

#endif
