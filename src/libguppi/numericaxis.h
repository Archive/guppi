// -*- C++ -*-

/* 
 * numericaxis.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_NUMERICAXIS_H
#define GUPPI_NUMERICAXIS_H

#include <list>
#include <algo.h>

#include "axis.h"

class NumericAxis : public Axis
{
public:
  NumericAxis(double min, double max) 
    : min_(min), max_(max),
      start_(min), stop_(max),
      has_min_(true), has_max_(true) 
    {}
  NumericAxis() : min_(0), max_(0), 
	start_(0), stop_(0), 
	has_min_(false), has_max_(false) 
    {}
  virtual ~NumericAxis() { destroy(); }

  virtual CanvasAxis* make_canvas_axis(Gnome_CanvasGroup& parent, 
				       Axis_Base::alignment_t a);

  void set_start(double start) { start_ = start; size_changed(*this); }
  void set_stop(double stop) { stop_ = stop; size_changed(*this); }

  // avoids two signals.
  void set_bounds(double start, double stop) {
    start_ = start; stop_ = stop;
    size_changed(*this);
  }

  // Start and stop are the user-selected bounds
  double get_start() const { return start_; }
  double get_stop() const { return stop_; }

  // Min and max are the limits on what the user can
  //  select. i.e. we don't know how to do a plot outside them.
  double get_min() const { return min_; }
  double get_max() const { return max_; }

  // Some plots will be of an infinite function, for example.
  bool has_min() const { return has_min_; }
  bool has_max() const { return has_max_; }

  mutable Signal1<const NumericAxis &> size_changed;

protected:
  
  void set_min(double min) { min_ = min; has_min_ = true; }
  void set_max(double max) { max_ = max; has_max_ = true; }

  void set_has_min(bool hm) { has_min_ = hm; }
  void set_has_max(bool hm) { has_max_ = hm; }

private:
  double min_;
  double max_;

  double start_;
  double stop_;

  bool has_min_;
  bool has_max_;
};

#endif
