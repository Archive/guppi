// -*- C++ -*-

/* 
 * arrayplot.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_ARRAYPLOT_H
#define GUPPI_ARRAYPLOT_H

#include "plot.h"
#include "arraydoc.h"

// An array of plots. ("array" != programming sense)

class ArrayPlotImpl;

class ArrayPlot : public Plot {
public:
  ArrayPlot(ArrayDoc & ad);
  virtual ~ArrayPlot();

  virtual GtkWidget* widget();

  void change_name    (const string & name);
  void change_icon    (const string & icon);
  void destroy_model  ();
  
protected:

private:
  ArrayDoc* ad_;
  ArrayPlotImpl* impl_;
  friend class ArrayPlotImpl;
};

#endif
