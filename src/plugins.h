// -*- C++ -*-

/* 
 * plugins.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PLUGINS_H
#define GUPPI_PLUGINS_H

#include <string>
#include <gmodule.h>
#include "plugin.h"

// This is a base class for items in the registry. 
// It replicates much of the Plugin interface; that's because the
//  plugin interface is implemented in the plugins, which we 
//  don't want to keep loaded.
class PluginHandle {
public:
  // Handle is passed in so subclasses can use it if they want.
  PluginHandle(GModule* module, const char* filename, const Plugin& plugin);
  virtual ~PluginHandle();

  const string & get_type() const;
  const string & get_menu_name() const;
  const string & get_name() const; 
  const string & get_description() const;

  // This isn't typesafe; you have to cast to the type
  //  you want. But it's really no big deal since you have 
  //  to ask for the handle by type, and avoiding it would 
  //  be annoying.
  const Plugin* get_plugin(); 

  bool loaded() const;

protected:

  // Return 0 on success, or dynamically allocated error string
  //  user must g_free.

  char* load();
  char* unload();

  void consider_unloading();

private:
  const string filename_; 

  const string type_;
  const string menu_name_;
  const string name_;
  const string description_;
  
  GModule* handle_;
  const Plugin*  plugin_;

  friend class PluginRegistry;
};


#include <vector>
#include <gtk--.h>

// How plugins are accessed depends on the specific plugin
//  type. PluginRegistry has magic knowledge of where to store 
//  each type.
class PluginRegistry : public Gtk_Signal_Base 
{
public:
  PluginRegistry();
  ~PluginRegistry();
  
  // Return 0 on success, or dynamically allocated error string
  //  user must g_free.

  // register everything we find in this directory
  char* register_dir(const char* dirname);
  
  // register a single plugin
  char* register_plugin(const char* filename);

  // We are throwing out typesafety a little, just to make 
  //  our life simpler.
  typedef vector<PluginHandle*>::iterator iterator;
  typedef vector<PluginHandle*>::const_iterator const_iterator;

  iterator begin(const char* type);
  iterator end(const char* type);

  const_iterator begin(const char* type) const;
  const_iterator end(const char* type) const;

  PluginHandle* get_handle(const char* type, size_t i);
  const PluginHandle* get_handle(const char* type, size_t i) const;

  // Sweep plugins and see if any can be unloaded, in a one-shot idle.
  void consider_unloading();

private:
  // crude yet effective.
  vector<PluginHandle*> plot_plugins_;
  vector<PluginHandle*> column_importer_plugins_;
  vector<PluginHandle*> transform_plugins_;

  vector<PluginHandle*>* type_to_vector(const char* type);
  const vector<PluginHandle*>* type_to_vector(const char* type) const;

  gint consider_unloading_cb();
};

extern PluginRegistry global_plugin_registry;

#endif

