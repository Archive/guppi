// -*- C++ -*-

/* 
 * guppidoc.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "guppidoc.h"

#include <libgnome/libgnome.h>


#include <gtk--/object.h> // Gtk_Trashcan

#include <algo.h>

#include <plugins.h>

#include <plotwindow.h>

static Gtk_Trashcan<GuppiDoc> global_guppidoc_trashcan;

void GuppiDoc::delete_self() 
{
  global_guppidoc_trashcan.throw_away(this);
}

GuppiDoc::GuppiDoc() : Named(_("Untitled Data Set")),
  pc_(columns_)
{
  set_gnome_icon("gnome-default.png");
}

GuppiDoc::~GuppiDoc()
{
  list<PlotDoc*>::iterator i = plots_.begin();
  while (i != plots_.end()) {
    delete *i;
    ++i;
  }

}

#if 0
void
GuppiDoc::column_data_changed(const DataSet & ds)
{
  not_saved();
}
#endif

void 
GuppiDoc::add_plot(PlotDoc & p)
{
  // fixme check for another column with the same name
  plots_.push_back(&p);

  PlotWindow* w = new PlotWindow(p);

  guppidoc_model.plot_added(p);

  w->show();
}

void 
GuppiDoc::delete_plot(PlotDoc* pd)
{
  PlotDoc* found = 0;

  list<PlotDoc*>::iterator i = find(plots_.begin(),plots_.end(),pd);
  if ( i != plots_.end() ) {
    g_print("Found and erased plot\n");
    found = *i;
    plots_.erase(i);
  }
  
  if (found != 0) {
    g_print("Emitting plot_deleted\n");
    guppidoc_model.plot_deleted(*pd);
    not_saved();
    delete pd;
  }
  else {
    g_warning("Plot not found in GuppiDoc!\n");
  }

  global_plugin_registry.consider_unloading();
}

guint GuppiDoc::num_docs_ = 0;

